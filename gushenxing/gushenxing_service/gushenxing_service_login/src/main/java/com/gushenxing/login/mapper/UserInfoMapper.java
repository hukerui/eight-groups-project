package com.gushenxing.login.mapper;


import com.gushenxing.login.pojo.UserInfo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@Repository
public interface UserInfoMapper extends Mapper<UserInfo> {

    @Update("update t_user set status='0' where username=#{username}")
    void updataStatus(@Param("username") String username);

    @Update("update t_user set password=#{password} where username=#{username}")
    void updataPassword(@Param("username") String username, @Param("password") String password);
}
