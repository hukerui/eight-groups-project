package com.gushenxing.login.service.impl;

import com.gushenxing.login.mapper.UserInfoMapper;
import com.gushenxing.login.pojo.UserInfo;
import com.gushenxing.login.service.UserInfoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

@Service
public class UserInfoServiceImpl implements UserInfoService {


    @Autowired
    private UserInfoMapper userInfoMapper;

    //根据用户名查询用户信息
    @Override
    public UserInfo findById(String username) {
        Example example = new Example(UserInfo.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("username",username);
        return userInfoMapper.selectOneByExample(example);
    }

    //根据用户名修改用户状态
    @Override
    public void updataStatus(String username) {
       userInfoMapper.updataStatus(username);
    }

    //用户首次登录修改密码
    @Override
    public void updataPassword(String username,String password) {
        //对用户密码进行加密
        String passwordDb = BCrypt.hashpw(password, BCrypt.gensalt());
        userInfoMapper.updataPassword(username,passwordDb);
    }

  /*  public static void main(String[] args) {

        System.out.println(BCrypt.hashpw("123456", BCrypt.gensalt()));
    }*/
}
