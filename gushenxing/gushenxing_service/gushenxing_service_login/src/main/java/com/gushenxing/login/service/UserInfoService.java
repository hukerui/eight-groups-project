package com.gushenxing.login.service;

import com.gushenxing.login.pojo.UserInfo;


public interface UserInfoService {

    UserInfo findById(String username);

    void updataStatus(String username);

    void updataPassword(String username, String password);
}
