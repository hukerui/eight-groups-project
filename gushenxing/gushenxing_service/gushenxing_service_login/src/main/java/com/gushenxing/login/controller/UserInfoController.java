package com.gushenxing.login.controller;


import com.gushenxing.commom.pojo.Result;
import com.gushenxing.commom.pojo.StatusCode;
import com.gushenxing.login.pojo.UserInfo;
import com.gushenxing.login.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/user")
public class UserInfoController {

    @Autowired
    private UserInfoService userInfoService;

    //根据用户名查询实体
    @GetMapping("/load/{username}")
    public UserInfo findUserInfo(@PathVariable("username") String username) {
        return userInfoService.findById(username);
    }

    //账户锁定修改用户状态
    @PutMapping("/edit/{username}")
    public Result edit(@PathVariable("username") String username) {
        userInfoService.updataStatus(username);
        return new Result(true, StatusCode.OK, "账户已被锁定");
    }

    //用户首次登录修改密码
    @PutMapping("/updataUser/{username}/{password}")
    public Result updataUser(@PathVariable("username") String username, @PathVariable("password") String password) {
        userInfoService.updataPassword(username, password);
        return new Result(true, StatusCode.OK, "修改密码成功");
    }
}
