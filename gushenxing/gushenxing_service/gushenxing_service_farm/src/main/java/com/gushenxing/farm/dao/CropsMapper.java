package com.gushenxing.farm.dao;

import com.gushenxing.farm.pojo.Crops;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;


/**
 * @author Swift
 * @date 2020/10/12
 */

@Repository
public interface CropsMapper extends Mapper<Crops> {
    //根据农作物名称和生长状态查询
    @Select("select id,opt_range from t_crops_index where  cid=(select id from t_crops where name =#{cname} and " +
            "stage=#{stage})")
    List<Map<String, Object>> findIndex(@Param("cname") String cname, @Param("stage") String stage);

    //根据id查询指标名
    @Select("select name from t_index where id=#{id}")
    String findIndexType(Integer id);

    //根据农作物查询害虫
    @Select("select pest from t_crops_disease where crops =#{crops}")
    List<String> findPest(String crops);

    //根据农作物名称查询成长阶段
    @Select("select stage from t_crops where name =#{cname}")
    List<String> findStageByCrops(String cname);


}
