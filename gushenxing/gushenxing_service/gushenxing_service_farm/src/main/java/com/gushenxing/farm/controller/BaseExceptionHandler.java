package com.gushenxing.farm.controller;

import com.gushenxing.commom.pojo.Result;
import com.gushenxing.commom.pojo.StatusCode;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 统一异常处理类
 */
@RestControllerAdvice
public class BaseExceptionHandler {

    @ExceptionHandler(Exception.class)
    public Result doException(Exception e){
        e.printStackTrace();
        return new Result(false, StatusCode.ERROR,"执行出错");
    }
}
