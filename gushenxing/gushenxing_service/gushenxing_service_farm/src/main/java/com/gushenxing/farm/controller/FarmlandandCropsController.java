package com.gushenxing.farm.controller;

import com.gushenxing.commom.pojo.Result;
import com.gushenxing.commom.pojo.StatusCode;
import com.gushenxing.farm.service.FarmlandandCropsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/farmlandcrops")
public class FarmlandandCropsController {

    @Autowired
    private FarmlandandCropsService farmlandandCropsService;


    //1.农作物详情
    @GetMapping("/cropsInfo/{id}")
    public Result cropsInfo(@PathVariable Integer id) {
        try {
            Map<String, Object> map =farmlandandCropsService.cropsInfo(id);
            return new Result(true, StatusCode.OK,"查询成功",map);

        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, StatusCode.ERROR, "查询失败");
        }
    }

}
