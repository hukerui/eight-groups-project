package com.gushenxing.farm.controller;

import com.github.pagehelper.Page;
import com.gushenxing.commom.pojo.PageResult;
import com.gushenxing.commom.pojo.Result;
import com.gushenxing.commom.pojo.StatusCode;
import com.gushenxing.farm.pojo.TCustFarm;
import com.gushenxing.farm.service.AreaService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/area")
public class AreaController {
    @Autowired
    private AreaService areaService;

    //1.根据农场主的Id,查询农场主对应的农场片区信息
    @GetMapping("/findArea/{custId}")
    public Result findArea(@PathVariable("custId") Integer custId,
                           @RequestParam(name = "pageNum", required = false, defaultValue = "1") String pageNum,
                           @RequestParam(name = "pageSize", required = false, defaultValue = "10") String pageSize) {

        try {
            if (StringUtils.isEmpty("pageNum")) {
                pageNum = "1";
            }
            if (StringUtils.isEmpty("pageSize")) {
                pageSize = "10";
            }
            Page<TCustFarm> resultPage = areaService.findArea(custId, pageNum, pageSize);
            PageResult result = new PageResult(resultPage.getTotal(), resultPage.getResult());
            return new Result(true, StatusCode.OK, "查询片区信息成功", result);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, StatusCode.ERROR, "查询片区信息失败");
        }
    }

    //2.根据农场主输入的片区id,查询对应的农场片区信息
    @GetMapping("/findById/{id}")
    public Result findById(@PathVariable Integer id) {
        try {
            TCustFarm tCustFarm = areaService.findById(id);
            return new Result(true, StatusCode.OK, "查询成功", tCustFarm);

        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, StatusCode.ERROR, "查询失败");
        }

    }

    //3.添加农场片区
    @PostMapping("/add")
    public Result areaAdd(@RequestBody TCustFarm tCustFarm) {
        try {
            areaService.areaAdd(tCustFarm);
            return new Result(true, StatusCode.OK, "添加成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, StatusCode.ERROR, "添加失败");
        }
    }

    //4.修改农场片区
    @PutMapping("/update")
    public Result areaUpdate(@RequestBody TCustFarm tCustFarm){
        try{
            areaService.areaUpdate(tCustFarm);
            return new Result(true, StatusCode.OK,"修改成功");
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false, StatusCode.ERROR,"修改失败");
        }
    }
}