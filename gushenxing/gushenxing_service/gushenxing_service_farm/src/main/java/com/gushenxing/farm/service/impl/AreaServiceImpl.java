package com.gushenxing.farm.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.gushenxing.farm.dao.AreaMapper;
import com.gushenxing.farm.pojo.TCustFarm;
import com.gushenxing.farm.service.AreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

@Service
public class AreaServiceImpl implements AreaService {

    @Autowired
    private AreaMapper areaMapper;

    //根据农场主ID这个条件,分页查询该农场主下面所有的片区信息(条件+分页)
    @Override
    public Page<TCustFarm> findArea(Integer custId, String pageNum, String pageSize) {
        PageHelper.startPage(Integer.parseInt(pageNum), Integer.parseInt(pageSize));
        Example example = new Example(TCustFarm.class);
        example.createCriteria().andEqualTo("custId", custId);
        Page<TCustFarm> tCustFarms = (Page<TCustFarm>) areaMapper.selectByExample(example);
        return tCustFarms;
    }

    //根据片区ID进行查询片区信息
    @Override
    public TCustFarm findById(Integer id) {
        TCustFarm tCustFarm = areaMapper.selectByPrimaryKey(id);
        return tCustFarm;
    }

    //添加农场片区
    @Override
    public void areaAdd(TCustFarm tCustFarm) {
        int i = areaMapper.insertSelective(tCustFarm);
        if(i==0){
            throw  new RuntimeException("修改失败");
        }
    }

    //修改农场片区
    @Override
    public void areaUpdate(TCustFarm tCustFarm) {
        int i = areaMapper.updateByPrimaryKey(tCustFarm);
        if(i==0){
            throw  new RuntimeException("修改失败");
        }
    }
}
