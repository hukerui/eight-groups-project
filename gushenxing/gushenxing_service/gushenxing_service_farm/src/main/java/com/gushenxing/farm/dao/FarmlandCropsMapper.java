package com.gushenxing.farm.dao;

import com.gushenxing.farm.pojo.TCustFarmlandCrops;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@Repository
public interface FarmlandCropsMapper extends Mapper<TCustFarmlandCrops> {

    //根据大田ID查询大田农作物
    @Select("select * from t_cust_farmland_crops where fid=#{id}")
    TCustFarmlandCrops findNameById(@Param("id") Integer id);


    //根据大田ID查询农作物名称
    @Select("select * from t_cust_farmland_crops where fid =#{fid}")
    TCustFarmlandCrops findCrops(Integer fid);

}
