package com.gushenxing.farm.service.impl;

import com.gushenxing.farm.dao.CropsMapper;
import com.gushenxing.farm.dao.FarmlandCropsMapper;
import com.gushenxing.farm.pojo.TCustFarmlandCrops;
import com.gushenxing.farm.service.FarmlandandCropsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class FarmlandandCropsServiceImpl implements FarmlandandCropsService {

    @Autowired
    private FarmlandCropsMapper farmlandCropsMapper;

    @Autowired
    private CropsMapper cropsMapper;


    //1.农作物详情
    @Override
    public Map<String, Object> cropsInfo(Integer id) {
        Map<String, Object> map = new HashMap<>();
        //根据大田id查询农作物和农作物生长状态
        TCustFarmlandCrops crops = farmlandCropsMapper.findNameById(id);
        //获取农作物名称
        String cname = crops.getCname();
        //获取农作物生长状态
        String stage = crops.getStage();
        //根据农作物名称和生长状态查询指标
        List<Map<String, Object>> cropsMapperIndexList = cropsMapper.findIndex(cname, stage);
        //指标map，用于存放指标数据
        List<Map<String, String>> list = new ArrayList<>();
        for (Map<String, Object> cropsMapperIndexmap : cropsMapperIndexList) {
            //遍历map集合
            for (String s : cropsMapperIndexmap.keySet()) {
                if ("id".equals(s)) {
                    //获取联合表的ID
                    Integer cropid = (Integer) cropsMapperIndexmap.get(s);
                    //根据ID查询指标名称
                    String indexType = cropsMapper.findIndexType(cropid);
                    Map<String, String> indexMap = new HashMap<>();
                    indexMap.put("indexType", indexType);
                    indexMap.put("range", cropsMapperIndexmap.get("opt_range").toString());
                    list.add(indexMap);
                }
            }
        }
        //添加指标数据
        map.put("index", list);
        //获取检测设备检测数据指标(目前显示假数据)
        List<Map<String,String>> newlist=new ArrayList<>();
        Map<String,String> newMap=new HashMap<>();
        newMap.put("newType","水");
        newMap.put("newRange","40%");
        newlist.add(newMap);
        map.put("newIndex",newlist);
        return map;
    }

}

