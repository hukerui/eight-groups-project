package com.gushenxing.farm.dao;

import com.gushenxing.farm.pojo.TCustFarmland;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@Repository
public interface FarmlandMapper extends Mapper<TCustFarmland> {

    //根据关键词进行条件查询
    @Select("<script>select * from t_cust_farmland <if test=\"keyword!=null and keyword!=''\">where size=#{keyword} " +
            "or shape=#{keyword} or code=#{keyword} or status=#{keyword}</if></script>")
    List<TCustFarmland> findByKeyWord(@Param("keyword") String keyword);


    //根据大田id查询所属片区ID
    @Select("select aid FROM t_cust_farmland WHERE id =#{id}")
    Integer findIdById(Integer id);
}
