package com.gushenxing.farm.dao;

import com.gushenxing.farm.pojo.Index;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;



@Repository
public interface IndexMapper extends Mapper<Index> {
}
