package com.gushenxing.farm.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.gushenxing.farm.dao.*;
import com.gushenxing.farm.pojo.FarmlandDevice;
import com.gushenxing.farm.pojo.TCustFarm;
import com.gushenxing.farm.pojo.TCustFarmland;
import com.gushenxing.farm.pojo.TCustFarmlandCrops;
import com.gushenxing.farm.service.FarmlandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class FarmlandServiceImpl implements FarmlandService {

    @Autowired
    private FarmlandMapper farmlandMapper;

    @Autowired
    private CustFarmlandCropsMapper custFarmlandCropsMapper;

    @Autowired
    private AreaMapper areaMapper;

    @Autowired
    private FarmlandCropsMapper farmlandCropsMapper;

    @Autowired
    private CropsMapper cropsMapper;

    @Autowired
    private DeviceMapper deviceMapper;

    @Autowired
    private FieldMapper fieldMapper;

    //1.关键词条件分页查询大田
    @Override
    public Page<TCustFarmland> findPage(Integer pageSize, Integer pageNum, Integer aid, String keyword) {
        PageHelper.startPage(pageNum, pageSize);
        Page<TCustFarmland> page = (Page<TCustFarmland>) farmlandMapper.findByKeyWord(keyword);
        return page;
    }

    //2.根据大田ID查询大田详情
    @Override
    public Map<String, Object> queryById(Integer id) {
        //创建Map用于储存相关信息
        Map<String, Object> map = new HashMap<>();
        //根据大田ID查询片区id
        Integer aid = farmlandMapper.findIdById(id);
        //根据片区ID查询片区名称
        TCustFarm tCustFarm = areaMapper.selectByPrimaryKey(aid);
        map.put("areaName", tCustFarm.getName());
        //根据大田ID查询农作物名称
        TCustFarmlandCrops tCustFarmlandCrops = farmlandCropsMapper.findNameById(id);
        //添加农作物名称
        map.put("cname", tCustFarmlandCrops.getCname());
        //根据农作物名称和生长状态查询相关指标
        List<Map<String, Object>> cropsMapperIndexList = cropsMapper.findIndex(tCustFarmlandCrops.getCname(),
                tCustFarmlandCrops.getStage());
        //指标map，用于存放指标数据
        List<Map<String, String>> list = new ArrayList<>();
        for (Map<String, Object> cropsMapperIndexmap : cropsMapperIndexList) {
            //遍历map集合
            for (String s : cropsMapperIndexmap.keySet()) {
                if ("id".equals(s)) {
                    //获取联合表的ID
                    Integer cropid = (Integer) cropsMapperIndexmap.get(s);
                    //根据ID查询指标名称
                    String indexType = cropsMapper.findIndexType(cropid);
                    Map<String, String> indexMap = new HashMap<>();
                    indexMap.put("indexType", indexType);
                    indexMap.put("range", cropsMapperIndexmap.get("opt_range").toString());
                    list.add(indexMap);
                }
            }
        }
        //添加指标数据
        map.put("crops", list);
        //根据农作物查询害虫
        List<String> pestList = cropsMapper.findPest(tCustFarmlandCrops.getCname());
        map.put("pests", pestList);
        //根据大田fid查询设备信息
        Example example = new Example(FarmlandDevice.class);
        example.createCriteria().andEqualTo("fid", id);

        List<FarmlandDevice> deviceList = fieldMapper.selectByExample(example);
        //添加设备数量
        map.put("deviceNum", deviceList.size());
        //添加设备集合
        map.put("devices", deviceList);
        //根据大田ID查询大田基本信息
        TCustFarm custFarm1 = areaMapper.selectByPrimaryKey(id);
        map.put("custFarml", custFarm1);
        return map;

    }


    //3.添加大田
    @Override
    public void add(TCustFarmland tCustFarmland) {
        int i = farmlandMapper.insertSelective(tCustFarmland);
        if (i == 0) {
            throw new RuntimeException("添加失败!");
        }
    }

    //4.删除大田
    @Override
    public void delete(Integer id) {
        int i = farmlandMapper.deleteByPrimaryKey(id);
        if (i == 0) {
            throw new RuntimeException("删除失败!");
        }
    }

    //5.添加大田农作物
    @Override
    public void addCrops(TCustFarmlandCrops tCustFarmlandCrops) {
        farmlandCropsMapper.insertSelective(tCustFarmlandCrops);
    }

}
