package com.gushenxing.farm.service;

import com.github.pagehelper.Page;
import com.gushenxing.farm.pojo.TCustFarmland;
import com.gushenxing.farm.pojo.TCustFarmlandCrops;

import java.util.Map;

public interface FarmlandService {
    //1.根据片区id或关键词条件分页查询大田信息
    Page<TCustFarmland> findPage(Integer pageSize, Integer pageNum, Integer aid, String keyword);

    //2.根据大田ID查询大田详情
    Map<String, Object> queryById(Integer id);

    //3.添加大田
    void add(TCustFarmland tCustFarmland);

    //4.根据大田id删除大田
    void delete(Integer id);

    //5.添加大田农作物
    void addCrops(TCustFarmlandCrops tCustFarmlandCrops);
}
