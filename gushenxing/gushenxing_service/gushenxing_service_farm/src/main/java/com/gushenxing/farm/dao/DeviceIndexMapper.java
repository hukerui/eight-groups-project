package com.gushenxing.farm.dao;

import com.gushenxing.farm.pojo.DeviceIndex;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author Swift
 * @date 2020/10/12
 */

@Repository
public interface DeviceIndexMapper extends Mapper<DeviceIndex> {
}
