package com.gushenxing.farm.service;

import com.github.pagehelper.Page;
import com.gushenxing.farm.pojo.FarmlandDevice;

import java.util.List;
import java.util.Map;

/**
 * @author Swift
 * @date 2020/10/12
 */


public interface FieldService {

    //根据农田id去查询所有的设备信息
    List<Map<Object,Object>> checkDeviceById(Integer id);

    //根据农田的id去查询农作物数据和设备数据,进而比较,实现自动告警
    List<Map> autoAlarm(Integer id);

    //添加大田设备
    void addDevice(FarmlandDevice farmlandDevice);

    //删除大田设备根据设备id
    void deleteDeviceByDid(Integer did);

    //查询该大田所有设备信息
    Page<FarmlandDevice> findPage(Integer fid,String pageNum, String pageSize);

    //查询设备运行状态, 并自动设置设备运行状态
    List<Map> deviceStage(Integer fid);

    //手动开启设备
    void deviceStart(Map deviceMap);

    //手动关闭设备
    void deviceStop(Map deviceMap);

    //显示大田某个设备的详细信息
    Map deviceDetail(Integer did);

    //分页条件查询
    Page<FarmlandDevice> queryPage(Map<String,Object> searchMap, String page, String size);
}
