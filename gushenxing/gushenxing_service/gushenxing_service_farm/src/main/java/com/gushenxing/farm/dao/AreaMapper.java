package com.gushenxing.farm.dao;

import com.gushenxing.farm.pojo.TCustFarm;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@Repository
public interface AreaMapper extends Mapper<TCustFarm>{

}
