package com.gushenxing.farm.controller;

import com.github.pagehelper.Page;
import com.gushenxing.commom.pojo.PageResult;
import com.gushenxing.commom.pojo.Result;
import com.gushenxing.commom.pojo.StatusCode;
import com.gushenxing.farm.pojo.TCustFarmland;
import com.gushenxing.farm.pojo.TCustFarmlandCrops;
import com.gushenxing.farm.service.FarmlandService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/farmland")
public class FarmlandController {
    @Autowired
    private FarmlandService farmlandService;

    //1.根据片区进行分页条件分页查询
    @GetMapping("/query")
    public Result query(Integer pageSize,
                        Integer pageNum,
                        Integer aid,
                        String keyword) {
        if (StringUtils.isEmpty("pageSize")) {
            pageNum = 1;
        }
        if (StringUtils.isEmpty("pageSize")) {
            pageSize = 10;
        }
        try {
            Page<TCustFarmland> pageList = farmlandService.findPage(pageSize, pageNum, aid, keyword);
            PageResult pageResult = new PageResult(pageList.getTotal(), pageList.getResult());
            return new Result(true, StatusCode.OK, "查询成功", pageResult);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, StatusCode.ERROR, "查询失败");
        }
    }

    //2.根据大田ID查询大田详情
    @GetMapping("/queryById/{id}")
    public Result queryById(@PathVariable Integer id) {
        try {
            Map<String, Object> map = farmlandService.queryById(id);
            return new Result(true, StatusCode.OK, "查询成功", map);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, StatusCode.ERROR, "查询失败");
        }
    }

    //3.添加大田:
    @PostMapping("/add")
    public Result add(@RequestBody TCustFarmland tCustFarmland) {
        try {
            farmlandService.add(tCustFarmland);

        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, StatusCode.ERROR, "添加失败");
        }
        return new Result(true, StatusCode.OK, "添加成功");
    }

    //4.根据大田id删除大田
    @DeleteMapping("/deleteById/{id}")
    public Result delete(@PathVariable Integer id) {
        try {
            farmlandService.delete(id);

        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, StatusCode.ERROR, "删除失败");
        }
        return new Result(true, StatusCode.OK, "删除成功");
    }

    //5.添加农作物
    @PostMapping("/addCrops/{fid}/{cid}")
    public Result addCrops(@RequestBody TCustFarmlandCrops tCustFarmlandCrops, @PathVariable Integer fid,
                           @PathVariable Integer cid) {
        try {
            //添加大田id和农作物id到大田农作物表中
            tCustFarmlandCrops.setFid(fid);
            tCustFarmlandCrops.setCid(cid);
            //调用service层, 添加大田农作物
            farmlandService.addCrops(tCustFarmlandCrops);
            return new Result(true, StatusCode.OK, "添加成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, StatusCode.ERROR, "添加失败");
        }
    }




}
