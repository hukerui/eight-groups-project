package com.gushenxing.farm.dao;

import com.gushenxing.farm.pojo.FarmlandDevice;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author Swift
 * @date 2020/10/12
 */


@Repository
public interface FieldMapper extends Mapper<FarmlandDevice> {

    //手动开启设备
    @Update("update t_cust_farmland_device set mstage=0 where did = #{did} ")
    void getMstageUpByDid(@Param("did")Integer did);

    //手动关闭设备
    @Update("update t_cust_farmland_device set mstage=1 where did = #{did} ")
    void getMstageDownByDid(@Param("did") Integer did);
}
