package com.gushenxing.farm.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.gushenxing.farm.dao.*;
import com.gushenxing.farm.pojo.*;
import com.gushenxing.farm.service.FieldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Swift
 * @date 2020/10/12
 */


@Service
public class FieldServiceImpl implements FieldService {

    //大田设备 mapper
    @Autowired
    private FieldMapper fieldMapper;

    @Autowired
    private DeviceIndexMapper deviceIndexMapper;

    @Autowired
    private IndexMapper indexMapper;

    @Autowired
    private DeviceMapper deviceMapper;

    @Autowired
    private CropsIndexMapper cropsIndexMapper;

    @Autowired
    private CropsMapper cropsMapper;

    @Autowired
    private CustFarmlandCropsMapper custFarmlandCropsMapper;

    /**
     *     根据农田id去查询所有的设备信息
     * @param id
     * @return
     */
    @Override
    public List<Map<Object,Object>> checkDeviceById(Integer id) {
        //根据农田的id去查询所有的设备信息
        List<FarmlandDevice> deviceList = this.getFarmlandDevice(id);

        List<Map<Object,Object>> resultList = new ArrayList<>();
        //对设备信息进行遍历
        if (deviceList != null && deviceList.size() > 0){
            for (FarmlandDevice farmlandDevice : deviceList) {
                Map<Object,Object> map = new HashMap<>();
                //获取设备状态, 运行状态, 位置
                map.put("position",farmlandDevice.getPosition());
                map.put("dstage",farmlandDevice.getDstage());
                map.put("mstage",farmlandDevice.getMstage());
                //如果设备的状态为1 , 说明该设备已损坏, 需要给出提示信息  正常就不需要给出信息
                if (farmlandDevice.getDstage() == 1){
                    map.put("message","该设备需要维修");
                }else if (farmlandDevice.getDstage() == 0){
                    map.put("message","设备正常");
                }
                //根据设备id去查询设备名字, 编号
                Example example2 = new Example(Device.class);
                example2.createCriteria().andEqualTo("id",farmlandDevice.getDid());
                Device device = deviceMapper.selectOneByExample(example2);
                map.put("deviceName",device.getName());
                map.put("deviceCode",device.getCode());

                //根据设备id去查询 设备检测信息
                Example example1 = new Example(DeviceIndex.class);
                example1.createCriteria().andEqualTo("did",farmlandDevice.getDid());

                DeviceIndex deviceIndex = deviceIndexMapper.selectOneByExample(example1);
                //获取测量最大值  获取测量最小值
                String rangeMax = deviceIndex.getRangeMax();
                String rangeMin = deviceIndex.getRangeMin();
                //获取设备的数据指标id
                int iid = deviceIndex.getIid();
                //根据数据指标id去查询指标名称, 指标单位
                Index index = new Index();
                index.setId(iid);
                Index selectOne = indexMapper.selectOne(index);
                map.put("indexName",selectOne.getName());
                map.put("range",rangeMin+"-"+rangeMax+selectOne.getUnit());
                resultList.add(map);
            }
        }

        return resultList;
    }

    private List<FarmlandDevice> getFarmlandDevice(Integer id){
        //根据农田id查询t_cust_farmland_device这张表
        Example example = new Example(FarmlandDevice.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("fid",id);
        //查询所有设备信息
        List<FarmlandDevice> deviceList = fieldMapper.selectByExample(example);
        return deviceList;
    }

    /**
     *      //根据农田的id去查询农作物数据和设备数据,进而比较,实现自动告警
     * @param id
     * @return
     */
    @Override
    public List<Map> autoAlarm(Integer id) {
        //根据农田的id去查询所有设备信息
        List<FarmlandDevice> deviceList = this.getFarmlandDevice(id);

        List<Map> resultList = new ArrayList<>();
        //对设备信息进行遍历
        if (deviceList != null && deviceList.size() > 0){
            for (FarmlandDevice farmlandDevice : deviceList) {
                Map<Object,Object> map = new HashMap<>();
                //获取设备状态, 运行状态, 位置
                map.put("position",farmlandDevice.getPosition());


                //这里要对设备的状态进行判断, 如果设备坏了, 则没有自动预警功能
                Integer dstage = farmlandDevice.getDstage();
                if (dstage == 1){
                    continue;//如果为已损坏, 则跳过此次循环
                }
                map.put("dstage",farmlandDevice.getDstage());
                map.put("mstage",farmlandDevice.getMstage());

                //根据设备id去查询设备名字, 编号
                Example example1 = new Example(Device.class);
                example1.createCriteria().andEqualTo("id",farmlandDevice.getDid());
                Device device = deviceMapper.selectOneByExample(example1);
                map.put("deviceName",device.getName());
                map.put("deviceCode",device.getCode());

                //根据设备id去查询 设备检测信息
                Example example2 = new Example(DeviceIndex.class);
                example2.createCriteria().andEqualTo("did",farmlandDevice.getDid());

                DeviceIndex deviceIndex = deviceIndexMapper.selectOneByExample(example1);
                //获取测量最大值  获取测量最小值
                Integer rangeMax = Integer.parseInt(deviceIndex.getRangeMax());
                Integer rangeMin = Integer.parseInt(deviceIndex.getRangeMin());
                //获取设备的数据指标id
                int iid = deviceIndex.getIid();
                //根据数据指标id去查询指标名称, 指标单位
                Index index = new Index();
                index.setId(iid);
                Index selectOne = indexMapper.selectOne(index);
                map.put("indexName",selectOne.getName());
                map.put("deviceRange",rangeMin+"-"+rangeMax+selectOne.getUnit());


                //根据大田id去查询农作物id
                CustFarmlandCrops custFarmlandCrops = new CustFarmlandCrops();
                custFarmlandCrops.setFid(id);
                CustFarmlandCrops resultCfc = custFarmlandCropsMapper.selectOne(custFarmlandCrops);
                Integer cid = resultCfc.getCid();

                //根据农作物id去查询农作物名字
                Example example4 = new Example(Crops.class);
                example4.createCriteria().andEqualTo("id",cid);
                Crops crops = cropsMapper.selectOneByExample(example4);
                map.put("cropsName",crops.getName());


                //根据农作物id去查指标id的集合  一对多
                Example example3 = new Example(CropsIndex.class);
                example3.createCriteria().andEqualTo("cid", cid);

                List<CropsIndex> cropsIndices = cropsIndexMapper.selectByExample(example3);
                if (cropsIndices != null && cropsIndices.size() > 0){
                    for (CropsIndex cropsIndex : cropsIndices) {
                        if (cropsIndex.getIid() == iid){
                            //获取农作物最大指标, 农作物最小指标
                            Integer maxRange = Integer.parseInt(cropsIndex.getMaxRange());
                            Integer minRange = Integer.parseInt(cropsIndex.getMinRange());
                            //农作物的生长阶段
                            String stage = cropsIndex.getStage();
                            map.put("cropsStage",stage);
                            //农作物的生长范围
                            map.put("cropsRange",minRange+"-"+maxRange+selectOne.getUnit());

                            //将设备数据和农作物数据进行比较
                            if (rangeMin >= minRange && rangeMax <= maxRange){
                                map.put("message","农作物生长正常");
                            }else {
                                map.put("message","农作物不在生长范围内,需要处理");
                            }
                        }
                    }
                }

                resultList.add(map);
            }
        }

        return resultList;
    }

    /**
     *  添加大田设备
     * @param farmlandDevice
     */
    @Override
    public void addDevice(FarmlandDevice farmlandDevice) {
        fieldMapper.insertSelective(farmlandDevice);
    }

    /**
     *  删除大田设备
     * @param did
     */
    @Override
    public void deleteDeviceByDid(Integer did) {
        Example example = new Example(FarmlandDevice.class);
        example.createCriteria().andEqualTo("did",did);
        fieldMapper.deleteByExample(example);
    }

    /**
     *      查询所有大田设备信息
     * @param pageNum
     * @param pageSize
     */
    @Override
    public Page<FarmlandDevice> findPage(Integer fid,String pageNum, String pageSize) {
        PageHelper.startPage(Integer.parseInt(pageNum), Integer.parseInt(pageSize));
        Example example = new Example(FarmlandDevice.class);
        example.createCriteria().andEqualTo("fid",fid);
        return (Page<FarmlandDevice>)fieldMapper.selectByExample(example);
    }

    //查询设备运行状态, 并自动设置设备运行状态
    @Override
    public List<Map> deviceStage(Integer fid) {

        //根据农田的id去查询所有设备信息
        List<FarmlandDevice> deviceList = this.getFarmlandDevice(fid);

        List<Map> resultList = new ArrayList<>();
        //对设备信息进行遍历
        if (deviceList != null && deviceList.size() > 0){
            for (FarmlandDevice farmlandDevice : deviceList) {
                Map<Object,Object> map = new HashMap<>();
                //获取设备id,状态,  位置
                map.put("did",farmlandDevice.getDid());
                map.put("position",farmlandDevice.getPosition());


                //这里要对设备的状态进行判断, 如果设备的状态为已损坏, 则无法自动开启关闭设备
                Integer dstage = farmlandDevice.getDstage();
                if (dstage == 1){
                    continue; // 跳出此次循环
                }
                map.put("dstage",dstage);

                //根据设备id去查询设备名字, 编号
                Example example1 = new Example(Device.class);
                example1.createCriteria().andEqualTo("id",farmlandDevice.getDid());
                Device device = deviceMapper.selectOneByExample(example1);
                map.put("deviceName",device.getName());
                map.put("deviceCode",device.getCode());

                //根据设备id去查询 设备检测信息
                Example example2 = new Example(DeviceIndex.class);
                example2.createCriteria().andEqualTo("did",farmlandDevice.getDid());

                DeviceIndex deviceIndex = deviceIndexMapper.selectOneByExample(example1);
                //获取测量最大值  获取测量最小值
                Integer rangeMax = Integer.parseInt(deviceIndex.getRangeMax());
                Integer rangeMin = Integer.parseInt(deviceIndex.getRangeMin());
                //获取设备的数据指标id
                int iid = deviceIndex.getIid();
                //根据数据指标id去查询指标名称, 指标单位
                Index index = new Index();
                index.setId(iid);
                Index selectOne = indexMapper.selectOne(index);
                map.put("indexName",selectOne.getName());
                map.put("deviceRange",rangeMin+"-"+rangeMax+selectOne.getUnit());


                //根据大田id去查询农作物id
                CustFarmlandCrops custFarmlandCrops = new CustFarmlandCrops();
                custFarmlandCrops.setFid(fid);
                CustFarmlandCrops resultCfc = custFarmlandCropsMapper.selectOne(custFarmlandCrops);
                Integer cid = resultCfc.getCid();

                //根据农作物id去查询农作物名字
                Example example4 = new Example(Crops.class);
                example4.createCriteria().andEqualTo("id",cid);

                Crops crops = cropsMapper.selectOneByExample(example4);
                map.put("cropsName",crops.getName());


                //根据农作物id去查指标id的集合  一对多
                Example example3 = new Example(CropsIndex.class);
                example3.createCriteria().andEqualTo("cid", cid);

                List<CropsIndex> cropsIndices = cropsIndexMapper.selectByExample(example3);
                if (cropsIndices != null && cropsIndices.size() > 0){
                    for (CropsIndex cropsIndex : cropsIndices) {
                        //如果农作物指标中间表的指标id 等于 设备表的指标id
                        if (cropsIndex.getIid() == iid){
                            //获取农作物最大指标, 农作物最小指标
                            Integer maxRange = Integer.parseInt(cropsIndex.getMaxRange());
                            Integer minRange = Integer.parseInt(cropsIndex.getMinRange());
                            //农作物的生长阶段
                            String stage = cropsIndex.getStage();
                            map.put("cropsStage",stage);
                            //农作物的生长范围
                            map.put("cropsRange",minRange+"-"+maxRange+selectOne.getUnit());

                            //将设备数据和农作物数据进行比较  在农作物生长范围内
                            if (rangeMin >= minRange && rangeMax <= maxRange){
                                //开启设备
                                farmlandDevice.setMstage(1);

                                //将数据库表中信息 运行状态修改 关闭
                                fieldMapper.getMstageDownByDid(farmlandDevice.getDid());
                                map.put("mstage",farmlandDevice.getMstage());
                                //给出提示信息, 方便手动用户手动开启看到提示信息
                                map.put("message","农作物在正常范围内,自动关闭设备");
                                System.out.println("自动关闭了设备");
                            }else {
                                //如果农作物不在生长范围内, 就开启设备
                                farmlandDevice.setMstage(0);

                                //将数据库表中信息 运行状态修改 关开启
                                fieldMapper.getMstageUpByDid(farmlandDevice.getDid());
                                map.put("mstage",farmlandDevice.getMstage());
                                //给出提示信息, 方便手动用户手动关闭看到提示信息
                                map.put("message","农作物不在正常范围内,自动开启设备");
                                System.out.println("自动开启了设备");
                            }
                        }
                    }
                }

                resultList.add(map);
            }
        }

        return resultList;
    }


    //手动开启设备
    @Override
    public void deviceStart(Map deviceMap) {
        Integer did = (Integer) deviceMap.get("did");

        //将表中信息 运行状态修改 开启
        fieldMapper.getMstageUpByDid(did);

        System.out.println("手动开启了设备");

    }


    //手动关闭设备
    @Override
    public void deviceStop(Map deviceMap) {
        Integer did = (Integer) deviceMap.get("did");

        //将表中信息 运行状态修改关闭
        fieldMapper.getMstageDownByDid(did);

        System.out.println("手动关闭了设备");

    }

    //通过设备id获取大田设备信息
    private FarmlandDevice getFarmlandDeviceByDid(Integer did) {

        //通过设备的id  去t_farmland_device表中查询这个信息
        Example example = new Example(FarmlandDevice.class);
        example.createCriteria().andEqualTo("did", did);
        return fieldMapper.selectOneByExample(example);
    }

    //显示大田某个设备的详细信息, 包括农作物指标, 设备指标
    public Map deviceDetail(Integer did) {
        FarmlandDevice farmlandDevice = this.getFarmlandDeviceByDid(did);
        Map<Object,Object> map = new HashMap<>();
        //获取设备id,状态,  位置
        map.put("did",farmlandDevice.getDid());
        map.put("position",farmlandDevice.getPosition());
        map.put("dstage",farmlandDevice.getDstage());
        map.put("mstage",farmlandDevice.getMstage());

        //根据设备id去查询设备名字, 编号
        Example example1 = new Example(Device.class);
        example1.createCriteria().andEqualTo("id",farmlandDevice.getDid());
        Device device = deviceMapper.selectOneByExample(example1);
        map.put("deviceName",device.getName());
        map.put("deviceCode",device.getCode());

        //根据设备id去查询 设备检测信息
        Example example2 = new Example(DeviceIndex.class);
        example2.createCriteria().andEqualTo("did",farmlandDevice.getDid());

        DeviceIndex deviceIndex = deviceIndexMapper.selectOneByExample(example1);
        //获取测量最大值  获取测量最小值
        Integer rangeMax = Integer.parseInt(deviceIndex.getRangeMax());
        Integer rangeMin = Integer.parseInt(deviceIndex.getRangeMin());
        //获取设备的数据指标id
        int iid = deviceIndex.getIid();
        //根据数据指标id去查询指标名称, 指标单位
        Index index = new Index();
        index.setId(iid);
        Index selectOne = indexMapper.selectOne(index);
        map.put("indexName",selectOne.getName());

        //设备正常 才能显示监测范围信息
        if (farmlandDevice.getDstage() == 0){
            map.put("deviceRange",rangeMin+"-"+rangeMax+selectOne.getUnit());
        }


        //根据大田id去查询农作物id
        CustFarmlandCrops custFarmlandCrops = new CustFarmlandCrops();
        custFarmlandCrops.setFid(farmlandDevice.getFid());
        CustFarmlandCrops resultCfc = custFarmlandCropsMapper.selectOne(custFarmlandCrops);
        Integer cid = resultCfc.getCid();

        //根据农作物id去查询农作物名字
        Example example4 = new Example(Crops.class);
        example4.createCriteria().andEqualTo("id",cid);
        Crops crops = cropsMapper.selectOneByExample(example4);
        map.put("cropsName",crops.getName());


        //根据农作物id去查指标id的集合  一对多
        Example example3 = new Example(CropsIndex.class);
        example3.createCriteria().andEqualTo("cid", cid);

        List<CropsIndex> cropsIndices = cropsIndexMapper.selectByExample(example3);
        if (cropsIndices != null && cropsIndices.size() > 0){
            for (CropsIndex cropsIndex : cropsIndices) {
                //如果农作物指标中间表的指标id 等于 设备表的指标id
                if (cropsIndex.getIid() == iid){
                    //获取农作物最大指标, 农作物最小指标
                    Integer maxRange = Integer.parseInt(cropsIndex.getMaxRange());
                    Integer minRange = Integer.parseInt(cropsIndex.getMinRange());
                    //农作物的生长阶段
                    String stage = cropsIndex.getStage();
                    map.put("cropsStage",stage);
                    //农作物的生长范围
                    map.put("cropsRange",minRange+"-"+maxRange+selectOne.getUnit());

                    if (farmlandDevice.getDstage() == 0) {
                        //将设备数据和农作物数据进行比较 不再农作物生长范围内
                        if (rangeMin >= minRange && rangeMax <= maxRange) {
                            //给出提示信息, 方便手动用户手动开启看到提示信息
                            map.put("message", "农作物在正常范围内");
                        } else {

                            //给出提示信息, 方便手动用户手动关闭看到提示信息
                            map.put("message", "农作物不在正常范围内");
                        }
                    }
                }

            }

        }
        return map;
    }

    //分页条件查询
    @Override
    public Page<FarmlandDevice> queryPage(Map<String,Object> searchMap, String page, String size) {
        PageHelper.startPage(Integer.parseInt(page),Integer.parseInt(size));
        Example example = createExample(searchMap);
        Page<FarmlandDevice> farmlandDevices = (Page<FarmlandDevice>) fieldMapper.selectByExample(example);
        
        return farmlandDevices;
    }

    private Example createExample(Map<String,Object> searchMap){
        Example example = new Example(FarmlandDevice.class);
        Example.Criteria criteria = example.createCriteria();

        if (searchMap != null){
            //大田设备的位置
            if (searchMap.get("position") != null && !"".equals(searchMap.get("position"))){
                criteria.andLike("position","%"+searchMap.get("position")+"%");
            }
            //大田设备的状态
            if (searchMap.get("dstage") != null ){
                criteria.andEqualTo("dstage",searchMap.get("dstage"));
            }
            //大田设备的运行状态
            if (searchMap.get("mstage") != null){
                criteria.andEqualTo("mstage",searchMap.get("mstage"));
            }
        }
        return example;
    }
}
