package com.gushenxing.farm.service;

import java.util.Map;

public interface FarmlandandCropsService {

    //农作物详情
    Map<String, Object> cropsInfo(Integer id);

}
