package com.gushenxing.farm.controller;

import com.github.pagehelper.Page;
import com.gushenxing.commom.pojo.PageResult;
import com.gushenxing.commom.pojo.Result;
import com.gushenxing.commom.pojo.StatusCode;
import com.gushenxing.farm.pojo.FarmlandDevice;
import com.gushenxing.farm.service.FieldService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


@CrossOrigin
@RestController
@RequestMapping("/field")
public class FieldController {

    @Autowired
    private FieldService fieldService;

    /**
     *  通过大田的id去查询大田中的设备信息
     * @param id
     * @return
     */
    @GetMapping("/checkDevice/{id}")
    public Result checkDevice(@PathVariable("id")Integer id){
        try{
            List<Map<Object,Object>> deviceList = fieldService.checkDeviceById(id);
            return new Result(true,StatusCode.OK,"巡检设备成功",deviceList);
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false, StatusCode.ERROR,"巡检设备失败");
        }
    }

    /**
     * 通过大田的id去显示设备信息和农作物信息, 进而判断指标, 从而实现设备自动预警
     * @param id
     * @return
     */
    @GetMapping("/autoAlarm/{id}")
    public Result autoAlarm(@PathVariable("id")Integer id){
        try{
            List<Map> resultList = fieldService.autoAlarm(id);
            return new Result(true,StatusCode.OK,"自动告警成功",resultList);
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false,StatusCode.OK,"自动告警失败");
        }
    }


    //向大田中添加设备  根据设备的编号和大田的编号进行设备的安装
    @PostMapping("/addDevice/{fid}/{did}")
    public Result addDevice(@RequestBody FarmlandDevice farmlandDevice,@PathVariable Integer fid, @PathVariable Integer did){
        try{
            //添加设备的id和大田的id到大田设备表中
            farmlandDevice.setDid(did);
            farmlandDevice.setFid(fid);
            //调用service层, 添加大田设备
            fieldService.addDevice(farmlandDevice);
            return new Result(true,StatusCode.OK,"添加大田设备成功");
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false,StatusCode.ERROR,"添加大田设备失败");
        }
    }

    //向大田中删除设备  根据设备的id来删除  因为一个大田中有多台设备, 所以不能根据大田的id去删除设备
    @GetMapping("/deleteDevice/{did}")
    public Result deleteDevice(@PathVariable("did") Integer did){
        try {
            fieldService.deleteDeviceByDid(did);
            return new Result(true,StatusCode.OK,"删除大田设备成功");
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false,StatusCode.ERROR,"删除大田设备失败");
        }
    }


    //根据大田id查询所有设备  采用分页查询所有设备信息
    @GetMapping("/findDevicePage/{fid}")
    public Result findDevicePage(@RequestParam(value = "pageNum",required = false,defaultValue = "1")String pageNum,
                                 @RequestParam(value = "pageSize",required = false,defaultValue = "2")String pageSize,
                                 @PathVariable("fid")Integer fid){
        try {
            if (StringUtils.isEmpty(pageNum)){
                pageNum = "1";
            }
            if (StringUtils.isEmpty(pageSize)){
                pageSize = "2";
            }
            Page<FarmlandDevice> pageList = fieldService.findPage(fid,pageNum, pageSize);
            PageResult pageResult = new PageResult(pageList.getTotal(),pageList.getResult());
            return new Result(true,StatusCode.OK,"查询大田设备成功",pageResult);
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false,StatusCode.ERROR,"查询大田设备失败");
        }
    }

    //分页条件查询
    @GetMapping("/search/{page}/{size}")
    public Result search(@RequestParam Map searchMap,@PathVariable String page,@PathVariable String size){
        try{
            Page<FarmlandDevice> devicePage = fieldService.queryPage(searchMap,page,size);
            PageResult pageResult = new PageResult(devicePage.getTotal(),devicePage.getResult());
            return new Result(true,StatusCode.OK,"条件查询成功",pageResult);
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false,StatusCode.ERROR,"条件查询失败");
        }
    }



    // 根据大田的id去 显示设备的起停状态, 并自动设置设备运行状态
    @GetMapping("/deviceStage/{fid}")
    public Result deviceStage(@PathVariable("fid")Integer fid){
        try {
            List<Map> resultList = fieldService.deviceStage(fid);
            return new Result(true,StatusCode.OK,"查询大田设备状态成功",resultList);
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false,StatusCode.ERROR,"查询大田设备状态失败");
        }
    }

    //设备手动开启
    @PutMapping("/deviceStart")
    public Result deviceStart(@RequestBody Map deviceMap){
        try {
            fieldService.deviceStart(deviceMap);
            return new Result(true,StatusCode.OK,"手动开启设备成功");
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false,StatusCode.ERROR,"手动开启设备失败");
        }
    }


    //设备手动关闭
    @PutMapping("/deviceStop")
    public Result deviceStop(@RequestBody Map deviceMap){
        try {
            fieldService.deviceStop(deviceMap);
            return new Result(true,StatusCode.OK,"手动关闭设备成功");
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false,StatusCode.ERROR,"手动关闭设备失败");
        }
    }

    //显示某个设备的详细信息 包括设备的数据，设备检测农作物数据
    @GetMapping("/deviceDetail/{did}")
    public Result deviceDetail(@PathVariable("did")Integer did){
        try {
            Map resultMap = fieldService.deviceDetail(did);
            return new Result(true,StatusCode.OK,"查询设备详情成功",resultMap);
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false,StatusCode.ERROR,"查询设备详情失败");
        }
    }
}
