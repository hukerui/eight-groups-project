package com.gushenxing.farm.dao;

import com.gushenxing.farm.pojo.CustFarmlandCrops;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author Swift
 * @date 2020/10/12
 */

@Repository
public interface CustFarmlandCropsMapper extends Mapper<CustFarmlandCrops> {
    @Select("SELECT * FROM t_cust_farmland_crops WHERE fid=#{id}")
    CustFarmlandCrops findNameById(@Param("id") Integer id);
}
