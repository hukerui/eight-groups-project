package com.gushenxing.farm.service;

import com.github.pagehelper.Page;
import com.gushenxing.farm.pojo.TCustFarm;

public interface AreaService {

    //根据农场主ID分页查询该农场主下面所有的片区信息
    Page<TCustFarm> findArea(Integer custId, String pageNum, String pageSize);

   //根据片区ID进行查询片区信息
    TCustFarm findById(Integer id);

   //添加农场片区
    void areaAdd(TCustFarm tCustFarm);

    //修改农场信息
    void areaUpdate(TCustFarm tCustFarm);
}
