package com.gushenxing.service.impl;

import com.gushenxing.crops.pojo.TIndex;
import com.gushenxing.dao.IndexMappper;
import com.gushenxing.service.IndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IndexServiceImpl implements IndexService {
    @Autowired
    private IndexMappper indexMappper;
    /**
     * 数据指标添加
     * @param tIndex
     */
    @Override
    public void addIndex(TIndex tIndex) {
        indexMappper.insertSelective(tIndex);
    }

    @Override
    public void deleteIndex(Integer id) {
        indexMappper.deleteByPrimaryKey(id);
    }

    @Override
    public void updateIndex(TIndex tIndex) {
        indexMappper.updateByPrimaryKey(tIndex);
    }

    @Override
    public List<TIndex> findAllIndex() {
        return indexMappper.selectAll();
    }

    @Override
    public void deleteIndexByName(String name) {
        indexMappper.deleteIndexByName(name);
    }
}
