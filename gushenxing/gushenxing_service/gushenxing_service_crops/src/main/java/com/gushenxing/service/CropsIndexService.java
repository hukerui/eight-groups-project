package com.gushenxing.service;

import com.gushenxing.commom.pojo.PageResult;
import com.gushenxing.commom.pojo.QueryPageBean;
import com.gushenxing.crops.pojo.TCropsIndex;

import java.util.List;

public interface CropsIndexService {
    List<TCropsIndex> findAll();

    /**
     * 分页条件查询
     * @param queryPageBean
     * @return
     */
    PageResult findPage(QueryPageBean queryPageBean);

    /**
     * 根据id删除
     * @param name
     */
    void delete(String name);

    /**
     * 修改
     * @param tCropsIndex
     */
    void update(TCropsIndex tCropsIndex);


}
