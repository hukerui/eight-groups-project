package com.gushenxing.service;

import com.gushenxing.commom.pojo.PageResult;
import com.gushenxing.commom.pojo.QueryPageBean;
import com.gushenxing.crops.pojo.TCropsDisease;

import java.util.List;

public interface CropsDiseaseService {
    List<TCropsDisease> findAll();

    /**
     * 分页条件查询
     * @param queryPageBean
     * @return
     */
    PageResult findPage(QueryPageBean queryPageBean);

    void delete(String name);

    void update(TCropsDisease tCropsDisease);
}
