package com.gushenxing.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.gushenxing.commom.pojo.PageResult;
import com.gushenxing.commom.pojo.QueryPageBean;
import com.gushenxing.crops.pojo.TCropsIndex;
import com.gushenxing.dao.CropsIndexMapper;
import com.gushenxing.service.CropsIndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CropsIndexServiceImpl implements CropsIndexService {
    @Autowired
    private CropsIndexMapper cropsIndexMapper;

    /**
     * 查询所有数据
     * @return
     */
    @Override
    public List<TCropsIndex> findAll() {
        return cropsIndexMapper.selectAll();
    }

    /**
     * 分页条件查询
     * @param queryPageBean
     * @return
     */
    @Override
    public PageResult findPage(QueryPageBean queryPageBean) {
        Integer currentPage = queryPageBean.getCurrentPage();
        Integer pageSize = queryPageBean.getPageSize();
        String queryString = queryPageBean.getQueryString();//查询条件
        //完成分页查询，基于mybatis框架提供的分页助手插件完成
        PageHelper.startPage(currentPage,pageSize);
        Page<TCropsIndex> page=cropsIndexMapper.selectByCondition(queryString);
        long total = page.getTotal();
        List<TCropsIndex> result = page.getResult();
        return new PageResult(total,result);
    }

    /**
     * 根据name删除
     * @param name
     */
    @Override
    public void delete(String name) {
        cropsIndexMapper.deleteCropsIndexById(name);
    }

    @Override
    public void update(TCropsIndex tCropsIndex) {
        cropsIndexMapper.updateByPrimaryKey(tCropsIndex);
    }



}
