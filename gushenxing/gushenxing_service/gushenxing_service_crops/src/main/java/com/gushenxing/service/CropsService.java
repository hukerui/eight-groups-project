package com.gushenxing.service;


import com.gushenxing.commom.pojo.PageResult;
import com.gushenxing.commom.pojo.QueryPageBean;
import com.gushenxing.crops.pojo.TCrops;

import java.util.List;
import java.util.Map;

public interface CropsService {
    /**
     * 查询全部数据
     * @return
     */
    List<TCrops> findAll();

    /**
     * 根据农作物id查询
     * @param name
     * @return
     */
    List<TCrops> findOne(String name);

    /**
     * 新增数据
     * @param
     */

    void add( Map map);

    /**
     * 编辑数据
     * @param map
     */
    void update(Integer id, Map map);

    /**
     * 根据ID删除数据
     * @param name
     */
    void delete(String name);

    /**
     * 分页条件查询
     * @param queryPageBean
     * @return
     */
    PageResult findPage(QueryPageBean queryPageBean);



}
