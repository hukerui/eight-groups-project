package com.gushenxing.controller;

import com.gushenxing.commom.pojo.PageResult;
import com.gushenxing.commom.pojo.QueryPageBean;
import com.gushenxing.commom.pojo.Result;
import com.gushenxing.commom.pojo.StatusCode;
import com.gushenxing.crops.pojo.TCropsIndex;
import com.gushenxing.crops.pojo.TIndex;
import com.gushenxing.service.CropsIndexService;
import com.gushenxing.service.IndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/index")
public class CropsIndexController {
    @Autowired
    private CropsIndexService cropsIndexService;
    @Autowired
    private IndexService indexService;
    /**
     * 查询全部数据
     *
     * @return
     */
    @GetMapping("/findAll")
    public Result findAll() {
        List<TCropsIndex> cropsIndexList = cropsIndexService.findAll();
        return new Result(true, StatusCode.OK, "查询全部成功", cropsIndexList);
    }

    /**
     * 分页条件查询
     */
    @GetMapping("/findPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean) {
        return cropsIndexService.findPage(queryPageBean);
    }

    @DeleteMapping(value = "/delete/{name}")
    public Result delete(@PathVariable String name) {
        cropsIndexService.delete(name);
        return new Result(true, StatusCode.OK, "删除成功");
    }
    /*
     * 修改数据
     * @return
     */
    @PutMapping(value="/update/{id}")
    public Result update(@RequestBody TCropsIndex tCropsIndex, @PathVariable Integer id){
        tCropsIndex.setId(id);
        cropsIndexService.update(tCropsIndex);
        return new Result(true,StatusCode.OK,"修改成功");
    }

    /**
     * 数据指标添加
     * @param tIndex
     * @return
     */
    @PostMapping("/addIndex")
    public Result addIndex(@RequestBody TIndex tIndex){
            indexService.addIndex(tIndex);
            return new Result(true,20000,"数据指标添加成功");
        }
    /**
     * 数据指标删除
     */
    @DeleteMapping("/deleteIndex/{id}")
    public Result deleteIndex(@PathVariable Integer id) {
        indexService.deleteIndex(id);
        return new Result(true,20000,"数据指标删除成功");
    }

    /**
     * 数据指标软删除
     * @param name
     * @return
     */
    @DeleteMapping("/deleteIndexByName/{name}")
    public Result deleteIndexByName(@PathVariable String name) {
        indexService.deleteIndexByName(name);
        return new Result(true,20000,"删除成功");
    }

    @PutMapping("/updateIndex")
    public Result updateIndex(@RequestBody TIndex tIndex,Integer id){
        tIndex.setId(id);
        indexService.updateIndex(tIndex);
        return new Result(true,20000,"数据指标新增成功");
    }

    @GetMapping("/findAllIndex")
    public Result findAllIndex(){
        List<TIndex> indexList=indexService.findAllIndex();
        return new Result(true,20000,"查询成功",indexList);
    }
    }

