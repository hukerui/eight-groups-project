package com.gushenxing.controller;

import com.gushenxing.commom.pojo.PageResult;
import com.gushenxing.commom.pojo.QueryPageBean;
import com.gushenxing.commom.pojo.Result;
import com.gushenxing.commom.pojo.StatusCode;
import com.gushenxing.crops.pojo.TCrops;
import com.gushenxing.service.CropsDiseaseService;
import com.gushenxing.service.CropsIndexService;
import com.gushenxing.service.CropsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/crops")
public class CropsController {
  /*  用户点击农作物管理时，对数据库农作物表查询所有，先判断农作物状态是否需要表示出来，再将表中所有的农作物的信息通过分页查询展示在前台
    关键字动态查询：农场主在输入框输入农作物、当前地区、农作物序号等关键字时，对后台数据库进行分页查询，展示结果在前台
    添加功能：后台管理员添加新农作物物种，并关联数据表数据指标表，添加数据指标到农作物数据指标中间表中；
    编辑功能：根据农作物序号修改农作物对应的介绍信息
    删除功能（软删除）：当用户点击删除时  将表中状态改为不上线状态*/
    @Autowired
    private CropsService cropsService;
    @Autowired
    private CropsIndexService cropsIndexService;
    @Autowired
    private CropsDiseaseService cropsDiseaseService;

    /**
     * 查询全部数据
     * @return
     */
    @GetMapping("/findAll")
    public Result findAll(){
        List<TCrops> cropsList = cropsService.findAll();
        return new Result(true, StatusCode.OK,"查询成功",cropsList) ;
    }

    /**
     * 根据id查询数据
     * @param  name
     * @return
     */
    @GetMapping("/findById/{name}")
    public Result findById(@PathVariable String name){
        try {
            List<TCrops> tCrops = cropsService.findOne(name);
            return new Result(true, StatusCode.OK,"查询成功",tCrops);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, StatusCode.ERROR,"查询失败");
        }

    }
    /***
     * 新增数据,农作物添加功能
     * @param map
     * @return
     */
    @PostMapping("/add")
    public Result add(@RequestBody Map map){
        cropsService.add(map);
        return new Result(true,StatusCode.OK,"添加成功");
    }

 /*
     * 修改数据
     * @param orderItem
     * @param id
     * @return
     */
  @PutMapping(value="/update/{id}")
    public Result update(@PathVariable Integer id,@RequestBody Map map){
        cropsService.update(id,map);
        return new Result(true,StatusCode.OK,"修改成功");
    }

    @DeleteMapping(value="/delete/{name}")
    public Result delete(@PathVariable String name) {
        cropsService.delete(name);
        return new Result(true, StatusCode.OK,"删除成功");
    }
/*
    *//***
     * 根据ID删除品牌数据
     * @param id
     * @return
     *//*
    @DeleteMapping(value = "/{id}" )
    public Result delete(@PathVariable String id){
        orderItemService.delete(id);
        return new Result(true,StatusCode.OK,"删除成功");
    }*/

    /***
     * 分页条件查询
     * @return
     */
    @GetMapping("/findPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean){
        return cropsService.findPage(queryPageBean);
    }
}
