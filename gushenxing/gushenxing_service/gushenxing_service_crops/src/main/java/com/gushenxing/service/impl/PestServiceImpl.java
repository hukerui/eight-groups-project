package com.gushenxing.service.impl;

import com.gushenxing.crops.pojo.TPest;
import com.gushenxing.dao.PestMapper;
import com.gushenxing.service.PestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PestServiceImpl implements PestService{
    @Autowired
    private PestMapper pestMapper;

    @Override
    public void addPest(TPest tPest) {
        pestMapper.insertSelective(tPest);
    }

    @Override
    public void deletePest(Integer id) {
        pestMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void updatePest(TPest tPest) {
        pestMapper.updateByPrimaryKeySelective(tPest);
    }

    @Override
    public List<TPest> findAllPest() {
        return pestMapper.selectAll();
    }
}
