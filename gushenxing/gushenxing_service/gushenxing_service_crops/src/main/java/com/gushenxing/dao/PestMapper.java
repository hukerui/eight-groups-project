package com.gushenxing.dao;

import com.gushenxing.crops.pojo.TPest;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@Repository
public interface PestMapper extends Mapper<TPest> {
    @Select("select * from t_pest where id in (select pid from t_crops_disease where cid=#{id})")
    List<TPest> findPestByCid(Integer id);
}
