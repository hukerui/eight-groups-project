package com.gushenxing.service;

import com.gushenxing.crops.pojo.TPest;

import java.util.List;

public interface PestService {
    void addPest(TPest tPest);

    void deletePest(Integer id);

    void updatePest(TPest tPest);

    List<TPest> findAllPest();
}
