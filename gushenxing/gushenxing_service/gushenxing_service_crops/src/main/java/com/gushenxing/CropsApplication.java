package com.gushenxing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@EnableEurekaClient
@MapperScan(basePackages = {"com.gushenxing.dao"})
public class CropsApplication {
    public static void main(String[] args) {
        SpringApplication.run(CropsApplication.class,args);
    }
}
