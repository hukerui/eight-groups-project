package com.gushenxing.dao;

import com.gushenxing.crops.pojo.TIndex;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@Repository
public interface IndexMappper extends Mapper<TIndex> {
    @Select("select * from t_index where id in (select iid from t_crops_index where cid=#{id})")
    List<TIndex> findIndexByCid(Integer id);
    @Update("update t_index set status=0 where name=#{name}")
    void deleteIndexByName(String name);
}
