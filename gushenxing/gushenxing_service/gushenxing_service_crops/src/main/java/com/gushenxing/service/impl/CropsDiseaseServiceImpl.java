package com.gushenxing.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.gushenxing.commom.pojo.PageResult;
import com.gushenxing.commom.pojo.QueryPageBean;
import com.gushenxing.crops.pojo.TCropsDisease;

import com.gushenxing.dao.CropsDiseaseMapper;

import com.gushenxing.service.CropsDiseaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CropsDiseaseServiceImpl implements CropsDiseaseService {
    @Autowired
    private CropsDiseaseMapper cropsDiseaseMapper;

    /**
     * 查询所有数据
     * @return
     */
    @Override
    public List<TCropsDisease> findAll() {
        return cropsDiseaseMapper.selectAll();
    }

    @Override
    public PageResult findPage(QueryPageBean queryPageBean) {
        Integer pageSize = queryPageBean.getPageSize();
        Integer currentPage = queryPageBean.getCurrentPage();
        String queryString = queryPageBean.getQueryString();//查询条件
        //完成分页查询，基于mybatis框架提供的分页助手插件完成
        PageHelper.startPage(currentPage,pageSize);
        Page<TCropsDisease> page =cropsDiseaseMapper.selectByCondition(queryString);
        long total = page.getTotal();
        List<TCropsDisease> result = page.getResult();
        return new PageResult(total,result);
    }

    /**
     * 软删除
     * @param name
     */
    @Override
    public void delete(String name) {
        cropsDiseaseMapper.deleteDiseaseById(name);
    }

    @Override
    public void update(TCropsDisease tCropsDisease) {
        cropsDiseaseMapper.updateByPrimaryKey(tCropsDisease);
    }
}
