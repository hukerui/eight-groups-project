package com.gushenxing.controller;

import com.gushenxing.commom.pojo.PageResult;
import com.gushenxing.commom.pojo.QueryPageBean;
import com.gushenxing.commom.pojo.Result;
import com.gushenxing.commom.pojo.StatusCode;
import com.gushenxing.crops.pojo.TCropsDisease;

import com.gushenxing.crops.pojo.TCropsIndex;
import com.gushenxing.crops.pojo.TIndex;
import com.gushenxing.crops.pojo.TPest;
import com.gushenxing.service.CropsDiseaseService;
import com.gushenxing.service.PestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/disease")
public class CropsDiseaseController {
    @Autowired
    private CropsDiseaseService cropsDiseaseService;
    @Autowired
    private PestService pestService;


    /**
     * 查询全部数据
     *
     * @return
     */
    @GetMapping("/findAll")
    public Result findAll() {
        List<TCropsDisease> cropsDiseasesList = cropsDiseaseService.findAll();
        return new Result(true, StatusCode.OK, "查询全部成功", cropsDiseasesList);
    }

    /**
     * 分页条件查询
     */
    @GetMapping("/findPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean) {
        return cropsDiseaseService.findPage(queryPageBean);
    }

    @DeleteMapping("/delete/{name}")
    public Result delete(@PathVariable String name) {
        cropsDiseaseService.delete(name);
        return new Result(true, StatusCode.OK, "删除成功");
    }
    /*
     * 修改数据
     * @return
     */
    @PutMapping(value="/update/{id}")
    public Result update(@RequestBody TCropsDisease tCropsDisease, @PathVariable Integer id){
        tCropsDisease.setId(id);
        cropsDiseaseService.update(tCropsDisease);
        return new Result(true,StatusCode.OK,"修改成功");
    }
    /**
     * 病虫添加
     * @param
     * @return
     */
    @PostMapping("/addPest")
    public Result addPest(@RequestBody TPest tPest){
        pestService.addPest(tPest);
        return new Result(true,20000,"病虫添加成功");
    }
    /**
     * 病虫删除
     */
    @DeleteMapping("/deletePest/{id}")
    public Result deletePest(@PathVariable Integer id) {
        pestService.deletePest(id);
        return new Result(true,20000,"病虫删除成功");
    }

    /**
     * 病虫编辑
     * @param tPest
     * @param id
     * @return
     */
    @PutMapping("/updatePest")
    public Result updatePest(@RequestBody TPest tPest ,Integer id){
        tPest.setId(id);
        pestService.updatePest(tPest);
        return new Result(true,20000,"病虫害修改");
    }

    /**
     * 病虫全部查询
     * @return
     */
    @GetMapping("/findAllPest")
    public Result findAllPest(){
        List<TPest> pestList=pestService.findAllPest();
        return new Result(true,20000,"病虫查询成功",pestList);
    }
}
