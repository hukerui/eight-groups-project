package com.gushenxing.dao;

import com.github.pagehelper.Page;
import com.gushenxing.crops.pojo.TCropsDisease;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;
@Repository
public interface CropsDiseaseMapper extends Mapper<TCropsDisease> {

    /**
     * 分页条件查询
     * @param queryString
     * @return
     */
    @Select("<script>select * from t_crops_disease <if test=\"queryString!=null and queryString!=''\">where pest=#{queryString} or aspect=#{queryString} or crops=#{queryString} or id=#{queryString}</if></script>")
    Page<TCropsDisease> selectByCondition(@Param("queryString") String queryString);

    /**
     * 软删除
     * @param name
     */
    @Update("update t_crops_disease set status= 0 where cid in (select id from t_crops where name=#{name})")
    void deleteDiseaseById(String name);

}
