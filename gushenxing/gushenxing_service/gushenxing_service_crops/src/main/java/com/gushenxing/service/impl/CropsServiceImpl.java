package com.gushenxing.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.gushenxing.commom.pojo.PageResult;
import com.gushenxing.commom.pojo.QueryPageBean;
import com.gushenxing.crops.pojo.TCrops;
import com.gushenxing.crops.pojo.TIndex;
import com.gushenxing.crops.pojo.TPest;
import com.gushenxing.dao.*;
import com.gushenxing.service.CropsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class CropsServiceImpl implements CropsService {
    @Autowired
    private CropsMapper cropsMapper;
    @Autowired
    private PestMapper pestMapper;
    @Autowired
    private IndexMappper indexMappper;
    @Autowired
    private CropsIndexMapper cropsIndexMapper;
    @Autowired
    private CropsDiseaseMapper cropsDiseaseMapper;
    /**
     * 查询所有信息
     * @return
     */
    @Override
    public List<TCrops> findAll() {
        List<TCrops> tCrops = cropsMapper.selectAll();
        packageCrops(tCrops);
        return tCrops;
    }
    //crops实体封装方法
    private List<TCrops> packageCrops(List<TCrops> cropsList) {
        for (TCrops tCrops : cropsList) {
            List<TIndex> indexList =indexMappper.findIndexByCid(tCrops.getId());
            List<TPest> pestList = pestMapper.findPestByCid(tCrops.getId());
            tCrops.setIndexList(indexList);
            tCrops.setPestList(pestList);
        }
        return cropsList;
    }
/*
* //查询所有农作物
    @Override
    public List<Crops> findAll() {
        List<Crops> cropsList = cropsMapper.selectAll();
        packageCrops(cropsList);
        return cropsList;
    }

    //crops实体封装方法
    private List<Crops> packageCrops(List<Crops> cropsList) {
        for (Crops crops : cropsList) {
            List<Index> indexList = indexMapper.findIndexByCid(crops.getId());
            List<Pest> pestList = pestMapper.findPestByCid(crops.getId());
            crops.setIndexList(indexList);
            crops.setPestList(pestList);
        }
        return cropsList;
    }*/
    /**
     * 根据id查询农作物信息
     * @param name
     * @return
     */
    @Override
    public List<TCrops> findOne(String name) {
      /*  Example example = new Example(TCrops.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("name",name );
        List<TCrops> tCrops = cropsMapper.selectByExample(example);*/
        TCrops tCrops = new TCrops();
        tCrops.setName(name);
        List<TCrops> select = cropsMapper.select(tCrops);
        packageCrops(select);
        return select;
    }


    /**
     * 新增数据
     * @param
     * map
     */
    @Override
    public void add( Map map) {
       if (map !=null&& map.size()>0){
           TCrops tCrops = new TCrops();
           //新增农作物必须信息
           if(map.get("name") !=null){
               tCrops.setName((String) map.get("name"));
           }
           //新增农作物信息介绍
           if(map.get("info") !=null){
               tCrops.setInfo((String) map.get("info"));
           }
           //新增农作物外观
           if(map.get("aspect") !=null){
               tCrops.setAspect((String) map.get("aspect"));
           }
           //新增农作物栽培技术
           if(map.get("cultivation") !=null){
               tCrops.setCultivation((String) map.get("cultivation"));
           }
           //新增农作物生命阶段
           if(map.get("stage") !=null){
               tCrops.setStage((String) map.get("stage"));
           }
           //新增农作物非必须信息
           //新增农作物别名
           tCrops.setAlias((String) map.get("alias"));
           //新增农作物科属
           tCrops.setCdoosgs((String) map.get("cdoosgs"));
           //新增农作物分布地区
           tCrops.setArea((String) map.get("area"));
           //新增农作物注意事项
           tCrops.setAttention((String) map.get("attention"));
           //默认新增农作物图片
           tCrops.setImage((String) map.get("image"));
           //默认新增农作物为上线状态
           tCrops.setStatus(1);
           //添加农作物的基本信息
           cropsMapper.insertSelective(tCrops);

           //设置农作物以及数据指标中间表的信息
           setCropsAndIndex((List<Map<String, String>>) map.get("indexList"),tCrops);
           //设置农作物以及病虫害表的信息
           setCropsAndDisease((List<Map<String, String>>) map.get("diseaseList"),tCrops);
       }

    }

    private void setCropsAndIndex(List<Map<String, String>> indexList, TCrops tCrops) {
        if (indexList !=null && indexList.size()>0){
            for (Map<String,String> index : indexList) {
                Map<String,Object> map = new HashMap<>();
                //添加中间表
                map.put("cid",tCrops.getId());
                map.put("iid",index.get("iid"));
                map.put("stage",tCrops.getStage());
                map.put("max_range",index.get("max_range"));
                map.put("min_range",index.get("min_range"));
                map.put("opt_range",index.get("opt_range"));
                map.put("status",tCrops.getStatus());
                cropsMapper.setCropsAndIndex(map);
            }
        }
    }


    /**
     *编辑数据
     * @param map
     */
    @Override
    public void update(Integer id, Map map) {
      //封装农作物信息
        if(map !=null&&map.size() > 0){
            TCrops tCrops = new TCrops();
            tCrops.setId(id);
            //更新农作物必须信息
            //更新农作物名称
            if (map.get("name") != null) {
                tCrops.setName((String) map.get("name"));
            }
            //更新农作物信息介绍
            if (map.get("info") != null) {
                tCrops.setInfo((String) map.get("info"));
            }
            //更新农作物外观
            if (map.get("aspect") != null) {
                tCrops.setAspect((String) map.get("aspect"));
            }
            //更新农作物栽培技术
            if (map.get("cultivation") != null) {
                tCrops.setCultivation((String) map.get("cultivation"));
            }
            //新增农作物生命阶段
            if (map.get("stage") != null) {
                tCrops.setStage((String) map.get("stage"));
            }
            //更新农作物非必须信息
            //更新农作物别名
            tCrops.setAlias((String) map.get("alias"));
            tCrops.setCdoosgs((String) map.get("cdoosgs"));//更新农作物科属
            tCrops.setArea((String) map.get("area"));//更新农作物分布地区
            tCrops.setAttention((String) map.get("attention"));//更新农作物注意事项
            tCrops.setImage((String) map.get("image"));//更新农作物图片
            tCrops.setStatus(1);//默认更新农作物为上线状态
            cropsMapper.updateByPrimaryKey(tCrops);
            //更新农作物和指标信息
            //删除农作物数据指标中间表关系
            cropsMapper.deleteIndexByCid(id);
            //删除农作物数据指标中间表关系
            cropsMapper.deleteDiseaseByCid(id);
            //设置农作物以及数据指标表中间表信息
            setCropsAndIndex((List<Map<String, String>>) map.get("indexList"),tCrops);
            //添加新的农作物与病害虫中间表信息
            setCropsAndDisease((List<Map<String, String>>) map.get("diseaseList"),tCrops);
        }
    }


    /**
     * 根据ID删除数据
     * @param name
     */
    @Override
    public void delete(String name) {
        cropsMapper.deleteCropsById(name);
        cropsIndexMapper.deleteCropsIndexById(name);
        cropsDiseaseMapper.deleteDiseaseById(name);
    }

    /**
     * 多条件分页查询
     * @param queryPageBean
     * @return
     */
    @Override
    public PageResult findPage(QueryPageBean queryPageBean) {
        String queryString = queryPageBean.getQueryString();
        Integer currentPage = queryPageBean.getCurrentPage();
        Integer pageSize = queryPageBean.getPageSize();
        //完成分页查询，基于mybatis框架提供的分页助手插件完成
        PageHelper.startPage(currentPage,pageSize);
        Page<TCrops> page= cropsMapper.selectByCondition(queryString);
        long total = page.getTotal();
        List<TCrops> result=page.getResult();
        packageCrops(result);
        return new PageResult(total,result);
    }

    private void setCropsAndDisease(List<Map<String,String>> diseaseList, TCrops tCrops) {
        if (diseaseList !=null && diseaseList.size()>0){
            for (Map<String,String> pestInfo : diseaseList) {
                TPest pest = pestMapper.selectByPrimaryKey(pestInfo.get("pid"));
                Map<String,Object> map = new HashMap<>();
                //添加中间表
                map.put("cid",tCrops.getId());
                map.put("pid",pestInfo.get("pid"));
                map.put("crops",tCrops.getName());
                map.put("pest",pest.getPest());
                map.put("stage",tCrops.getStage());
                map.put("disease_range",pestInfo.get("disease_range"));
                map.put("normal_range",pestInfo.get("normal_range"));
                map.put("cure",pestInfo.get("cure"));
                map.put("status",tCrops.getStatus());
                cropsMapper.setCropsAndDisease(map);
            }
    }


    }}
