package com.gushenxing.dao;


import com.github.pagehelper.Page;
import com.gushenxing.crops.pojo.TCrops;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

@Repository
public interface CropsMapper extends Mapper<TCrops> {
    /*软删除*/
    @Update("update t_crops set status=0 where name=#{name}")
    void deleteCropsById(String name);
    /*分页多条件查询*/
    @Select("<script>select * from t_crops <if test=\"queryString!=null and queryString!=''\">where id=#{queryString} or name=#{queryString} or alias=#{queryString} or area=#{queryString}</if> </script>")
    Page<TCrops> selectByCondition(@Param("queryString") String queryString);
    @Insert("insert into t_crops_index values(null,#{cid},#{iid},#{stage},#{max_range},#{min_range},#{opt_range},#{status})")
    void setCropsAndIndex(Map<String, Object> map);
    @Insert("insert into t_crops_disease values(null,#{cid},#{pid},#{crops},#{pest},#{aspect},#{disease_range},#{normal_range},#{cure},#{stage},#{status})")
    void setCropsAndDisease(Map<String, Object> map);
    @Delete("delete from t_crops_index where cid=#{id}")
    void deleteIndexByCid(Integer id);
    @Delete("delete from t_crops_disease where cid=#{cid}")
    void deleteDiseaseByCid(Integer id);
}
