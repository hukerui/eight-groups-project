package com.gushenxing.service;

import com.gushenxing.crops.pojo.TIndex;

import java.util.List;

public interface IndexService {
    void addIndex(TIndex tIndex);

    void deleteIndex(Integer id);

    void updateIndex(TIndex tIndex);

    List<TIndex> findAllIndex();

    void deleteIndexByName(String name);
}
