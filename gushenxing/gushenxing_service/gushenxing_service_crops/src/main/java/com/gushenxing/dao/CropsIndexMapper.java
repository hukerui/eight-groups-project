package com.gushenxing.dao;

import com.github.pagehelper.Page;
import com.gushenxing.crops.pojo.TCropsIndex;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;
@Repository
public interface CropsIndexMapper extends Mapper<TCropsIndex> {
    /*不确定条件查询*/
    @Select("<script>select * from t_crops_index <if test=\"queryString!=null and queryString!=''\">where id=#{queryString} or cid=#{queryString} </if></script>")
    Page<TCropsIndex> selectByCondition(@Param("queryString")String queryString);

    /**
     * 软删除，改变字符状态
     * @param name
     */
    @Update("update t_crops_index set status= 0 where cid in (select id from t_crops where name=#{name})")
    void deleteCropsIndexById(String name);
}
