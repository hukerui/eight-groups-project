package com.gushenxing.order.service;

import com.github.pagehelper.Page;
import com.gushenxing.commom.pojo.PageResult;
import com.gushenxing.device.pojo.Buy;
import com.gushenxing.devices.pojo.Device;

import java.util.List;
import java.util.Map;

public interface BuyService {

    //查询所有订单
    List<Buy> selectAll();

    //分页查询
    PageResult findPage(Integer size , Integer page);

    //模糊查询
    Page<Device> findPage(Map<String,Object>map , Integer size, Integer page);

    //添加订单数据
    void addShop(String deviceId,Integer num,String custId,String orderId);

    //修改订单状态
    void update();
}
