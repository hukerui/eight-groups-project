package com.gushenxing.order.controller;

import com.github.pagehelper.Page;
import com.gushenxing.commom.pojo.PageResult;
import com.gushenxing.commom.pojo.Result;
import com.gushenxing.commom.pojo.StatusCode;
import com.gushenxing.device.pojo.Buy;
import com.gushenxing.devices.pojo.Device;
import com.gushenxing.order.dao.BuyMapper;
import com.gushenxing.order.service.BuyService;
import com.sun.org.apache.regexp.internal.RE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/buy")
public class Buycontroller {
    @Autowired
    private BuyService buyService;


    /**
     * 查询所有订单
     * @return
     */
    @GetMapping("/findAll")
    public Result findAll(){
        List<Buy> buys = null;
        try {
            buys = buyService.selectAll();
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, StatusCode.ERROR,"查询失败");
        }
        return new Result(true,StatusCode.OK,"查询成功",buys);
    }

    /**
     * 分页搜索
     * @param page
     * @param size
     * @return
     */
    @GetMapping("/findPage/{page}/{size}")
    public Result findPage(@PathVariable Integer page, @PathVariable Integer size){
        try {
            PageResult page1 = buyService.findPage(page, size);
            return new Result(true,StatusCode.OK,"查询成功",page1);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,StatusCode.ERROR,"查询失败");
        }
    }


    /**
     * 模糊查询
     * @return
     */
    @GetMapping("/dimfindPage/{page}/{size}")
    public Result findPage(  @PathVariable Integer page, @PathVariable Integer size,@RequestParam Map<String,Object> map){
        PageResult pageResult= null;
        try {
            Page<Device> pageList = buyService.findPage(map, page, size);
            pageResult = new PageResult(pageList.getTotal(),pageList.getResult());
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(true,StatusCode.OK,"查询失败");
        }
        return new Result(true,StatusCode.OK,"查询成功",pageResult);
    }

    /**
     * 官方采购的添加设备
     * @param deviceId
     * @param num
     * @param custId
     * @param orderId
     * @return
     */
    @PostMapping("/addShop")
    public Result addShop(@RequestParam("deviceId") String deviceId,@RequestParam("num") Integer num,@RequestParam("custId") String custId,@RequestParam("orderId") String orderId){
        try {
            buyService.addShop(deviceId, num, custId,orderId);
            return new Result(true,StatusCode.OK,"添加成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,StatusCode.ERROR,"添加失败");
        }
    }
}
