package com.gushenxing.order.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.gushenxing.device.pojo.CustDevice;
import com.gushenxing.order.dao.CustDeviceMapper;
import com.gushenxing.order.service.CustDeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.HashMap;
import java.util.Map;

@Service
public class CustDeviceServiceImpl implements CustDeviceService {

    @Autowired
    private CustDeviceMapper custDeviceMapper;

    //分页查询
    @Override
    public Page<CustDevice> findPage(Map<String, Object> map, Integer size, Integer page) {
        PageHelper.startPage(size, page);
        Example example = createExample(map);
        return (Page<CustDevice>) custDeviceMapper.selectByExample(example);
    }

    //修改运行状态
    @Override
    public void update(Integer id) {
        CustDevice custDevice = custDeviceMapper.selectByPrimaryKey(id);
        if (custDevice == null) {
            throw new RuntimeException("设备不存在");
        }
        if ("1".equals(custDevice.getStatus())) {
            custDevice.setStatus("0");
        } else if ("0".equals(custDevice.getStatus())) {
            custDevice.setStatus("1");
        }
        custDeviceMapper.updateByPrimaryKeySelective(custDevice);
    }

    //删除设备
    @Override
    public void deleteById(Integer id) {
        custDeviceMapper.deleteByPrimaryKey(id);
    }

    //修改安装状态
    @Override
    public void update2(Integer id) {
        CustDevice custDevice = custDeviceMapper.selectByPrimaryKey(id);
        if ("1".equals(custDevice.getInstall())){
            custDevice.setStatus("0");
        }
        else if ("0".equals(custDevice.getInstall())){
            custDevice.setStatus("1");
        }
        custDeviceMapper.updateByPrimaryKeySelective(custDevice);
    }


    @Override
    public void updateByCustDeviceId(Integer cdid, String position) {

    }

    //增加设备
    @Override
    public void addDevice(Integer cid, Integer did, String code, String position) {
        Map<String, Object> map = new HashMap<>();
        map.put("cid", cid);
        map.put("did", did);
        map.put("code", code);
        map.put("status", 0);
        map.put("install", 0);
        map.put("position", position);
        custDeviceMapper.addDevice(map);

    }

    private Example createExample(Map<String, Object> map) {
        Example example = new Example(CustDevice.class);
        Example.Criteria criteria = example.createCriteria();
        if (map != null) {
            if (map.get("cid") != null && !"".equals(map.get("cid"))) {
                criteria.andEqualTo("cid", map.get("cid"));
            }
            if (map.get("did") != null && !"".equals(map.get("did"))) {
                criteria.andEqualTo("did", map.get("did"));
            }
        }
        return example;
    }
}
