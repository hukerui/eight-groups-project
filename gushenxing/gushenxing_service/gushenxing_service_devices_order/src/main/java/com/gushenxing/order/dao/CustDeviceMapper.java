package com.gushenxing.order.dao;

import com.gushenxing.device.pojo.CustDevice;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

@Repository
public interface CustDeviceMapper extends Mapper<CustDevice> {

    @Update("UPDATE t_cust_device SET `status` = #{status} WHERE `id` = #{id}")
    void  update(Integer id);

    @Insert("INSERT into t_cust_device (cid,did,code,status,install,position) VALUES(#{cid},#{did},#{code},#{status},#{install},#{position}) ")
    void addDevice(Map map);
}
