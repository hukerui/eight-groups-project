package com.gushenxing.order.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.gushenxing.commom.pojo.PageResult;
import com.gushenxing.device.pojo.Buy;
import com.gushenxing.devices.pojo.Device;
import com.gushenxing.farm.pojo.TCustFarm;
import com.gushenxing.order.dao.BuyMapper;
import com.gushenxing.order.dao.DeviceMapper;
import com.gushenxing.order.service.BuyService;
import com.gushenxing.system.pojo.TUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.text.SimpleDateFormat;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class BuyServiceImpl implements BuyService {

    @Autowired
    private DeviceMapper deviceMapper;

    @Autowired
    private BuyMapper buyMapper;

    @Override
    public List<Buy> selectAll() {
        return buyMapper.selectAll();
    }

    @Override
    public PageResult findPage(Integer size, Integer page) {
        PageHelper.startPage(page,size);
        Page<Device> devicepage = (Page<Device>) deviceMapper.selectAll();
        return new PageResult(devicepage.getTotal(),devicepage.getResult());
    }


    //模糊查询
    @Override
    public Page<Device> findPage(Map<String, Object> map, Integer size, Integer page) {
        PageHelper.startPage(size, page);
        Example example = createExample(map);
        return (Page<Device>) deviceMapper.selectByExample(example);
    }

    //添加设备
    @Override
    public void addShop(String deviceId, Integer num, String custId, String orderId) {
        //查询相关商品的信息
        Buy buy = buyMapper.selectByPrimaryKey(orderId);
        if (buy != null) {
            buy.setNum(buy.getNum() + num);
            //总金额,商品价格*数量
            // buy.setAmount(buy.getPrice()*buy.getNum());
            buyMapper.updateByPrimaryKey(buy);
        } else {
            //如果商品不存在我的设备中,添加数据
            Device device = deviceMapper.selectByPrimaryKey(deviceId);
            Buy buy1 = this.device2shop(device, num);
            buyMapper.insert(buy1);
            update();
        }
    }

    //订单完成
    @Override
    public void update() {
        //当走官方购买购买时,存在上门服务,如何确保订单完成
        //所有订单初始的status为0,当配送员送货上门以后修改为1, 展示给后台为0是未配送,1是已配送
        //默认购买即送达
        Buy buy = new Buy();
        buy.setStatus("1");
    }


    private Buy device2shop(Device device, Integer num) {
        TCustFarm tCustFarm = new TCustFarm();
        TUser tUser = new TUser();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = sdf.format(new Date());
        Timestamp ts = Timestamp.valueOf(format);
        Buy buy = new Buy();
        buy.setDid(device.getId());
        buy.setCid(tCustFarm.getId());
        buy.setPrice(device.getPrice());
        buy.setTel(tUser.getPhone());
        buy.setStatus("0");
        buy.setNum(num);
        return buy;
    }


    /**
     * 构建查询对象
     *
     * @param map
     * @return
     */
    private Example createExample(Map<String, Object> map) {
        Example example = new Example(Device.class);
        Example.Criteria criteria = example.createCriteria();
        if (map != null) {
            // 设备id
            if (map.get("id") != null && !"".equals(map.get("id"))) {
                criteria.andEqualTo("id", map.get("id"));
            }
            // 编码
            if (map.get("code") != null && !"".equals(map.get("code"))) {
                criteria.andEqualTo("code", map.get("code"));
            }
            //设备名称
            if (map.get("name") != null && !"".equals(map.get("name"))) {
                criteria.andLike("name", "%" + map.get("name") + "%");
            }
            //设备类型
            if (map.get("type") != null && !"".equals(map.get("type"))) {
                criteria.andLike("type", "%" + map.get("type") + "%");
            }
            //价格
            if (map.get("price") != null && !"".equals(map.get("price"))) {
                criteria.andLessThanOrEqualTo("price", map.get("price"));
            }
            //品牌brand
            if (map.get("brand") != null && !"".equals(map.get("brand"))) {
                criteria.andLike("brand", "%" + map.get("brand") + "%");
            }
            //型号
            if (map.get("model") != null && !"".equals(map.get("model"))) {
                criteria.andEqualTo("model", map.get("model"));
            }
            //应用场景
            if (map.get("scenarios") != null && !"".equals(map.get("scenarios"))) {
                criteria.andLike("scenarios", "%" + map.get("scenarios") + "%");
            }
        }
        return example;
    }
}
