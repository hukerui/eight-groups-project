package com.gushenxing.order.dao;

import com.gushenxing.device.pojo.Buy;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@Repository
public interface BuyMapper extends Mapper<Buy> {

}
