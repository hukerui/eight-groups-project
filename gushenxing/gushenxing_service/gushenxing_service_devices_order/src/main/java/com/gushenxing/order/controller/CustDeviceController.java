package com.gushenxing.order.controller;

import com.github.pagehelper.Page;
import com.gushenxing.commom.pojo.PageResult;
import com.gushenxing.commom.pojo.Result;
import com.gushenxing.commom.pojo.StatusCode;
import com.gushenxing.device.pojo.CustDevice;
import com.gushenxing.order.service.CustDeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/custdevice")
public class CustDeviceController {

    @Autowired
    private CustDeviceService custDeviceService;

    /**
     * 查询设备
     *
     * @param page
     * @param size
     * @param map
     * @return
     */
    @GetMapping("/dimfindPage/{page}/{size}")
    public Result findPage(@PathVariable Integer page, @PathVariable Integer size, @RequestParam Map<String, Object> map) {
        PageResult pageResult = null;
        try {
            Page<CustDevice> pageList = custDeviceService.findPage(map, page, size);
            pageResult = new PageResult(pageList.getTotal(), pageList.getResult());
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(true, StatusCode.OK, "查询失败");
        }
        return new Result(true, StatusCode.OK, "查询成功", pageResult);
    }

    /**
     * 删除设备
     *
     * @param id
     * @return
     */
    @DeleteMapping("/deleteById/{id}")
    public Result deleteById(@PathVariable Integer id) {
        try {
            custDeviceService.deleteById(id);
            return new Result(true, StatusCode.OK, "删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, StatusCode.ERROR, "删除失败");
        }
    }

    /**
     * 修改运行状态
     *
     * @param id
     * @return
     */
    @PutMapping("/update/{id}")
    public Result update(@PathVariable Integer id) {
        custDeviceService.update(id);
        return new Result(true, StatusCode.OK, "修改成功");
    }

    /**
     * 修改安装状态
     *
     * @param id
     * @return
     */
    @PutMapping("/update2/{id}")
    public Result update2(@PathVariable Integer id) {
        custDeviceService.update(id);
        return new Result(true, StatusCode.OK, "修改成功");
    }

    /**
     * 手动添加
     *
     * @return
     */
    @PostMapping("/addShop")
    public Result addShop(@RequestParam("cid") Integer cid, @RequestParam("did") Integer did, @RequestParam("code") String code, @RequestParam("position") String position) {
        try {
            custDeviceService.addDevice(cid, did, code, position);
            return new Result(true, StatusCode.OK, "添加成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, StatusCode.OK, "添加失败");
        }
    }

    /**
     * 根据客户的设备id修改安装状态以及设备位置
     *
     * @param cdid
     * @param position
     */
    @RequestMapping("/updateByCustDeviceId/{cdid}/{position}")
    public void updateByCustDeviceId(@PathVariable("cdid") Integer cdid, @PathVariable("position") String position) {
        custDeviceService.updateByCustDeviceId(cdid, position);
    }
}
