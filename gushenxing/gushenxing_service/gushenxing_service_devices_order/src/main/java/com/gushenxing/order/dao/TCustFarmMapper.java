package com.gushenxing.order.dao;

import com.gushenxing.farm.pojo.TCustFarm;
import tk.mybatis.mapper.common.Mapper;

public interface TCustFarmMapper extends Mapper<TCustFarm> {
}
