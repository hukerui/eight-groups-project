package com.gushenxing.order.service;

import com.github.pagehelper.Page;
import com.gushenxing.device.pojo.CustDevice;

import java.util.Map;

public interface CustDeviceService {
    /**
     * 分页查询
     * @param size
     * @param page
     * @return
     */
    Page<CustDevice> findPage(Map<String, Object> map, Integer size , Integer page);

    /**
     * 修改运行状态
     * @param
     */
    void update(Integer id);

    /**
     * 删除设备
     * @param id
     */
    void deleteById(Integer id);

    /**
     * 修改安装状态
     * @param id
     */
    void update2(Integer id);

    /**
     * 添加设备
     * @param cid
     * @param did
     * @param code
     */
    void addDevice(Integer cid,Integer did,String code,String position);

    /**
     * 根据客户的设备id修改安装状态以及设备位置
     * @param cdid
     * @param position
     */
    void updateByCustDeviceId(Integer cdid,String position);
}
