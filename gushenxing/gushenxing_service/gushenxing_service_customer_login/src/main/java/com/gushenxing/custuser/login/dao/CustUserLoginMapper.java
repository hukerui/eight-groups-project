package com.gushenxing.custuser.login.dao;

import com.gushenxing.custuser.pojo.CustUser;

import tk.mybatis.mapper.common.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import java.util.List;
import java.util.Map;

public interface CustUserLoginMapper extends Mapper<CustUser> {
    @Select("SELECT category FROM t_role WHERE id =(\n" +
            "SELECT rid id FROM t_cust_user_role WHERE cuid=#{cuid}\n" +
            ")")
    public List<Map>findCategoryByCuid(@Param("cuid")Integer cuid);
}
