package com.gushenxing.custuser.login.service.impl;

import com.gushenxing.custuser.login.dao.CustUserLoginMapper;
import com.gushenxing.custuser.login.service.CustUserLoginService;
import com.gushenxing.custuser.pojo.CustUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;
import org.springframework.data.redis.core.RedisTemplate;

@Service
public class CustUserLoginServiceImpl implements CustUserLoginService {
    @Autowired
    private CustUserLoginMapper custUserLoginMapper;

    @Autowired
    private RedisTemplate redisTemplate;


    public CustUser findUsernameAndPassword(String username, String password) {

        CustUser custUser = custuserRedisCount(username, password);
        if ("1".equals(custUser.getIsdelete())) {
            throw new RuntimeException("用户已被注销");
        }
        if ("-1".equals(custUser.getStatus())) {
            throw new RuntimeException("该用户已被锁定请联系超级管理员");
        }
        //判断是否是超级管理员
        if ("1".equals(custUserLoginMapper.findCategoryByCuid(custUser.getId()))) {
            //五次输入密码机会内输入正确登录成功，则rides中缓存内容删除
            redisTemplate.opsForHash().delete("redisCount", username);
            //在进行判断用户是否是第一次登录
            if ("0".equals(custUser.getStatus())) {
                throw new RuntimeException("请修改密码重新登录");
            }
        }

        return custUser;
    }

    /**
     * @param username 用户名
     * @param password 原密码
     * @param updatePassword 新密码
     * @return
     */
   @Override
    public Integer updateByPassword(String username, String password, String updatePassword) {
        Integer passwordStatus = null;
        //根据用户名，密码查询农业组织用户表
        CustUser custUser = custuserRedisCount(username,password);
        if ("0".equals(custUser.getIsdelete())){
            throw new RuntimeException("用户已被注销");
        }
        if ("-1".equals(custUser.getStatus())){
            throw new RuntimeException("该用户已被锁定请联系超级管理员");
        }
        //判断是不是管理员
        if ("0".equals(custUserLoginMapper.findCategoryByCuid(custUser.getId()))){
        //判断是否第一次登录
        if ("0".equals(custUser.getIsload())){
            custUser.setIsload("1");
            //修改密码
            custUser.setPassword("1");
            //激活用户
            custUser.setStatus("1");
            }
        }
        return passwordStatus;
    }

    /**
     * @param username 用户名
     * @param password 密码
     * @return
     */
    public CustUser custuserRedisCount(String username, String password) {
        //判断rides中是否存在目前登录的username
        if (!redisTemplate.opsForHash().hasKey("redisCount", username)) {
            //若查询不存在，则将count设置为0；
            int count = 0;
            redisTemplate.opsForHash().put("redisCount", username, count);
        }
        //获取redis里面存的值
        Integer redisCount = (Integer) redisTemplate.boundHashOps("redisCount").get(username);
        //自定义查询
        Example example = new Example(CustUser.class);
        Example.Criteria criteria = example.createCriteria();

        //用户名
        criteria.andEqualTo("username", username);

        //密码，加密储存
        String gensalt = BCrypt.gensalt();
        BCrypt.hashpw(password,gensalt);

        criteria.andEqualTo("password", password);

        //根据用户名和密码查询数据库
        CustUser custUser = custUserLoginMapper.selectOneByExample(example);

        //判断该用户是否为空
        if (custUser == null) {
            //将错误次数存入redis中
            redisTemplate.opsForHash().put("redisCount", username, ++redisCount);
            //再从redis中拿到存入的错误值
            redisCount = (Integer) redisTemplate.boundHashOps("redisCount").get(username);
            //当错误次数大于5次的时候就会锁定该用户
            if (redisCount >= 5) {
                //自定义查询
                Example exampleUsername = new Example(CustUser.class);
                Example.Criteria criteriaUsername = exampleUsername.createCriteria();
                // 查询找到该用户名对应的CustUser修改它的状态字段为-1表示该用户已被锁定
                criteriaUsername.andEqualTo("username", username);
                CustUser CU = custUserLoginMapper.selectOneByExample(exampleUsername);
                if (CU == null) {
                    throw new RuntimeException("该用户不存在");
                }

            }
            throw new RuntimeException("用户名或密码错误,您还有" + (5 - redisCount) + "次机会");
        }

        return custUser;
    }


}
