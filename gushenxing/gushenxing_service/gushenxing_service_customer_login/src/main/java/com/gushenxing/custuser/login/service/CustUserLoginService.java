package com.gushenxing.custuser.login.service;

import com.gushenxing.custuser.pojo.CustUser;


public interface CustUserLoginService {

     CustUser findUsernameAndPassword(String username, String password);

     Integer updateByPassword(String username, String password, String updatePassword);
}
