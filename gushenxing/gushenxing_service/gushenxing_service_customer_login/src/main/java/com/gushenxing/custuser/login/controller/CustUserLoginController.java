package com.gushenxing.custuser.login.controller;

import com.gushenxing.commom.pojo.Result;
import com.gushenxing.commom.pojo.StatusCode;
import com.gushenxing.custuser.login.service.CustUserLoginService;
import com.gushenxing.custuser.pojo.CustUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/custuser")
public class CustUserLoginController {
    @Autowired
    private CustUserLoginService custUserLoginService;

    @Autowired
    private RedisTemplate redisTemplate;
    //登录
    @PostMapping("/login")
    public Result login(@RequestParam("username")String username, @RequestParam("password")String password) {

        CustUser custUser = null;
        try {
            custUser = custUserLoginService.findUsernameAndPassword(username, password);
            //登录成功后将用户相关信息存入rides中
            redisTemplate.opsForHash().put("hashCustUser", username, custUser);
        } catch (RuntimeException e) {
            e.printStackTrace();
            return new Result(false, StatusCode.LOGIN_ERROER, "登录失败") {

            };
        }
        return new Result(true, StatusCode.LOGIN_OK, "登录成功");
    }
    //修改密码
    @PutMapping("/updatePassword")
    public Result updateByPassword(@RequestParam("username")String username,@RequestParam("password")String password,@RequestParam("updatePassword")String updatePassword){
        Integer passwordStatus = null;
        try{
        passwordStatus=custUserLoginService.updateByPassword(username,password,updatePassword);
        //修改失败
        if (passwordStatus==null){
            return new Result(false, StatusCode.UPDATE_ERROR,"修改失败");
            }
        }catch (RuntimeException e){
            e.printStackTrace();
        }
        return new Result(true,StatusCode.UPDATE_OK,"修改成功");
    }
    //退出登录
    @GetMapping("/loginOut")
    public Result loginout(@RequestParam("username")String username){
        redisTemplate.opsForHash().delete("hashCustUser",username);
        return new Result(true,StatusCode.LOGINOUT_OK,"退出登录成功");
    }
}
