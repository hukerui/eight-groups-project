import com.alibaba.fastjson.JSON;
import com.gushenxing.custuser.dao.CustUserMapper;
import com.gushenxing.custuser.pojo.CustUser;
import org.springframework.beans.factory.annotation.Autowired;

public class JsonTest {
    public static void main(String[] args) {
        CustUser custUser = new CustUser();
        String json = JSON.toJSONString(custUser);
        System.out.println(json);
    }
}
