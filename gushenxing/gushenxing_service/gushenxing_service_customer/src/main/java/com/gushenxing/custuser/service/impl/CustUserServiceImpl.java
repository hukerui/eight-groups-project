package com.gushenxing.custuser.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.gushenxing.commom.pojo.PageResult;
import com.gushenxing.commom.pojo.QueryPageBean;
import com.gushenxing.custuser.dao.CustUserMapper;
import com.gushenxing.custuser.dao.RoleMapper;
import com.gushenxing.custuser.pojo.CustUser;
import com.gushenxing.custuser.pojo.Role;
import com.gushenxing.custuser.service.CustUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CustUserServiceImpl implements CustUserService {
    @Autowired
    private CustUserMapper custUserMapper;

    @Autowired
    private RoleMapper roleMapper;

    @Autowired
    private RedisTemplate redisTemplate;
    /**
     * 查询所有用户信息
     * @return
     */
    public List<CustUser> findAll(String username) {
//        Example example = new Example(CustUser.class);
//        Example.Criteria criteria = example.createCriteria();
//        criteria.andEqualTo("isdelete","0" );
//        List<CustUser> custUsers = custUserMapper.selectByExample(example);
        CustUser custUser = (CustUser) redisTemplate.boundHashOps("hashCustUser").get(username);
        Integer customerId = custUser.getCustomerId();
        CustUser custUser1 = new CustUser();
        custUser1.setIsdelete("0");
        custUser1.setCustomerId(customerId);
        List<CustUser> custUsers = custUserMapper.select(custUser1);
        for (CustUser user : custUsers) {
            user.setPassword(null);
        }
        return custUsers;
    }

    /**
     * 根据id查找用户
     * @param id
     * @return
     */
    public CustUser findById(int id) {
        CustUser custUser = custUserMapper.selectByPrimaryKey(id);
        custUser.setPassword(null);
        return custUser;
    }

    /**
     * 查询所有平台角色
     * @return
     */
    public List<Role> findRole() {
        Example example = new Example(Role.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("category","1" );
        List<Role> roles = roleMapper.selectByExample(example);
        for (Role role : roles) {
            role.setCategory(null);
        }
        return roles;
    }

    //分页条件查询所有用户信息
    public PageResult findPage(QueryPageBean queryPageBean) {
        Integer currentPage = queryPageBean.getCurrentPage();
        Integer pageSize = queryPageBean.getPageSize();
        String queryString = queryPageBean.getQueryString();//查询条件
        //完成分页查询，基于mybatis框架提供的分页助手插件完成
        PageHelper.startPage(currentPage, pageSize);
        //select * from t_checkitem limit 0,10
        Page<CustUser> page = custUserMapper.selectByCondition(queryString);
        for (CustUser custUser : page) {
            custUser.setPassword(null);
        }
        long total = page.getTotal();
        List<CustUser> rows = page.getResult();
        return new PageResult(total, rows);
    }

    /**
     * 新增用户和角色
     * @param custUser
     * @param roleId
     */
    public void add(CustUser custUser, Integer roleId) {
        custUser.setIsdelete("0");
        custUser.setStatus("1");
        custUser.setCustomerId(1);
        custUser.setIsload("1");
        custUserMapper.insert(custUser);
        Integer cuId = custUser.getId();
        if (roleId != null){
            Map<String, Integer> map = new HashMap<>();
            map.put("cust_user_id", cuId);
            map.put("role_id", roleId);
            custUserMapper.setCustUserAndRole(map);
        }
    }

    /**
     * 修改用户角色
     * @param id
     * @param roleId
     */
    public void update(int id, Integer roleId) {
        if (roleId != null){
            Map<String, Integer> map = new HashMap<>();
            map.put("cust_user_id", id);
            map.put("role_id", roleId);
            custUserMapper.setRoleIdByCustUserId(map);
        }
    }

    /**
     * 重置用户密码
     */
    public void resetPassword(int id,String password) {
        custUserMapper.resetPassword(id,password);
    }

    /**
     * 删除用户
     */
    public void delete(int id) {
        custUserMapper.updateDel(id);
    }
}
