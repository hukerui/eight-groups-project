package com.gushenxing.custuser.dao;

import com.gushenxing.custuser.pojo.Role;
import tk.mybatis.mapper.common.Mapper;

public interface RoleMapper extends Mapper<Role> {
}
