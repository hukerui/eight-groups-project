package com.gushenxing.custuser.service;

import com.gushenxing.commom.pojo.PageResult;
import com.gushenxing.commom.pojo.QueryPageBean;
import com.gushenxing.custuser.pojo.CustUser;
import com.gushenxing.custuser.pojo.Role;

import java.util.List;

public interface CustUserService {
    List<CustUser> findAll(String username);
    CustUser findById(int id);
    List<Role> findRole();
    PageResult findPage(QueryPageBean queryPageBean);
    void add(CustUser custUser, Integer roleId);
    void update(int id, Integer roleId);
    void resetPassword(int id,String password);
    void delete(int id);
}
