package com.gushenxing.custuser.controller;

import com.gushenxing.commom.pojo.PageResult;
import com.gushenxing.commom.pojo.QueryPageBean;
import com.gushenxing.commom.pojo.Result;
import com.gushenxing.commom.pojo.StatusCode;
import com.gushenxing.custuser.feign.CustUserLoginFeign;
import com.gushenxing.custuser.pojo.CustUser;
import com.gushenxing.custuser.pojo.Role;
import com.gushenxing.custuser.service.CustUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
//@CrossOrigin
@RequestMapping("/custuser")
public class CustUserController {
    @Autowired
    private CustUserService custUserService;
    @Autowired
    private CustUserLoginFeign custUserLoginFeign;

    /**
     * 查询所有用户信息
     * @return
     */
    @GetMapping
    public Result findAll(String username){

        List<CustUser> custUserList =custUserService.findAll(username);
        return new Result(true, StatusCode.OK, "查询成功",custUserList);
    }

    /**
     * 根据id查询用户信息
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Result findbyId(@PathVariable int id){
        CustUser custUser = custUserService.findById(id);
        return new Result(true, StatusCode.OK, "查询成功",custUser);
    }

    /**
     * 查询所有平台角色
     */
    @GetMapping("/findRole")
    public Result findRole(){
        List<Role> roles = custUserService.findRole();
        return new Result(true, StatusCode.OK, "查询成功",roles);
    }

    /**
     * 分页条件查询所有用户信息
     * @param queryPageBean
     * @return
     */
    @GetMapping("/findPage")
    public Result findPage(@RequestBody QueryPageBean queryPageBean){
        PageResult page = custUserService.findPage(queryPageBean);
        return new Result(true, StatusCode.OK, "查询成功",page);
    }

    /**
     *新增用户信息
     */
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;
    @PostMapping("/add")
    public Result add(@RequestBody CustUser custUser,Integer roleId){
        String password = passwordEncoder.encode("123456");
        custUser.setPassword(password);
        custUserService.add(custUser,roleId);
        return new Result(true, StatusCode.OK, "添加成功");
    }

    /**
     * 修改用户角色信息
     */
    @PutMapping("/update")
    public Result update( int id,Integer roleId){
        custUserService.update(id,roleId);
        return new Result(true, StatusCode.OK, "修改成功");
    }

    /**
     * 重置密码
     */
    @PutMapping("/resetPassword/{id}")
    public Result resetPassword(@PathVariable int id){
        String password = passwordEncoder.encode("123456");
        custUserService.resetPassword(id,password);
        return new Result(true, StatusCode.OK, "重置密码成功");
    }

    /**
     * 删除用户
     * 逻辑删除，数据库表数据并没有删
     */
    @DeleteMapping("/{id}")
    public Result delete(@PathVariable int id){
        custUserService.delete(id);
        return new Result(true, StatusCode.OK, "删除成功");
    }
}
