package com.gushenxing.custuser.dao;

import com.github.pagehelper.Page;
import com.gushenxing.custuser.pojo.CustUser;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface CustUserMapper extends Mapper<CustUser> {
    @Select("<script>select * from t_cust_user <if test=\"queryString!=null and queryString!=''\">where name=#{queryString} </if></script>")
    Page<CustUser> selectByCondition(@Param("queryString")String queryString);
    @Select("insert into t_cust_user_role (cuid,rid) values (#{cust_user_id},#{role_id})")
    void setCustUserAndRole(Map<String, Integer> map);
    @Update("update t_cust_user_role set rid=#{role_id} where cuid=#{cust_user_id}")
    void setRoleIdByCustUserId(Map<String, Integer> map);
    @Update("update t_cust_user set password=#{password} where id = #{id}")
    void resetPassword(@Param("id") int id,@Param("password")String password);
    @Update("update t_cust_user set isdelete=1 where id = #{id}")
    void updateDel(int id);
}
