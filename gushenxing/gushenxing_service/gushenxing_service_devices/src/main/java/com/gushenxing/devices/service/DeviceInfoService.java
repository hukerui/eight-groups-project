package com.gushenxing.devices.service;

import com.gushenxing.devices.pojo.DeviceInfo;

import java.util.List;

public interface DeviceInfoService {

    //根据设备ID查询说明书
    public List<DeviceInfo> findByDid(Integer did);

    //设备说明书的添加
    public void add(DeviceInfo deviceInfo);

    //设备说明书的修改
    public void update(DeviceInfo deviceInfo);

    //根据设备ID删除设备
    public void delById(Integer did);
}
