package com.gushenxing.devices.service;


import com.github.pagehelper.Page;
import com.gushenxing.devices.pojo.Device;

import java.util.List;
import java.util.Map;

public interface DevicesService {
     //设备列表查询
    public List<Device> findList();

     //设备的添加
    public void add(Device device);

    //设备的修改
    public void update(Device device);

    //根据id删除设备
    public void delById(Integer id);

    //分页查询
    public Page<Device> findPage(int page, int size);


}
