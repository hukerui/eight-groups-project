package com.gushenxing.devices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@EnableEurekaClient //声明当前的工程是eureka客户端
@MapperScan(basePackages = {"com.gushenxing.devices.dao"})
public class DevicesApplication {

    public static void main(String[] args) {
        SpringApplication.run(DevicesApplication.class,args);
    }
}
