package com.gushenxing.devices.service;


import com.gushenxing.devices.pojo.DeviceIndex;

import java.util.List;
import java.util.Map;

public interface DeviceIndexService {

    /**
     * 根据设备id查询设备指标中间表
     */
      List<DeviceIndex> findByDid(Integer did);

}
