package com.gushenxing.devices.service.impl;

import com.gushenxing.devices.dao.DeviceIndexMapper;
import com.gushenxing.devices.pojo.DeviceIndex;
import com.gushenxing.devices.service.DeviceIndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Service
public class DeviceIndexServiceImpl implements DeviceIndexService {

    @Autowired
    private DeviceIndexMapper deviceIndexMapper;

    @Override
    public List<DeviceIndex> findByDid(Integer did) {

        Example example=new Example(DeviceIndex.class);
        Example.Criteria criteria = example.createCriteria();

        if (did!=null){
            criteria.andEqualTo("did",did);
        }
        List <DeviceIndex> deviceIndexList = deviceIndexMapper.selectByExample(example);

        return deviceIndexList;
    }

}
