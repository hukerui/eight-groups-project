package com.gushenxing.devices.dao;

import com.gushenxing.devices.pojo.Device;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;


@Repository
public interface DeviceMapper extends Mapper<Device> {
}


