package com.gushenxing.devices.dao;

import com.gushenxing.devices.pojo.DeviceIndex;

import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@Repository
public interface DeviceIndexMapper extends Mapper<DeviceIndex> {

}

