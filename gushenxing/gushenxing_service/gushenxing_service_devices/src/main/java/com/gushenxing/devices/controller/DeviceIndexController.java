package com.gushenxing.devices.controller;


import com.gushenxing.commom.pojo.Result;
import com.gushenxing.commom.pojo.StatusCode;
import com.gushenxing.devices.pojo.DeviceIndex;
import com.gushenxing.devices.service.DeviceIndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/deviceIndex")
public class DeviceIndexController {

    @Autowired
    private DeviceIndexService deviceIndexService;

    @GetMapping("/findByDid/{did}")
    public Result findByDid(@PathVariable("did") Integer did){
        List <DeviceIndex> byId = deviceIndexService.findByDid(did);
        return new Result(true, StatusCode.OK,"查询成功",byId);
    }
}
