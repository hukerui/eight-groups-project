package com.gushenxing.devices.controller;

import com.gushenxing.commom.pojo.PageResult;
import com.gushenxing.devices.pojo.Device;
import com.gushenxing.commom.pojo.Result;
import com.gushenxing.commom.pojo.StatusCode;

import com.github.pagehelper.Page;
import com.gushenxing.devices.service.DevicesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/devices")
public class DevicesController {

    @Autowired
    private DevicesService devicesService;

    /**
     * 查询所有设备信息
     *
     * @return
     */
    @GetMapping("/findAll")
    public Result<List<Device>> findList() {
        try {
            List<Device> deviceList = devicesService.findList();
            return new Result<>(true, StatusCode.OK, "查询成功", deviceList);
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(false,StatusCode.ERROR,"查询失败");
        }
    }

    /**
     * 增加设备
     *
     * @param device
     * @return
     */
    @PostMapping("/add")
    public Result add(@RequestBody Device device) {
        try {
            devicesService.add(device);
            return new Result(true, StatusCode.OK, "添加成功");
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(false,StatusCode.ERROR,"添加失败");
        }
    }

    /**
     * 根据id修改设备信息
     *
     * @param id
     * @param device
     * @return
     */
    @PutMapping("/update/{id}")
    public Result updateById(@PathVariable("id") Integer id, @RequestBody Device device) {
        try {
            device.setId(id);
            devicesService.update(device);
            return new Result(true, StatusCode.OK, "修改成功");
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(false,StatusCode.ERROR,"修改失败");
        }
    }

    @DeleteMapping("/delete/{id}")
    public Result deleteById(@PathVariable("id") Integer id) {
        try {
            devicesService.delById(id);
            return new Result(true,StatusCode.OK,"删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(false,StatusCode.ERROR,"修改失败");
        }
    }

    @GetMapping("/findPage/{page}/{size}")
    public Result findPage(@PathVariable int page,@PathVariable int size){
        try {
            Page<Device> devicePage = devicesService.findPage(page, size);
            PageResult pageResult = new PageResult(devicePage.getTotal(),devicePage.getResult());
            return new Result(true,StatusCode.OK,"查询成功",pageResult);
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(false,StatusCode.ERROR,"查询失败");
        }
    }
}
