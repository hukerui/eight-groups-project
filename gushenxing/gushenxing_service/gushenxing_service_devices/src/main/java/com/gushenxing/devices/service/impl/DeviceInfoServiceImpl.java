package com.gushenxing.devices.service.impl;

import com.gushenxing.devices.dao.DeviceInfoMapper;
import com.gushenxing.devices.pojo.DeviceInfo;
import com.gushenxing.devices.service.DeviceInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Service
public class DeviceInfoServiceImpl implements DeviceInfoService {

    @Autowired
    private DeviceInfoMapper deviceInfoMapper;

    //根据id进行查询
    @Override
    public List<DeviceInfo> findByDid(Integer did) {
        Example example=new Example(DeviceInfo.class);
        Example.Criteria criteria = example.createCriteria();
        if (did!=null){
            criteria.andEqualTo("did",did);
        }
        List <DeviceInfo> deviceInfoList = deviceInfoMapper.selectByExample(example);
        return deviceInfoList;
    }

    //添加
    @Override
    public void add(DeviceInfo deviceInfo) {
        deviceInfoMapper.insert(deviceInfo);
    }

    //修改
    @Override
    public void update(DeviceInfo deviceInfo) {
        deviceInfoMapper.updateByPrimaryKey(deviceInfo);
    }

    //根据设备id进行删除对应的设备说明书
    @Override
    public void delById(Integer did) {
        List <DeviceInfo> infos = findByDid(did);
        if (infos!=null||infos.size()>0){
            for (DeviceInfo info : infos) {
                Integer id = info.getId();
                deviceInfoMapper.deleteByPrimaryKey(id);
            }
        }
    }
}
