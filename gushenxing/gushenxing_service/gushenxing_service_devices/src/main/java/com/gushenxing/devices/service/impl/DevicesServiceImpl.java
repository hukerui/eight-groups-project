package com.gushenxing.devices.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import com.gushenxing.commom.pojo.PageResult;
import com.gushenxing.commom.pojo.Result;
import com.gushenxing.commom.pojo.StatusCode;
import com.gushenxing.devices.dao.DeviceMapper;
import com.gushenxing.devices.pojo.Device;

import com.gushenxing.devices.service.DevicesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class DevicesServiceImpl implements DevicesService {

    @Autowired
    private DeviceMapper deviceMapper;

    /**
     * 查询所有
     * @return
     */
    @Override
    public List<Device> findList() {
        return deviceMapper.selectAll();
    }


    /**
     * 增加设备
     * @param device
     */
    @Override
    @Transactional
    public void add(Device device) {
        deviceMapper.insert(device);
    }

    /**
     * 修改设备信息
     * @param device
     */
    @Override
    @Transactional
    public void update(Device device) {
        deviceMapper.updateByPrimaryKey(device);
    }

    /**
     * 删除设备信息
     * @param id
     */
    @Override
    @Transactional
    public void delById(Integer id) {

        deviceMapper.deleteByPrimaryKey(id);
    }

    /**
     * 分页查询
     * @param page
     * @param size
     * @return
     */
    @Override
    public Page<Device> findPage(int page, int size) {
        PageHelper.startPage(page, size);
        Page<Device> devices = (Page<Device>) deviceMapper.selectAll();
        return devices;
    }


}
