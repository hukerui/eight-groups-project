package com.gushenxing.devices.controller;

import com.gushenxing.commom.pojo.Result;
import com.gushenxing.commom.pojo.StatusCode;
import com.gushenxing.devices.pojo.DeviceInfo;
import com.gushenxing.devices.service.DeviceInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/deviceInfo")
public class DeviceInfoController {

    @Autowired
    private DeviceInfoService deviceInfoService;

    /**
     * 根据设备did查询
     */
    @GetMapping("/findByDid/{did}")
    public Result<DeviceInfo> findByDid(@PathVariable("did") Integer did) {
        List<DeviceInfo> byDid = deviceInfoService.findByDid(did);
        if (byDid != null || byDid.size() > 0) {
            return new Result<>(true, StatusCode.OK, "查询成功", byDid);
        }
        return new Result(false, StatusCode.ERROR, "查询失败");
    }

    /**
     * 增加设备
     *
     * @param deviceInfo
     * @return
     */
    @PostMapping("/add")
    public Result add(@PathVariable DeviceInfo deviceInfo) {
        try{deviceInfoService.add(deviceInfo);
            return new Result(true, StatusCode.OK, "添加成功");
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false,StatusCode.ERROR,"添加失败");
        }
    }

    /**
     * 根据id修改设备信息
     *
     * @param id
     * @param deviceInfo
     * @return
     */
    @PutMapping("/updateById/{id}")
    public Result updateById(@PathVariable("id") Integer id, @RequestBody DeviceInfo deviceInfo) {
        try {
            deviceInfoService.update(deviceInfo);
            return new Result(true,StatusCode.OK,"修改成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,StatusCode.ERROR,"修改失败");
        }
    }

}
