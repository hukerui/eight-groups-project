package com.gushenxing.devices.dao;

import com.gushenxing.devices.pojo.DeviceInfo;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@Repository
public interface DeviceInfoMapper extends Mapper<DeviceInfo> {

}

