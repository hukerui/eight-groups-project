package com.gushenxing.system.service.impl;


import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.gushenxing.system.dao.MenuMapper;
import com.gushenxing.system.dao.RoleMapper;
import com.gushenxing.system.dao.UserMapper;
import com.gushenxing.system.pojo.TMenu;
import com.gushenxing.system.pojo.TRole;
import com.gushenxing.system.pojo.TUser;
import com.gushenxing.system.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private RoleMapper roleMapper;

    @Autowired
    private MenuMapper menuMapper;


    /**
     * 查询全部列表
     *
     * @return
     */
    @Override
    public List<TRole> findAll() {
        List<TRole> roleList = roleMapper.selectAll();
        if (roleList != null && roleList.size() > 0) {
            for (TRole tRole : roleList) {
                this.setUserAndRole(tRole);
            }
        }
        return roleList;
    }

    private TRole setUserAndRole(TRole tRole) {
        int rid = tRole.getId();

        Set<TUser> user = userMapper.findUserByRoleId(rid);
        if (user != null && !"".equals(user)) {
            tRole.setUsers(user);
        }

        Set<TMenu> menus = menuMapper.findByRId(rid);
        if (menus != null && menus.size() > 0) {
            tRole.setMenus(menus);//为角色添加菜单
        }
        return tRole;
    }

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    @Override
    public TRole findById(Integer id) {
        TRole tRole = roleMapper.selectByPrimaryKey(id);
        if (tRole != null) {
            this.setUserAndRole(tRole);
        }
        return tRole;
    }


    /**
     * 增加
     *
     * @param tRole
     */
    @Override
    public void add(TRole tRole,Integer[] mids) {
//        int rid = tRole.getId();
//        List<Integer> mids = menuMapper.findMenuIdByRoleId(rid);
//        roleMapper.deleteAssociation(tRole.getId());
        roleMapper.insert(tRole);
        if (mids != null && mids.length>0) {
            for (Integer mid : mids) {
                Map<String, Integer> map = new HashMap<>();
                map.put("rid", tRole.getId());
                map.put("mid", mid);
                roleMapper.set(map);
            }
        }
    }


    /**
     * 修改
     *
     * @param tRole
     */
    @Override
    public void update(TRole tRole,Integer[] mids) {
        int rid = tRole.getId();
        roleMapper.deleteAssociation(rid);
        if (mids != null && mids.length>0) {
            for (Integer mid : mids) {
                Map<String, Integer> map = new HashMap<>();
                map.put("rid", rid);
                map.put("mid", mid);
                roleMapper.set(map);
            }
        }
        roleMapper.updateByPrimaryKey(tRole);
    }

    /**
     * 删除
     *
     * @param id
     */
    @Override
    public void delete(Integer id) {
        Set<TUser> users = userMapper.findUserByRoleId(id);
        for (TUser user : users) {
            int userId = user.getId();
            userMapper.deleteAssociation(userId);
        }
        roleMapper.deleteAssociation(id);
        roleMapper.deleteByPrimaryKey(id);
    }


    /**
     * 新增角色id关联多个菜单id
     *
     * @param mids
     * @param rid
     */
    @Override
    public void add(Integer[] mids, Integer rid) {

        roleMapper.deleteAssociation(rid);
        if (mids != null && mids.length > 0) {
            for (Integer mid : mids) {
                Map<String, Integer> map = new HashMap<>();
                map.put("rid", rid);
                map.put("mid", mid);
                roleMapper.set(map);
            }
        }
    }


    /**
     * 条件查询
     *
     * @param searchMap
     * @return
     */
    @Override
    public List<TRole> findList(Map<String, Object> searchMap) {
        Example example = createExample(searchMap);
        List<TRole> tRoles = roleMapper.selectByExample(example);
        if (tRoles != null && tRoles.size() > 0) {
            for (TRole tRole : tRoles) {
                this.setUserAndRole(tRole);
            }
        }
        return tRoles;
    }

    /**
     * 分页查询
     *
     * @param page
     * @param size
     * @return
     */
    @Override
    public Page<TRole> findPage(int page, int size) {
        PageHelper.startPage(page, size);
        Page<TRole> tRoles = (Page<TRole>) roleMapper.selectAll();
        if (tRoles != null && tRoles.size() > 0) {
            for (TRole tRole : tRoles) {
                this.setUserAndRole(tRole);
            }
        }
        return tRoles;
    }

    /**
     * 条件+分页查询
     *
     * @param searchMap 查询条件
     * @param page      页码
     * @param size      页大小
     * @return 分页结果
     */
    @Override
    public Page<TRole> findPage(Map<String, Object> searchMap, int page, int size) {
        PageHelper.startPage(page, size);
        Example example = createExample(searchMap);
        Page<TRole> tRoles = (Page<TRole>) roleMapper.selectByExample(example);
        if (tRoles != null && tRoles.size() > 0) {
            for (TRole tRole : tRoles) {
                this.setUserAndRole(tRole);
            }
        }
        return tRoles;
    }

    /**
     * 根据角色查询对应菜单
     *
     * @param id 查询条件
     */
    @Override
    public Set<TMenu> findMenuByRoleId(Integer id) {
        Set<TMenu> menus = roleMapper.findMenuByRoleId(id);
        return menus;
    }

    /**
     * 构建查询对象
     *
     * @param searchMap
     * @return
     */
    private Example createExample(Map<String, Object> searchMap) {
        Example example = new Example(TRole.class);
        Example.Criteria criteria = example.createCriteria();
        if (searchMap != null) {
            // 角色名称
            if (searchMap.get("role") != null && !"".equals(searchMap.get("role"))) {
                criteria.andLike("role", "%" + searchMap.get("role") + "%");
            }

            // ID
            if (searchMap.get("id") != null) {
                criteria.andEqualTo("id", searchMap.get("id"));
            }

        }
        return example;
    }


}
