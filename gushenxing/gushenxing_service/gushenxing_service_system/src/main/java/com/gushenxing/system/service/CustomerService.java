package com.gushenxing.system.service;

import com.github.pagehelper.Page;
import com.gushenxing.custuser.pojo.Customer;


import java.util.Map;

public interface CustomerService {
    //添加农场组织
    void add(Customer customer);
    //修改农场组织
    void update(Customer customer);
    //多条件分页查询
    Page<Customer> findPage(Map<String, Object> searchMap, int page, int size);
    //根据id查询
    Customer findById(Integer id);
}
