package com.gushenxing.system.controller;


import com.github.pagehelper.Page;
import com.gushenxing.commom.pojo.PageResult;
import com.gushenxing.commom.pojo.Result;
import com.gushenxing.commom.pojo.StatusCode;
import com.gushenxing.system.pojo.TMenu;
import com.gushenxing.system.pojo.TRole;
import com.gushenxing.system.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Set;

@RestController
@CrossOrigin
@RequestMapping("/role")
public class RoleController {


    @Autowired
    private RoleService roleService;

    /**
     * 查询全部数据
     *
     * @return
     */
    @GetMapping
    public Result findAll() {
        try {
            List<TRole> roleList = roleService.findAll();
            return new Result(true,StatusCode.OK,"查询成功",roleList);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,StatusCode.ERROR,"查询失败");
        }
    }

    /***
     * 根据ID查询数据
     * @param id
     * @return
     */
//    @GetMapping("/{id}")
//    public Result findById(@PathVariable Integer id) {
//        try {
//            TRole tRole = roleService.findById(id);
//            return new Result(true,StatusCode.OK,"查询成功",tRole);
//        } catch (Exception e) {
//            e.printStackTrace();
//            return new Result(false,StatusCode.ERROR,"查询失败");
//        }
//    }


    /***
     * 新增角色
     * @param tRole
     * @return
     */
    @PostMapping
    public Result add(@RequestBody TRole tRole,Integer[] mids) {
        try {
            roleService.add(tRole,mids);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,StatusCode.ERROR,"添加失败");
        }
        return new Result(true,StatusCode.OK,"添加成功");
    }


    /***
     * 修改角色
     * @param tRole
     * @param
     * @return
     */
    @PutMapping("update")
    public Result update(@RequestBody TRole tRole,Integer[] mids) {
        try {
            roleService.update(tRole, mids);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,StatusCode.ERROR,"修改失败");
        }
        return new Result(true,StatusCode.OK,"修改成功");
    }


    /***
     * 根据ID删除品牌数据
     * @param id
     * @return
     */
    @DeleteMapping(value = "/{id}")
    public Result delete(@PathVariable Integer id) {
        try {
            roleService.delete(id);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,StatusCode.ERROR,"删除失败");
        }
        return new Result(true,StatusCode.OK,"删除成功");
    }


    /***
     * 多条件搜索数据
     * @param searchMap
     * @return
     */
    @GetMapping(value = "/search")
    public Result findList(@RequestParam Map searchMap) {
        try {
            List<TRole> list = roleService.findList(searchMap);
            return new Result(true,StatusCode.OK,"查询成功",list);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,StatusCode.ERROR,"查询失败");
        }
    }


    /***
     * 分页搜索实现
     * @param searchMap
     * @param page
     * @param size
     * @return
     */
    @GetMapping(value = "/search/{page}/{size}")
    public Result findPage(@RequestParam Map searchMap, @PathVariable int page, @PathVariable int size) {
        Page<TRole> pageList = roleService.findPage(searchMap, page, size);
        PageResult pageResult = new PageResult(pageList.getTotal(), pageList.getResult());
        return new Result(true,StatusCode.OK,"查询成功",pageResult);
    }


    @GetMapping("/findMenuByRoleId")
    public Result findMenuByRoleId(Integer id) {
        try {
            Set<TMenu> list = roleService.findMenuByRoleId(id);
            return new Result(true,StatusCode.OK,"查询角色对应菜单成功", list);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,StatusCode.ERROR,"查询角色对应菜单失败");
        }
    }


    @PostMapping("/add")
    public Result add(Integer[] mids, Integer rid) {
        try {
            roleService.add(mids, rid);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,StatusCode.ERROR,"新增角色权限失败");
        }
        return new Result(true,StatusCode.OK,"新增角色权限成功");

    }


}
