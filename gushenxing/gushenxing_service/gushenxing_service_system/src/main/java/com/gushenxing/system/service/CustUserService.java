package com.gushenxing.system.service;



import com.github.pagehelper.Page;
import com.gushenxing.custuser.pojo.CustUser;

import java.util.Map;

public interface CustUserService {
    //添加平台超级管理员
    //void add(CustUser custUser);

    //查询平台超级管理员
    CustUser findByCustomerId(Integer id);

    //修改平台超级管理员信息
    void update(CustUser custUser);

    //多条件分页查询
    Page<CustUser> findPage(Map<String, Object> searchMap, int page, int size);

    CustUser findById(Integer id);
}
