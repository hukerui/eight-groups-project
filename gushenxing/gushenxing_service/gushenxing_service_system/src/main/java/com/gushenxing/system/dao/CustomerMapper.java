package com.gushenxing.system.dao;


import com.gushenxing.custuser.pojo.Customer;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import tk.mybatis.mapper.common.Mapper;

public interface CustomerMapper extends Mapper<Customer> {

    @Insert("insert into t_customer(name,code,address,contacts,phone,email,create_time) value(#{name},#{code},#{address},#{contacts},#{phone},#{email},#{createTime})")
    @Options(useGeneratedKeys = true,keyColumn = "id")
     void add(Customer customer);

}