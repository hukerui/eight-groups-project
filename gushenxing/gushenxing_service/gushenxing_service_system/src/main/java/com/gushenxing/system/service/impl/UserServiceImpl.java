package com.gushenxing.system.service.impl;


import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.gushenxing.system.dao.MenuMapper;
import com.gushenxing.system.dao.RoleMapper;
import com.gushenxing.system.dao.UserMapper;
import com.gushenxing.system.pojo.TMenu;
import com.gushenxing.system.pojo.TRole;
import com.gushenxing.system.pojo.TUser;
import com.gushenxing.system.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.*;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private RoleMapper roleMapper;

    @Autowired
    private MenuMapper menuMapper;


    /**
     * 查询全部列表
     *
     * @return
     */
    @Override
    public List<Map<String, Object>> findAll() {
        List<Map<String, Object>> resultList = new ArrayList<>();
        List<TUser> userList = userMapper.selectAll();
        if (userList != null) {
            for (TUser tUser : userList) {
                this.setRoleAndMenu(tUser);
                Map<String, Object> map = new HashMap<>();
                map.put("id", tUser.getId());
                map.put("name", tUser.getName());
                map.put("phone", tUser.getPhone());
                map.put("status", tUser.getStatus());
                map.put("roles", tUser.getRoles());
                resultList.add(map);
            }
        }
        return resultList;
    }

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    @Override
    public List<Map<String, Object>> findById(Integer id) {
        List<Map<String, Object>> resultList = new ArrayList<>();
        TUser tUser = userMapper.selectByPrimaryKey(id);
        if (tUser != null) {
            this.setRoleAndMenu(tUser);
            Map<String, Object> map = new HashMap<>();
            map.put("id", tUser.getId());
            map.put("name", tUser.getName());
            map.put("phone", tUser.getPhone());
            map.put("status", tUser.getStatus());
            map.put("roles", tUser.getRoles());
            resultList.add(map);
        }
        return resultList;
    }


    /**
     * 增加
     *
     * @param tUser
     */
    @Override
    public void add(TUser tUser, Integer rid) {
        tUser.setIsdelete("0");
        tUser.setStatus("1");
        tUser.setIsload("1");
        //获取盐
        String gensalt = BCrypt.gensalt();
        //对用户的密码进行加密
        String hashpw = BCrypt.hashpw(tUser.getPassword(), gensalt);
        tUser.setPassword(hashpw);
        userMapper.insert(tUser);
        Map<String, Integer> map = new HashMap<>();
        map.put("uid", tUser.getId());
        map.put("rid", rid);
        userMapper.set(map);
    }


    /**
     * 修改
     *
     * @param
     */
    @Override
    public void update(TUser tUser, Integer[] rids) {
        //获取盐
        String gensalt = BCrypt.gensalt();
        //对用户的密码进行加密
        String hashpw = BCrypt.hashpw(tUser.getPassword(), gensalt);
        tUser.setPassword(hashpw);
        userMapper.updateByPrimaryKeySelective(tUser);
        userMapper.deleteAssociation(tUser.getId());
        if (rids != null && rids.length > 0) {
            for (Integer rid : rids) {
                Map<String, Integer> map = new HashMap<>();
                map.put("uid", tUser.getId());
                map.put("rid", rid);
                userMapper.set(map);
            }
        }
    }

    /**
     * 删除
     *
     * @param id
     */
    @Override
    public void del(Integer id) {
        userMapper.deleteAssociation(id);
        userMapper.del(id);
    }


    /**
     * 新增用户与角色关联
     *
     * @param rids
     * @param id
     * @return
     */
    @Override
    public void insert(Integer[] rids, Integer id) {
        userMapper.deleteAssociation(id);
        if (rids != null && rids.length > 0) {
            for (Integer rid : rids) {
                Map<String, Integer> map = new HashMap<>();
                map.put("uid", id);
                map.put("rid", rid);
                userMapper.set(map);
            }
        }
    }


    /**
     * 条件查询
     *
     * @param searchMap
     * @return
     */
    @Override
    public List<Map<String, Object>> findList(Map<String, Object> searchMap) {
        List<Map<String, Object>> resultList = new ArrayList<>();
        Example example = createExample(searchMap);
        List<TUser> userList = userMapper.selectByExample(example);
        if (userList != null) {
            for (TUser tUser : userList) {
                this.setRoleAndMenu(tUser);
                Map<String, Object> map = new HashMap<>();
                map.put("id", tUser.getId());
                map.put("name", tUser.getName());
                map.put("phone", tUser.getPhone());
                map.put("status", tUser.getStatus());
                map.put("roles", tUser.getRoles());
                resultList.add(map);
            }
        }
        return resultList;
    }

    /**
     * 分页查询
     *
     * @param page
     * @param size
     * @return
     */
    @Override
    public Page<TUser> findPage(int page, int size) {
        PageHelper.startPage(page, size);
        List<TUser> userList = userMapper.selectAll();
        if (userList != null) {
            for (TUser tUser : userList) {
                this.setRoleAndMenu(tUser);
            }
        }
        return (Page<TUser>) userList;
    }


    /**
     * 条件+分页查询
     *
     * @param searchMap 查询条件
     * @param page      页码
     * @param size      页大小
     * @return 分页结果
     */
    @Override
    public Page<TUser> findPage(Map<String, Object> searchMap, int page, int size) {
        PageHelper.startPage(page, size);
        Example example = createExample(searchMap);
        List<TUser> userList = userMapper.selectByExample(example);
        if (userList != null) {
            for (TUser tUser : userList) {
                this.setRoleAndMenu(tUser);
            }
        }
        return (Page<TUser>) userList;
    }


    /***
     * 根据用户id查询对应的角色IDS
     * @param id
     * @return
     */
    @Override
    public List<Integer> findRoleByUserId(Integer id) {
        return userMapper.findRoleByUserId(id);
    }


    /***
     * 用户登录之后，获取到对应的登录人
     * 根据用户名查询对应的角色IDS，并绑定对应的菜单IDS
     * @param s
     * @return
     */
    @Override
    public TUser findByUsername(String s) {
        TUser user = userMapper.findByUsername(s);
        if (user == null) {
            return null;
        }
        this.setRoleAndMenu(user);
        return user;
    }

    @Override
    public TUser findByPhone(String phone) {
        Example example = new Example(TUser.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("phone", phone);
        List<TUser> userList = userMapper.selectByExample(example);
        if (userList!=null && userList.size()>0){
            for (TUser tUser : userList) {
                return tUser;
            }
        }
        return null;
    }


    private TUser setRoleAndMenu(TUser user) {
        int id = user.getId();
        Set<TRole> roles = roleMapper.findById(id);
        if (roles != null && roles.size() > 0) {
            for (TRole role : roles) {//遍历得到每一个角色
                int roleId = role.getId();
                Set<TMenu> menus = menuMapper.findByRId(roleId);
                if (menus != null && menus.size() > 0) {
                    role.setMenus(menus);//为角色添加菜单
                }
            }
            user.setRoles(roles);
        }
        return user;
    }


    /**
     * 构建查询对象
     *
     * @param searchMap
     * @return
     */
    private Example createExample(Map<String, Object> searchMap) {
        Example example = new Example(TUser.class);
        Example.Criteria criteria = example.createCriteria();
        if (searchMap != null) {
            // 用户名
            if (searchMap.get("name") != null && !"".equals(searchMap.get("name"))) {
                criteria.andLike("name", "%" + searchMap.get("name") + "%");
            }
            // 密码
            if (searchMap.get("password") != null && !"".equals(searchMap.get("password"))) {
                criteria.andLike("password", "%" + searchMap.get("password") + "%");
            }
            // 状态
            if (searchMap.get("status") != null && !"".equals(searchMap.get("status"))) {
                criteria.andEqualTo("status", searchMap.get("status"));
            }

            // id
            if (searchMap.get("id") != null) {
                criteria.andEqualTo("id", searchMap.get("id"));
            }

        }
        return example;
    }

}
