package com.gushenxing.system.service;



import com.github.pagehelper.Page;
import com.gushenxing.system.pojo.TRole;
import com.gushenxing.system.pojo.TUser;

import java.util.List;
import java.util.Map;
import java.util.Set;


public interface UserService {

    /***
     * 查询所有
     * @return
     */
    List<Map<String,Object>> findAll();

    /**
     * 根据ID查询
     * @param id
     * @return
     */
    List<Map<String,Object>> findById(Integer id);

    /***
     * 新增
     * @param
     */
    void add(TUser tUser,Integer rid);

    /***
     * 修改
     * @param
     */
    void update(TUser tUser,Integer[] rids);

    /***
     * 删除
     * @param id
     */
    void del(Integer id);

    /***
     * 新增一对多（一个角色对应多个权限）
     * @param id
     */
    void insert(Integer[] rids, Integer id);


    /***
     * 分页查询
     * @param page
     * @param size
     * @return
     */
    Page<TUser> findPage(int page, int size);


    /***
     * 多条件搜索
     * @param searchMap
     * @return
     */
    List<Map<String,Object>> findList(Map<String, Object> searchMap);

    /***
     * 多条件分页查询
     * @param searchMap
     * @param page
     * @param size
     * @return
     */
    Page<TUser> findPage(Map<String, Object> searchMap, int page, int size);

    /***
     * 根据用户id查询对应的角色IDS
     * @param id
     * @return
     */
    List<Integer> findRoleByUserId(Integer id);

    TUser findByUsername(String s);


    TUser findByPhone(String phone);
}
