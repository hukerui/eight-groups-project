package com.gushenxing.system.dao;


import com.gushenxing.custuser.pojo.CustUser;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

public interface CustUserMapper extends Mapper<CustUser> {
    //通过农场Id查找平台超级管理员用户
    @Select("select * from t_cust_user where customer_id=#{id}")
    CustUser findByCustomerId(Integer id);

    //向用户角色表中添加数据
    @Insert("insert into t_cust_user_role value (#{cuid},#{rid})")
    void addUserAndRole(@Param("cuid") Integer cuid, @Param("rid") Integer rid);


    @Insert("insert into t_cust_user(customer_id,name,username,password) value (#{customerId},#{name},#{username},#{password})")
    @Options(useGeneratedKeys = true,keyColumn = "id")
    void add(CustUser custUser);

    //查询用户的所有角色
    @Select("select rid from t_user_role where uid=#{uid}")
    Integer[] findByUid(Integer uid);
}
