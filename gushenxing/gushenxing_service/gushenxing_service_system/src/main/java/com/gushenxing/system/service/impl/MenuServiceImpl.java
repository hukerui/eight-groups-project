package com.gushenxing.system.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.gushenxing.system.dao.MenuMapper;
import com.gushenxing.system.dao.RoleMapper;
import com.gushenxing.system.dao.UserMapper;
import com.gushenxing.system.pojo.TMenu;
import com.gushenxing.system.pojo.TRole;
import com.gushenxing.system.pojo.TUser;
import com.gushenxing.system.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
public class MenuServiceImpl implements MenuService {



    @Autowired
    private RoleMapper roleMapper;

    @Autowired
    private MenuMapper menuMapper;

    /**
     * 查询全部列表
     * @return
     */
    @Override
    public List<TMenu> findAllMenu(Integer id) {
        List<TMenu> menus = menuMapper.findAllMenu(id);
        if (menus!=null && menus.size()>0){
            for (TMenu tMenu : menus) {
                this.setUserAndRoleAndMenu(tMenu);
            }
        }
        return menus;
    }

    private TMenu setUserAndRoleAndMenu(TMenu tMenu) {
        int mid = tMenu.getId();
        Set<TRole> roles = roleMapper.findRoleByMenuId(mid);
        if (roles!=null && roles.size()>0){
            tMenu.setRoles(roles);//为菜单添加角色
        }
//        for (TRole role : roles) {
//            int rid = role.getId();
//            Set<TUser> users = userMapper.findUserByRoleId(rid);
//            if (users!=null && users.size()>0){
//                for (TUser user : users) {
//
//                }
//            }
//            tMenu.set
//        }
        return tMenu;
    }


    /**
     * 根据ID查询
     * @param id
     * @return
     */
    @Override
    public TMenu findById(Integer id){
        TMenu tMenu = menuMapper.selectByPrimaryKey(id);
        this.setUserAndRoleAndMenu(tMenu);
        return tMenu;
    }


    /**
     * 增加
     * @param tMenu
     */
    @Override
    public void add(TMenu tMenu){
        menuMapper.insert(tMenu);
    }


    /**
     * 修改
     * @param tMenu
     */
    @Override
    public void update(TMenu tMenu){
        menuMapper.updateByPrimaryKey(tMenu);
    }

    /**
     * 删除
     * @param id
     */
    @Override
    public void delete(Integer id){
        menuMapper.deleteByPrimaryKey(id);
    }



    /**
     * 条件查询
     * @param searchMap
     * @return
     */
    @Override
    public List<TMenu> findList(Map<String, Object> searchMap){
        Example example = createExample(searchMap);
        List<TMenu> tMenus = menuMapper.selectByExample(example);
        if (tMenus!=null && tMenus.size()>0){
            for (TMenu tMenu : tMenus) {
                this.setUserAndRoleAndMenu(tMenu);
            }
        }
        return tMenus;
    }

    /**
     * 分页查询
     * @param page
     * @param size
     * @return
     */
    @Override
    public Page<TMenu> findPage(int page, int size){
        PageHelper.startPage(page,size);
        Page<TMenu> tMenus = (Page<TMenu>) menuMapper.selectAll();
        if (tMenus!=null && tMenus.size()>0){
            for (TMenu tMenu : tMenus) {
                this.setUserAndRoleAndMenu(tMenu);
            }
        }
        return tMenus;
    }

    /**
     * 条件+分页查询
     * @param searchMap 查询条件
     * @param page 页码
     * @param size 页大小
     * @return 分页结果
     */
    @Override
    public Page<TMenu> findPage(Map<String,Object> searchMap, int page, int size){
        PageHelper.startPage(page,size);
        Example example = createExample(searchMap);
        Page<TMenu> tMenus = (Page<TMenu>) menuMapper.selectByExample(example);
        if (tMenus!=null && tMenus.size()>0){
            for (TMenu tMenu : tMenus) {
                this.setUserAndRoleAndMenu(tMenu);
            }
        }
        return tMenus;
    }

    /**
     * 构建查询对象
     * @param searchMap
     * @return
     */
    private Example createExample(Map<String, Object> searchMap){
        Example example=new Example(TMenu.class);
        Example.Criteria criteria = example.createCriteria();
        if(searchMap!=null){
            // 菜单ID
            if(searchMap.get("id")!=null && !"".equals(searchMap.get("id"))){
                criteria.andLike("id","%"+searchMap.get("id")+"%");
            }
            // 菜单名称
            if(searchMap.get("menu")!=null && !"".equals(searchMap.get("menu"))){
                criteria.andLike("menu","%"+searchMap.get("menu")+"%");
            }

            // URL
            if(searchMap.get("url")!=null && !"".equals(searchMap.get("url"))){
                criteria.andLike("url","%"+searchMap.get("url")+"%");
            }
            // 上级菜单ID
            if(searchMap.get("pid")!=null && !"".equals(searchMap.get("pid"))){
                criteria.andLike("pid","%"+searchMap.get("pid")+"%");
            }


        }
        return example;
    }

}
