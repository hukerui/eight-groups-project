package com.gushenxing.system.controller;

import com.github.pagehelper.Page;

import com.gushenxing.commom.pojo.Result;
import com.gushenxing.commom.pojo.StatusCode;
import com.gushenxing.custuser.pojo.Customer;
import com.gushenxing.system.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    //添加农场组织
    @PostMapping("/add")
    public Result add(@RequestBody Customer customer){
        try {
            customerService.add(customer);
            return new Result(true, StatusCode.OK,"添加成功");
        }catch (Exception e){
            return new Result(false, StatusCode.ERROR,"添加失败");
        }

    }

    //修改农场组织信息
    @PostMapping("/update")
    public Result update(@RequestBody Customer customer){
        try {
            customerService.update(customer);
            return new Result(true, StatusCode.UPDATE_OK,"更新成功");
        }catch (Exception e){
            return new Result(true, StatusCode.UPDATE_ERROR,"更新失败");
        }

    }

    //多条件查询+分页查询
    @GetMapping(value = "/find")
    public Result findPage(@RequestParam(required = false) Map searchMap,
                           @RequestParam(name = "page",defaultValue = "1",required = false) int page,
                           @RequestParam(name = "size",defaultValue = "5",required = false) int size
                          ){
        try {
            Page findPage = customerService.findPage(searchMap, page, size);
            return new Result(true, StatusCode.OK,"查询成功",findPage);
        }catch (Exception e){
            return new Result(true, StatusCode.ERROR,"查询失败");
        }

    }

    //根据id查询
    @GetMapping(value = "/findById")
    public Result findById(@RequestParam("id") Integer id){
        try {
            Customer customer = customerService.findById(id);
            return new Result(true, StatusCode.OK,"查询成功",customer);
        }catch (Exception e){
            e.printStackTrace();
            return new Result(true, StatusCode.ERROR,"查询失败");

        }

    }
}
