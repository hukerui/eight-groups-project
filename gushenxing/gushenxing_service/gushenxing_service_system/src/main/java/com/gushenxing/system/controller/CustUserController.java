package com.gushenxing.system.controller;

import com.github.pagehelper.Page;

import com.gushenxing.commom.pojo.Result;
import com.gushenxing.commom.pojo.StatusCode;
import com.gushenxing.custuser.pojo.CustUser;
import com.gushenxing.system.service.CustUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/custuserInfo")
public class CustUserController {

    @Autowired
    private CustUserService custUserService;

    //修改平台超级管理员信息
    @PostMapping("/update")
    public Result update(@RequestBody CustUser custUser){
        try {
            custUserService.update(custUser);
            return new Result(true, StatusCode.OK,"成功");
        }catch (Exception e){
            return new Result(true, StatusCode.UPDATE_ERROR,"失败");
        }

    }

    //多条件查询+分页查询
    @GetMapping(value = "/find")
    public Result findPage(@RequestParam(required = false) Map searchMap,
                           @RequestParam(name = "page",defaultValue = "1",required = false) int page,
                           @RequestParam(name = "size",defaultValue = "5",required = false) int size
                           ){
        try {
            Page findPage = custUserService.findPage(searchMap, page, size);
            return new Result(true, StatusCode.OK,"查询成功",findPage);
        }catch (Exception e){
            return new Result(true, StatusCode.ERROR,"查询失败");
        }

    }

    //根据id查询
    @GetMapping(value = "/findById")
    public Result findById(@RequestParam("id") Integer id){
        try {
            CustUser custUser = custUserService.findById(id);
            return new Result(true, StatusCode.OK,"查询成功",custUser);
        }catch (Exception e){
            return new Result(true, StatusCode.ERROR,"查询失败");
        }

    }
}
