package com.gushenxing.system.controller;

import com.github.pagehelper.Page;
import com.gushenxing.commom.pojo.PageResult;
import com.gushenxing.commom.pojo.Result;
import com.gushenxing.commom.pojo.StatusCode;
import com.gushenxing.system.pojo.TUser;
import com.gushenxing.system.service.UserService;
import com.gushenxing.system.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping("/user")
public class UserController {


    @Autowired
    private UserService userService;

//    //获得当前登录用户的用户名
//    @RequestMapping("/getUsername")
//    public Result getUsername() {
//        //当Spring security完成认证后，会将当前用户信息保存到框架提供的上下文对象
//        TUser user = (TUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        System.out.println(user);
//        if (user != null) {
//            String username = user.getUsername();
//            return new Result(true,StatusCode.OK,username);
//        }
//
//        return new Result(false,StatusCode.ERROR,"获取用户名失败");
//    }


    @GetMapping("/findByPhone/{phone}")
    public Result<TUser> findByPhone(@PathVariable("phone") String phone){
        try {
            TUser tUser = userService.findByPhone(phone);
            return new Result(true,StatusCode.OK,"查询成功",tUser);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,StatusCode.ERROR,"查询失败");
        }
    }


    @GetMapping("/getUserId")
    public Result getUserId(String username) {

        try {
            Integer id = userService.findByUsername(username).getId();
            return new Result(true,StatusCode.OK,"查询成功",id);

        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,StatusCode.ERROR,"查询失败");
        }

    }

    /**
     * 查询全部数据
     *
     * @return
     */
    @GetMapping
    public Result findAll() {
        try {
            List<Map<String,Object>> userList = userService.findAll();
            return new Result(true,StatusCode.OK,"查询成功",userList);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,StatusCode.ERROR,"查询失败");
        }
    }

    /***
     * 根据ID查询数据
     * @param id
     * @return
     */
    @GetMapping("/findById/{id}")
    public Result findById(@PathVariable Integer id) {
        try {
            List<Map<String,Object>> tUser = userService.findById(id);
            return new Result(true,StatusCode.OK,"查询成功",tUser);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,StatusCode.ERROR,"查询失败");
        }
    }


    /***
     * 新增数据
     * @param tUser
     * @return
     */
    @PostMapping
    public Result add(@RequestBody TUser tUser,Integer rid) {
        try {
            userService.add(tUser,rid);
            return new Result(true,StatusCode.OK,"添加成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,StatusCode.ERROR,"添加失败");
        }
    }


    /***
     * 修改数据
     * @param tUser
     * @return
     */
    @PutMapping()
    public Result update(@RequestBody TUser tUser,Integer[] rids) {
        try {
            userService.update(tUser, rids);
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false,StatusCode.ERROR,"修改失败");
        }
        return new Result(true,StatusCode.OK,"修改成功");
    }


    /***
     * 根据ID删除品牌数据
     * @param id
     * @return
     */
    @PostMapping(value = "/{id}")
    public Result delete(@PathVariable Integer id) {
        try {
            userService.del(id);
            return new Result(true,StatusCode.OK,"删除成功");
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false,StatusCode.ERROR,"删除失败");
        }
    }


    /***
     * 多条件搜索数据
     * @param searchMap
     * @return
     */
    @GetMapping(value = "/search")
    public Result findList(@RequestParam Map searchMap) {
        try {
            List<Map<String,Object>> list = userService.findList(searchMap);
            return new Result(true, StatusCode.OK,"查询成功",list);
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false,StatusCode.ERROR,"查询失败");
        }
    }


    /***
     * 分页搜索实现
     * @param searchMap
     * @param page
     * @param size
     * @return
     */
    @GetMapping(value = "/search/{page}/{size}")
    public Result findPage(@RequestParam Map searchMap, @PathVariable int page, @PathVariable int size) {
        Page pageList = userService.findPage(searchMap,page,size);
        PageResult pageResult = new PageResult(pageList.getTotal(),pageList.getResult());
        return new Result(true,StatusCode.OK,"查询成功",pageResult);
    }




    /***
     * 根据用户id查询所有角色id
     * @param id
     * @return
     */
    @GetMapping("/findRoleByUserId")
    public Result findRoleByUserId(Integer id) {
        try {
            List<Integer> list = userService.findRoleByUserId(id);
            return new Result(true,StatusCode.OK,"查询成功",list);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,StatusCode.ERROR,"查询失败");
        }
    }

    /***
     * 为用户新增角色关联
     * @param rids
     * @param id
     * @return
     */
    @PostMapping("/insert")
    public Result insert(Integer[] rids, Integer id) {
        try {
            userService.insert(rids,id);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,StatusCode.ERROR,"为用户新增角色关联失败");
        }
        return new Result(true,StatusCode.OK,"为用户新增角色关联成功");
    }
}
