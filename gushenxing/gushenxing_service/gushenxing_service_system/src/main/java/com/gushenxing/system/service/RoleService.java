package com.gushenxing.system.service;



import com.github.pagehelper.Page;
import com.gushenxing.system.pojo.TMenu;
import com.gushenxing.system.pojo.TRole;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface RoleService {

    /***
     * 查询所有
     * @return
     */
    List<TRole> findAll();

    /**
     * 根据ID查询
     * @param id
     * @return
     */
    TRole findById(Integer id);

    /***
     * 新增
     * @param tRole
     */
    void add(TRole tRole,Integer[] mids);

    /***
     * 修改
     * @param tRole
     */
    void update(TRole tRole,Integer[] mids);

    /***
     * 删除
     * @param id
     */
    void delete(Integer id);



    /***
     * 多条件搜索
     * @param searchMap
     * @return
     */
    List<TRole> findList(Map<String, Object> searchMap);

    /***
     * 分页查询
     * @param page
     * @param size
     * @return
     */
    Page<TRole> findPage(int page, int size);

    /***
     * 多条件分页查询
     * @param searchMap
     * @param page
     * @param size
     * @return
     */
    Page<TRole> findPage(Map<String, Object> searchMap, int page, int size);


    Set<TMenu> findMenuByRoleId(Integer id);

    void add(Integer[] mids,Integer rid);

}
