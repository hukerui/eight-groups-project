package com.gushenxing.system.dao;


import com.gushenxing.system.pojo.TMenu;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Set;

public interface MenuMapper extends Mapper<TMenu> {

    @Select("select mid from t_role_menu where mid = #{id}")
    List<Integer> findMenuIdByRoleId(Integer id);


    @Select("select m.* from t_menu m,t_role_menu rm where m.id=rm.mid and rm.rid=#{rid}")
    Set<TMenu> findByRId(Integer rid);



    @Select("select * from t_menu where id in (select mid from t_role_menu where rid IN" +
            " (select rid from t_user_role where uid=#{id})) and level=1;")
    @Results({
            @Result(column = "id", property = "id", id = true),
            @Result(property = "children", column = "id", many = @Many(select = "com.gushenxing.dao.MenuMapper.findChildrenById"))
    })
    public List<TMenu> findAllMenu(Integer id);


    @Select("select * from t_menu where pid=#{id}")
    public List<TMenu> findChildrenById(Integer id);
}
