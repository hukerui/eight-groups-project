package com.gushenxing.system.controller;


import com.github.pagehelper.Page;
import com.gushenxing.commom.pojo.PageResult;
import com.gushenxing.commom.pojo.Result;
import com.gushenxing.commom.pojo.StatusCode;
import com.gushenxing.system.pojo.TMenu;
import com.gushenxing.system.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping("/menu")
public class MenuController {


    @Autowired
    private MenuService menuService;

    /**
     * 查询全部数据
     * @return
     */
    @GetMapping
    public Result findAllMenu(Integer id) {
        try {
            List<TMenu> menuList = menuService.findAllMenu(id);
            return new Result(true,StatusCode.OK,"查询成功",menuList);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,StatusCode.OK,"查询失败");
        }
    }

    /***
     * 根据ID查询数据
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Result findById(@PathVariable Integer id){
        try {
            TMenu tMenu = menuService.findById(id);
            return new Result(true,StatusCode.OK,"查询成功",tMenu);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,StatusCode.ERROR,"查询失败");
        }
    }


    /***
     * 新增数据
     * @param tMenu
     * @return
     */
    @PostMapping
    public Result add(@RequestBody TMenu tMenu){
        try {
            menuService.add(tMenu);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,StatusCode.ERROR,"添加失败");
        }
        return new Result(true,StatusCode.OK,"添加成功");
    }


    /***
     * 修改数据
     * @param tMenu
     * @param id
     * @return
     */
    @PutMapping(value="/{id}")
    public Result update(@RequestBody TMenu tMenu, @PathVariable Integer id){
        try {
            tMenu.setId(id);
            menuService.update(tMenu);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,StatusCode.ERROR,"修改失败");
        }
        return new Result(true,StatusCode.OK,"修改成功");
    }


    /***
     * 根据ID删除品牌数据
     * @param id
     * @return
     */
    @DeleteMapping(value = "/{id}" )
    public Result delete(@PathVariable Integer id){
        try {
            menuService.delete(id);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,StatusCode.ERROR,"删除失败");
        }
        return new Result(true,StatusCode.OK,"删除成功");
    }


    /***
     * 多条件搜索品牌数据
     * @param searchMap
     * @return
     */
    @GetMapping(value = "/search" )
    public Result findList(@RequestParam Map searchMap){
        try {
            List<TMenu> list = menuService.findList(searchMap);
            return new Result(true,StatusCode.OK,"查询成功",list);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,StatusCode.ERROR,"查询成功");
        }
    }


    /***
     * 分页搜索实现
     * @param searchMap
     * @param page
     * @param size
     * @return
     */
    @GetMapping(value = "/search/{page}/{size}" )
    public Result findPage(@RequestParam Map searchMap, @PathVariable  int page, @PathVariable  int size){
        Page<TMenu> pageList = menuService.findPage(searchMap, page, size);
        PageResult pageResult=new PageResult(pageList.getTotal(),pageList.getResult());
        return new Result(true,StatusCode.OK,"查询成功",pageResult);
    }


}
