package com.gushenxing.system.service.impl;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import com.gushenxing.custuser.pojo.CustUser;
import com.gushenxing.custuser.pojo.Customer;
import com.gushenxing.system.dao.CustUserMapper;
import com.gushenxing.system.dao.CustomerMapper;
import com.gushenxing.system.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.Map;

@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerMapper customerMapper;

    @Autowired
    private CustUserMapper custUserMapper;


    //添加农场组织
    @Override
    public void add(Customer customer) {
        //添加农场组织
        //设置农场组织创建时间
        customer.setCreateTime(new Date());
        customerMapper.add(customer);
        //添加农场组织超级管理员
        CustUser custUser = new CustUser();
        custUser.setCustomerId(customer.getId());
        custUser.setName("admin");
        custUser.setUsername("user"+customer.getId());
        custUser.setPassword("1111");
        //设置用户所属分类
        //custUser.setLevel("1"); //属于后台用户
        custUserMapper.add(custUser);

        //向用户角色表中添加数据进行用户与角色的关联
        custUserMapper.addUserAndRole(custUser.getId(), 5);

    }

    //修改农场组织
    @Override
    public void update(Customer customer) {

        //更新农场组织信息
        int row = customerMapper.updateByPrimaryKeySelective(customer);


    }

    //多条件分页查询
    @Override
    public Page<Customer> findPage(Map<String, Object> searchMap, int page, int size) {
        PageHelper.startPage(page, size);
        Example example = createExample(searchMap);
        return (Page<Customer>) customerMapper.selectByExample(example);
    }

    //根据id查询
    @Override
    public Customer findById(Integer id) {
        return customerMapper.selectByPrimaryKey(id);
    }

    /**
     * 构建查询对象
     *
     * @param searchMap
     * @return
     */
    private Example createExample(Map<String, Object> searchMap) {
        Example example = new Example(Customer.class);
        Example.Criteria criteria = example.createCriteria();

        if (searchMap != null) {

            // 机构代码
            if (searchMap.get("code") != null && !"".equals(searchMap.get("code"))) {
                criteria.andEqualTo("code", searchMap.get("code"));
            }
            // 农场组织名称
            if (searchMap.get("name") != null && !"".equals(searchMap.get("name"))) {
                criteria.andLike("name", "%" + searchMap.get("name") + "%");
            }
            // 农场地址
            if (searchMap.get("address") != null && !"".equals(searchMap.get("address"))) {
                criteria.andLike("address", "%" + searchMap.get("address") + "%");
            }
            // 联系人
            if (searchMap.get("contacts") != null && !"".equals(searchMap.get("contacts"))) {
                criteria.andLike("contacts", "%" + searchMap.get("contacts") + "%");
            }
            // 电话
            if (searchMap.get("phone") != null && !"".equals(searchMap.get("phone"))) {
                criteria.andEqualTo("phone", searchMap.get("phone"));
            }
            // 邮箱
            if (searchMap.get("email") != null && !"".equals(searchMap.get("email"))) {
                criteria.andEqualTo("email", "%" + searchMap.get("email") + "%");
            }

            // 状态 0-未激活，1-启用，-1-停用
            if (searchMap.get("status") != null && !"".equals(searchMap.get("status"))) {
                criteria.andEqualTo("status", searchMap.get("status"));
            }


        }
        return example;
    }
}
