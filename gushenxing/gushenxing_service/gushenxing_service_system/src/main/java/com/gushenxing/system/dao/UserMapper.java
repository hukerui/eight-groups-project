package com.gushenxing.system.dao;


import com.github.pagehelper.Page;
import com.gushenxing.system.pojo.TRole;
import com.gushenxing.system.pojo.TUser;
import org.apache.ibatis.annotations.*;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface UserMapper extends Mapper<TUser> {

    @Update("update t_user set status='0' where id=#{id}")
    void del(Integer id);

    @Insert("insert into t_user_role(uid,rid) values (#{uid},#{rid})")
    void set(Map<String, Integer> map);

    @Delete("delete from t_user_role where uid = #{id}")
    void deleteAssociation(Integer id);

    @Select("select rid from t_user_role where uid = #{id}")
    List<Integer> findRoleByUserId(Integer id);

    @Select("select * from t_user where username=#{s}")
    TUser findByUsername(String s);

    @Select("select u.* from t_user u,t_user_role ur where u.id=ur.uid and ur.rid=#{id}")
    Set<TUser> findUserByRoleId(Integer id);
}
