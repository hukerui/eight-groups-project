package com.gushenxing.system.service;


import com.github.pagehelper.Page;
import com.gushenxing.system.pojo.TMenu;

import java.util.List;
import java.util.Map;

public interface MenuService {

    /***
     * 查询所有
     * @return
     */
    List<TMenu> findAllMenu(Integer id);

    /**
     * 根据ID查询
     * @param id
     * @return
     */
    TMenu findById(Integer id);

    /***
     * 新增
     * @param tMenu
     */
    void add(TMenu tMenu);

    /***
     * 修改
     * @param tMenu
     */
    void update(TMenu tMenu);

    /***
     * 删除
     * @param id
     */
    void delete(Integer id);


    /***
     * 多条件搜索
     * @param searchMap
     * @return
     */
    List<TMenu> findList(Map<String, Object> searchMap);

    /***
     * 分页查询
     * @param page
     * @param size
     * @return
     */
    Page<TMenu> findPage(int page, int size);

    /***
     * 多条件分页查询
     * @param searchMap
     * @param page
     * @param size
     * @return
     */
    Page<TMenu> findPage(Map<String, Object> searchMap, int page, int size);







}
