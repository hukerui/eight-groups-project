package com.gushenxing.system.dao;



import com.gushenxing.system.pojo.TMenu;
import com.gushenxing.system.pojo.TRole;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface RoleMapper extends Mapper<TRole> {

    @Select("select m.* from t_menu m,t_role_menu rm where m.id=rm.mid and rm.rid=#{id}")
    Set<TMenu> findMenuByRoleId(Integer id);

    @Delete("delete from t_role_menu where rid = #{rid}")
    void deleteAssociation(Integer rid);

    @Insert("insert into t_role_menu(rid,mid) values (#{rid},#{mid})")
    void set(Map<String, Integer> map);

    @Select("select r.* from t_role r,t_user_role ur where r.id=ur.rid and ur.uid=#{id}")
    Set<TRole> findById(Integer id);

    @Select("select r.* from t_role r,t_role_menu rm where r.id=rm.rid and rm.mid=#{id}")
    Set<TRole> findRoleByMenuId(Integer id);


}
