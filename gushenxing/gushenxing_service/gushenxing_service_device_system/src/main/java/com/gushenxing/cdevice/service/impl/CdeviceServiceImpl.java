package com.gushenxing.cdevice.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.gushenxing.cdevice.dao.CdeviceMapper;
import com.gushenxing.cdevice.service.CdeviceService;
import com.gushenxing.devices.pojo.Cdevice;
import org.springframework.beans.factory.annotation.Autowired;

public class CdeviceServiceImpl implements CdeviceService {
    @Autowired
    private CdeviceMapper cdeviceMapper;

    //分页查询
    @Override
    public Page<Cdevice> findPage(int page, int size) {
        PageHelper.startPage(page, size);
        return (Page<Cdevice>) cdeviceMapper.selectAll();
    }

    //添加设备
    @Override
    public void add(Cdevice cdevice) {
        cdeviceMapper.insert(cdevice);
    }

    //修改设备
    @Override
    public void update(Cdevice cdevice) {
        cdeviceMapper.updateByPrimaryKey(cdevice);
    }

    //根据id删除设备
    @Override
    public void delById(Integer id) {
        cdeviceMapper.deleteByPrimaryKey(id);
    }
}
