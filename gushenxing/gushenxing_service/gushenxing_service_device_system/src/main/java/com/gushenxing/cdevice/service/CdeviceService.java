package com.gushenxing.cdevice.service;

import com.github.pagehelper.Page;
import com.gushenxing.devices.pojo.Cdevice;


public interface CdeviceService {

    //分页查询
    public Page<Cdevice> findPage(int page, int size);

    //设备的添加
    public void add(Cdevice cdevice);

    //设备的修改
    public void update(Cdevice cdevice);

    //根据id删除设备
    public void delById(Integer id);


}
