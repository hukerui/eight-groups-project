package com.gushenxing.cdevice.dao;

import com.gushenxing.devices.pojo.Cdevice;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;


@Repository
public interface CdeviceMapper extends Mapper<Cdevice> {
}


