package com.gushenxing.cdevice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@EnableEurekaClient //声明当前的工程是eureka客户端
@MapperScan(basePackages = {"com.gushenxing.cdevice.dao"})
@ComponentScan(basePackages = "com.gushenxing")
public class CdevicesApplication {

    public static void main(String[] args) {
        SpringApplication.run(CdevicesApplication.class,args);
    }
}
