package com.gushenxing.login.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigInteger;


@Table(name = "t_user")
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
/*@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)*/
public class UserInfo implements Serializable {

    @Id
    private Integer id;
    private String isload;
    private String isdelete;
    private String name;
    private String username;
    private String password;
    private String email;
    private String phone;
    private String status;
}
