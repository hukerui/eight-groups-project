package com.gushenxing.login.feign;


import com.gushenxing.commom.pojo.Result;
import com.gushenxing.login.pojo.UserInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;

@FeignClient(name = "login")
public interface UserFeign {

    //根据用户名查询实体
    @GetMapping("/user/load/{username}")
    public UserInfo findUserInfo(@PathVariable("username") String username);

    //账户锁定修改用户状态
    @PutMapping("/user/edit/{username}")
    public Result edit(@PathVariable("username") String username);


    //用户首次登录修改密码
    @PutMapping("/user/updataUser/{username}/{password}")
    public Result updataUser(@PathVariable("username") String username, @PathVariable("password") String password);

}
