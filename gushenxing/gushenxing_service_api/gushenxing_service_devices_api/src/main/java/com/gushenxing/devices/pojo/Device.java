package com.gushenxing.devices.pojo;

import lombok.Data;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@ToString
@Entity
@Table(name = "t_device")
public class Device implements Serializable {
        private Integer  id;
        private String  code;
        private String  name;
        private String  type;
        private String  price;
        private String  brand;
        private String  model;
        private String  scenarios;

}
