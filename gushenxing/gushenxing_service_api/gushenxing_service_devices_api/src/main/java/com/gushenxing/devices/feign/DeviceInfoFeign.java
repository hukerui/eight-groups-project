package com.gushenxing.devices.feign;


import com.gushenxing.commom.pojo.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "device")
public interface DeviceInfoFeign {
    @GetMapping("/device_index/findByDid/{did}")
    public Result findByDid(@PathVariable("did") Integer did);
}
