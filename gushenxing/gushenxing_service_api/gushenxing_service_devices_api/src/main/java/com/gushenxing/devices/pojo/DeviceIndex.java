package com.gushenxing.devices.pojo;

import lombok.Data;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.List;

@Data
@ToString
@Entity
@Table(name = "t_device_index")
public class DeviceIndex {

    private Integer id;
    private Integer did;                  //设备ID;
    private List<Integer> iid;            //数据指标ID;
    private String rangeMax;             //测量范围最大值;
    private String rangeMin;             //测量范围最小值;
    private String accuracy;             //精度;
    private String resolving_power;      //分辨率
}
