package com.gushenxing.devices.pojo;


import lombok.Data;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@ToString
@Entity
@Table(name = "t_device_info")
public class DeviceInfo {
    private Integer id;
    private Integer did;
    private String info;
    private String install;
    private String dbtype;
    private String URL;

}
