package com.gushenxing.system.pojo;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * @Description  
 * @Author  meitonglin
 * @Date 2020-10-11 
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity
@Table ( name ="t_user" )
public class TUser implements Serializable {

	private static final long serialVersionUID =  6793013093724523677L;

   	@Column(name = "id")
	@Id
	@GeneratedValue(generator = "JDBC")
	private int id;

	/**
	 * 1:是。0:不是
	 */
   	@Column(name = "isload" )
	private String isload;

	/**
	 * 1:是。0:不是
	 */
   	@Column(name = "isdelete" )
	private String isdelete;

	/**
	 * 姓名
	 */
   	@Column(name = "name" )
	private String name;

	/**
	 * 登录名
	 */
   	@Column(name = "username" )
	private String username;

	/**
	 * 密码
	 */
   	@Column(name = "password" )
	private String password;

	/**
	 * 邮箱
	 */
   	@Column(name = "email" )
	private String email;

	/**
	 * 电话
	 */
   	@Column(name = "phone" )
	private String phone;

	/**
	 * 1:活跃。0:停用
	 */
   	@Column(name = "`status`" )
	private String status;


	private Set<TRole> roles = new HashSet<TRole>(0);//对应角色集合

	public TUser(Set<TRole> roles) {
		this.roles = roles;
	}

	public TUser(String username, Set<TRole> roles) {
		this.username = username;
		this.roles = roles;
	}

	public TUser(int id, Set<TRole> roles) {
		this.id = id;
		this.roles = roles;
	}

	public Set<TRole> getRoles() {
		return roles;
	}

	public void setRoles(Set<TRole> roles) {
		this.roles = roles;
	}
}
