package com.gushenxing.system.pojo;

import javax.persistence.*;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * @Description  
 * @Author  meitonglin
 * @Date 2020-10-11 
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity
@Table ( name ="t_user_role" )
public class TUserRole  implements Serializable {

	private static final long serialVersionUID =  6609207416330406733L;

   	@Column(name = "uid"  )
	@Id
	@GeneratedValue(generator = "JDBC")
	private int uid;

   	@Column(name = "rid" )
	@Id
	@GeneratedValue(generator = "JDBC")
	private int rid;

}
