package com.gushenxing.system.pojo;

import javax.persistence.*;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * @Description  
 * @Author  meitonglin
 * @Date 2020-10-11 
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity
@Table ( name ="t_role_menu" )
public class TRoleMenu  implements Serializable {

	private static final long serialVersionUID =  682282925138309984L;

   	@Column(name = "rid" )
	@Id
	@GeneratedValue(generator = "JDBC")
	private int rid;

   	@Column(name = "mid" )
	@Id
	@GeneratedValue(generator = "JDBC")
	private int mid;

}
