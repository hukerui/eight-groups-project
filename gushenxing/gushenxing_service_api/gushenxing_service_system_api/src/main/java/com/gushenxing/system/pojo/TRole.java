package com.gushenxing.system.pojo;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * @Description  
 * @Author  meitonglin
 * @Date 2020-10-11 
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity
@Table ( name ="t_role" )
public class TRole  implements Serializable {

	private static final long serialVersionUID =  1627172387030982621L;

   	@Column(name = "id" )
	@Id
	@GeneratedValue(generator = "JDBC")
	private int id;

	/**
	 * 角色名
	 */
   	@Column(name = "role" )
	private String role;

	/**
	 * 说明
	 */
   	@Column(name = "info" )
	private String info;

	/**
	 * 0:后台角色，1:平台角色
	 */
   	@Column(name = "category" )
	private String category;




	public TRole(LinkedHashSet<TMenu> menus) {
		this.menus = menus;
	}

	public TRole(int id, LinkedHashSet<TMenu> menus) {
		this.id = id;
		this.menus = menus;
	}

	public TRole(String role, Set<TUser> users) {
		this.role = role;
		this.users = users;
	}

	public TRole(int id, Set<TUser> users) {
		this.id = id;
		this.users = users;
	}

	public TRole(int id, Set<TUser> users, LinkedHashSet<TMenu> menus) {
		this.id = id;
		this.users = users;
		this.menus = menus;
	}

	public TRole(Set<TUser> users, LinkedHashSet<TMenu> menus) {
		this.users = users;
		this.menus = menus;
	}

	private Set<TUser> users = new HashSet<TUser>(0);

	private Set<TMenu> menus = new HashSet<TMenu>(0);//对应菜单集合
}
