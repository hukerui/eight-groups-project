package com.gushenxing.system.feign;


import com.gushenxing.commom.pojo.Result;
import com.gushenxing.system.pojo.TUser;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "user")
public interface TUserFeign {
    @GetMapping("/user/findByPhone/{phone}")
    public Result<TUser> findByPhone(@PathVariable("phone") String phone);
}
