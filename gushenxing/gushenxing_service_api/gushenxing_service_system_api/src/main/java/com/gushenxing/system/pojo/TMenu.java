package com.gushenxing.system.pojo;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * @Description  
 * @Author  meitonglin
 * @Date 2020-10-11 
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity
@Table ( name ="t_menu" )
public class TMenu  implements Serializable {

	private static final long serialVersionUID =  950391186715742816L;

   	@Column(name = "id" )
	@Id
	@GeneratedValue(generator = "JDBC")
	private int id;

	/**
	 * 菜单或按钮名称
	 */
   	@Column(name = "menu" )
	private String menu;

	/**
	 * 菜单或按钮说明
	 */
   	@Column(name = "info" )
	private String info;

	/**
	 * 菜单请求链接
	 */
   	@Column(name = "url" )
	private String url;

	/**
	 * 上级菜单
	 */
   	@Column(name = "pid" )
	private int pid;

	/**
	 * 1:菜单。2:按钮。
	 */
   	@Column(name = "type" )
	private String type;

	/**
	 * 0:后台菜单，1:平台菜单
	 */
   	@Column(name = "category" )
	private String category;

	private Set<TRole> roles = new HashSet<TRole>(0);//角色集合

	private List<TMenu> children = new ArrayList<>();//子菜单集合

}
