package com.gushenxing.crops.pojo;

import javax.persistence.*;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * @Description  
 * @Author  meitonglin
 * @Date 2020-10-11 
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
@Table ( name ="t_index" )
public class TIndex  implements Serializable {

	private static final long serialVersionUID =  6152302352102021565L;
	@Id
	@GeneratedValue(generator = "JDBC")
   	@Column(name = "id" )
	private Integer id;

	/**
	 * 指标名称
	 */
   	@Column(name = "name" )
	private String name;

	/**
	 * 计量单位
	 */
   	@Column(name = "unit" )
	private String unit;

}
