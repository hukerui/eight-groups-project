package com.gushenxing.crops.pojo;

import javax.persistence.*;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * @Description  
 * @Author  meitonglin
 * @Date 2020-10-11 
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
@Table ( name ="t_crops_disease" )
public class TCropsDisease  implements Serializable {

	private static final long serialVersionUID =  9198989311033834135L;
	@Id
	@GeneratedValue(generator = "JDBC")
   	@Column(name = "id" )
	private Integer id;

	/**
	 * 农作物id
	 */
   	@Column(name = "cid" )
	private Integer cid;

	/**
	 * 病虫id
	 */
   	@Column(name = "pid" )
	private Integer pid;

	/**
	 * 农作物名称
	 */
   	@Column(name = "crops" )
	private String crops;

	/**
	 * 病虫名称
	 */
   	@Column(name = "pest" )
	private String pest;

	/**
	 * 表现特征
	 */
   	@Column(name = "aspect" )
	private String aspect;

	/**
	 * 病虫害数据范围
	 */
   	@Column(name = "disease_range" )
	private String diseaseRange;

	/**
	 * 正常数据指标
	 */
   	@Column(name = "normal_range" )
	private Integer normalRange;

	/**
	 * 防治方式
	 */
   	@Column(name = "cure" )
	private String cure;

	/**
	 * 生命阶段
	 */
   	@Column(name = "stage" )
	private String stage;

	/**
	 * 状态
	 */
   	@Column(name = "status" )
	private Integer status;

}
