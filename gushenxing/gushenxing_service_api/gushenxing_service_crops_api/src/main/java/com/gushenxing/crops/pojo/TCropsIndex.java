package com.gushenxing.crops.pojo;

import javax.persistence.*;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * @Description  
 * @Author  meitonglin
 * @Date 2020-10-11 
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
@Table ( name ="t_crops_index" )
public class TCropsIndex  implements Serializable {

	private static final long serialVersionUID =  4812557921458710041L;
	@Id
	@GeneratedValue(generator = "JDBC")
   	@Column(name = "id" )
	private Integer id;

	/**
	 * 植物ID
	 */
   	@Column(name = "cid" )
	private Integer cid;

	/**
	 * 指标ID
	 */
   	@Column(name = "iid" )
	private Integer iid;

	/**
	 * 生长阶段
	 */
   	@Column(name = "stage" )
	private String stage;

	/**
	 * 最大范围
	 */
   	@Column(name = "max_range" )
	private String maxRange;

	/**
	 * 最小范围
	 */
   	@Column(name = "min_range" )
	private String minRange;

	/**
	 * 最佳范围
	 */
   	@Column(name = "opt_range" )
	private String optRange;

	/**
	 * 农作物状态
	 */
   	@Column(name = "status" )
	private Integer status;

}
