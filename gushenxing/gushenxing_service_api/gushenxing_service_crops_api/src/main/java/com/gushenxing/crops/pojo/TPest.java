package com.gushenxing.crops.pojo;

import javax.persistence.*;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * @Description  
 * @Author  meitonglin
 * @Date 2020-10-12 
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
@Table ( name ="t_pest" )
public class TPest  implements Serializable {

	private static final long serialVersionUID =  206818731961353217L;
     @Id
	 @GeneratedValue(generator = "JDBC")
   	@Column(name = "id" )
	private Integer id;

	/**
	 * 病虫
	 */
   	@Column(name = "pest" )
	private String pest;

}
