package com.gushenxing.crops.pojo;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @Description  
 * @Author  meitonglin
 * @Date 2020-10-11 
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
@Table ( name ="t_crops" )
public class TCrops  implements Serializable {

	private static final long serialVersionUID =  4011140100736133740L;

	@Id
	@GeneratedValue(generator = "JDBC")
	private Integer id;
	private String name;
	private String alias;
	private String cdoosgs;
	private String info;
	private String area;
	private String aspect;
	private String cultivation;
	private String stage;
	private String attention;
	private String image;
	private Integer status;
	private List<TIndex> indexList;
	private List<TPest> pestList;

	public TCrops(String name, String alias, String cdoosgs, String info, String area, String aspect, String cultivation, String stage,  String attention, String image, Integer status, List<TIndex> indexList, List<TPest> pestList) {
		this.name = name;
		this.alias = alias;
		this.cdoosgs = cdoosgs;
		this.info = info;
		this.area = area;
		this.aspect = aspect;
		this.cultivation = cultivation;
		this.stage = stage;

		this.attention = attention;
		this.image = image;
		this.status = status;
		this.indexList = indexList;
		this.pestList = pestList;
	}

	public TCrops() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getCdoosgs() {
		return cdoosgs;
	}

	public void setCdoosgs(String cdoosgs) {
		this.cdoosgs = cdoosgs;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getAspect() {
		return aspect;
	}

	public void setAspect(String aspect) {
		this.aspect = aspect;
	}

	public String getCultivation() {
		return cultivation;
	}

	public void setCultivation(String cultivation) {
		this.cultivation = cultivation;
	}

	public String getStage() {
		return stage;
	}

	public void setStage(String stage) {
		this.stage = stage;
	}



	public String getAttention() {
		return attention;
	}

	public void setAttention(String attention) {
		this.attention = attention;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public List<TIndex> getIndexList() {
		return indexList;
	}

	public void setIndexList(List<TIndex> indexList) {
		this.indexList = indexList;
	}

	public List<TPest> getPestList() {
		return pestList;
	}

	public void setPestList(List<TPest> pestList) {
		this.pestList = pestList;
	}

}