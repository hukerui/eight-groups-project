package com.gushenxing.device.pojo;

import lombok.Data;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@ToString
@Entity
@Table(name = "t_cust_device")
public class CustDevice {
    private Integer id;
    private Integer cid;
    private Integer did;
    private String code;
    private String status;
    private String install;
    private String position;
}
