package com.gushenxing.device.pojo;

import lombok.Data;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@ToString
@Entity
@Table(name = "t_buy")
public class Buy {
    private Integer id;
    private Integer cid;
    private Integer did;
    private String place;
    private String tel;
    private Integer num;
    private String status;
    private String price;

}
