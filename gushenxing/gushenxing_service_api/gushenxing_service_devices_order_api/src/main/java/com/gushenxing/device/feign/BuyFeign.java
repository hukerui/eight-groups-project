package com.gushenxing.device.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;


@FeignClient(name = "buy")
public interface BuyFeign {

    @RequestMapping("/custdevice/updateByCustDeviceId/{cid}/{position}")
    public void updateByCustDeviceId(@PathVariable("cid") Integer cid, @PathVariable("position") String position);
}
