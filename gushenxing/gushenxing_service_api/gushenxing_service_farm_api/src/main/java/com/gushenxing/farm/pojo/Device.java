package com.gushenxing.farm.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * @Description  
 * @Author  meitonglin
 * @Date 2020-10-12 
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
@Table ( name ="t_device" )
public class Device implements Serializable {

	private static final long serialVersionUID =  7035027432570415309L;

	@Id
   	@Column(name = "id" )
	private Integer id;

	/**
	 * 编码
	 */
   	@Column(name = "code" )
	private String code;

	/**
	 * 设备名称
	 */
   	@Column(name = "name" )
	private String name;

	/**
	 * 设备类型
	 */
   	@Column(name = "type" )
	private String type;

	/**
	 * 价格，以分为单位。
	 */
   	@Column(name = "price" )
	private int price;

	/**
	 * 品牌
	 */
   	@Column(name = "brand" )
	private String brand;

	/**
	 * 型号
	 */
   	@Column(name = "model" )
	private String model;

	/**
	 * 应用场景
	 */
   	@Column(name = "scenarios" )
	private String scenarios;

}
