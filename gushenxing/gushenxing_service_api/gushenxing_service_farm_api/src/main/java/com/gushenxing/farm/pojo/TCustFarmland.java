package com.gushenxing.farm.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table ( name ="t_cust_farmland" )
public class TCustFarmland  implements Serializable {

   	@Column(name = "id" )
	@Id
	private Integer id;

	/**
	 * 所属于农场
	 */
   	@Column(name = "aid" )
	private Integer aid;

	/**
	 * 大小
	 */
   	@Column(name = "size" )
	private String size;

	/**
	 * 形状
	 */
   	@Column(name = "shape" )
	private String shape;

	/**
	 * 编号
	 */
   	@Column(name = "code" )
	private String code;

	/**
	 * 状态
	 */
   	@Column(name = "status" )
	private String status;

}
