package com.gushenxing.farm.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @Description  
 * @Author  meitonglin
 * @Date 2020-10-16 
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table ( name ="t_cust_farmland_crops" )
public class TCustFarmlandCrops  implements Serializable {

	private static final long serialVersionUID =  4293474666040001080L;

   	@Column(name = "id" )
	private Integer id;

	/**
	 * 大田ID
	 */
   	@Column(name = "fid" )
	private Integer fid;

	/**
	 * 作物ID
	 */
   	@Column(name = "cid" )
	private Integer cid;

	/**
	 * 农作物名称
	 */
   	@Column(name = "cname" )
	private String cname;

	/**
	 * 开始时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd")
   	@Column(name = "start_time" )
	private Date startTime;

	/**
	 * 结束时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd")
   	@Column(name = "end_time" )
	private Date endTime;

	/**
	 * 生长阶段
	 */
   	@Column(name = "stage" )
	private String stage;

}
