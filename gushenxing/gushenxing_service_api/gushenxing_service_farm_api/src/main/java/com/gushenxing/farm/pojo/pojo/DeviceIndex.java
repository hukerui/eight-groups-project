package com.gushenxing.farm.pojo.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @Description  
 * @Author  meitonglin
 * @Date 2020-10-14 
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table ( name ="t_device_index" )
public class DeviceIndex implements Serializable {

	private static final long serialVersionUID =  7161551626019898977L;

   	@Column(name = "id" )
	private Integer id;

	/**
	 * 设备ID
	 */
   	@Column(name = "did" )
	private Integer did;

	/**
	 * 数据指标ID
	 */
   	@Column(name = "iid" )
	private Integer iid;

	/**
	 * 测量范围最大值
	 */
   	@Column(name = "range_max" )
	private String rangeMax;

	/**
	 * 测量范围最小值
	 */
   	@Column(name = "range_min" )
	private String rangeMin;

	/**
	 * 精度
	 */
   	@Column(name = "accuracy" )
	private String accuracy;

	/**
	 * 分辨率
	 */
   	@Column(name = "resolving_power" )
	private String resolvingPower;

}
