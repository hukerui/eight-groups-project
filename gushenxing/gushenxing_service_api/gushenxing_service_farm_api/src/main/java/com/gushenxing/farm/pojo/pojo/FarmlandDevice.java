package com.gushenxing.farm.pojo.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
@Table ( name ="t_cust_farmland_device" )
public class FarmlandDevice implements Serializable {


   	@Column(name = "id" )
	private Integer id;

	/**
	 * 大田ID
	 */
   	@Column(name = "fid" )
	private Integer fid;

	/**
	 * 用户的设备ID
	 */
   	@Column(name = "did" )
	private Integer did;

	/**
	 * 设备安装的位置（经纬度）
	 */
   	@Column(name = "position" )
	private String position;

	/**
	 * 设备状态（0正常，1损坏）
	 */
   	@Column(name = "dstage" )
	private Integer dstage;

	/**
	 * 运行状态（0正常，1停止）
	 */
   	@Column(name = "mstage" )
	private Integer mstage;

}
