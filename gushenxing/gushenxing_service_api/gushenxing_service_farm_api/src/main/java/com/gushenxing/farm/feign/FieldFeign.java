package com.gushenxing.farm.feign;

import com.gushenxing.commom.pojo.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author Swift
 * @date 2020/10/15
 */

@FeignClient(name="farm")
public interface FieldFeign {

    /**
     *  通过大田的id去查询大田中的设备信息
     * @param id
     * @return
     */
    @GetMapping("/field/checkDevice/{id}")
    public Result checkDevice(@PathVariable("id")Integer id);
}
