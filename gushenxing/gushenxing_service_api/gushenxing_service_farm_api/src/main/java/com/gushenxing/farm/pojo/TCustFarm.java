package com.gushenxing.farm.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table ( name ="t_cust_farm" )
public class TCustFarm  implements Serializable {


   	@Column(name = "id" )
	@Id
	private Integer id;

	/**
	 * 本农场所属组织
	 */
   	@Column(name = "cust_id" )
	private Integer custId;

	/**
	 * 农场编号
	 */
   	@Column(name = "code" )
	private String code;

	/**
	 * 农场名称
	 */
   	@Column(name = "name" )
	private String name;

	/**
	 * 农场状态
	 */
   	@Column(name = "status" )
	private String status;

	/**
	 * 农场经度
	 */
   	@Column(name = "longitude" )
	private String longitude;

	/**
	 * 农场纬度
	 */
   	@Column(name = "latitude" )
	private String latitude;

}
