package com.gushenxing.farm.pojo.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @Description  
 * @Author  meitonglin
 * @Date 2020-10-12 
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table ( name ="t_cust_farmland_crops" )
public class CustFarmlandCrops implements Serializable {

	private static final long serialVersionUID =  591441816254308555L;

   	@Column(name = "id" )
	private Integer id;

	/**
	 * 大田ID
	 */
   	@Column(name = "fid" )
	private Integer fid;

	/**
	 * 作物ID
	 */
   	@Column(name = "cid" )
	private Integer cid;

   //农作物名称
   	@Column(name = "cname")
	private  String cname;

	/**
	 * 开始时间
	 */
   	@Column(name = "start_time" )
	private String startTime;

	/**
	 * 结束时间
	 */
   	@Column(name = "end_time" )
	private String endTime;

	/**
	 * 生长阶段
	 */
   	@Column(name = "stage" )
	private String stage;

}
