package com.gushenxing.farm.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * @Description  
 * @Author  meitonglin
 * @Date 2020-10-12 
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
@Table ( name ="t_index" )
public class Index implements Serializable {

	private static final long serialVersionUID =  1927442208424417740L;

   	@Column(name = "id" )
	private Integer id;

	/**
	 * 指标名称
	 */
   	@Column(name = "name" )
	private String name;

	/**
	 * 计量单位
	 */
   	@Column(name = "unit" )
	private String unit;

}
