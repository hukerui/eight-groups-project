package com.gushenxing.farm.pojo.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @Description  
 * @Author  meitonglin
 * @Date 2020-10-12 
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
@Table ( name ="t_crops" )
public class Crops implements Serializable {

	private static final long serialVersionUID =  470794977866298306L;

	/**
	 * 农作物id
	 */
   	@Column(name = "id" )
	private Integer id;

	/**
	 * 农作物名称
	 */
   	@Column(name = "name" )
	private String name;

	/**
	 * 农作物别名
	 */
   	@Column(name = "alias" )
	private String alias;

	/**
	 * 界门纲目科属种
	 */
   	@Column(name = "cdoosgs" )
	private String cdoosgs;

	/**
	 * 农作物简介
	 */
   	@Column(name = "info" )
	private String info;

	/**
	 * 分布区域
	 */
   	@Column(name = "area" )
	private String area;

	/**
	 * 农作物外观
	 */
   	@Column(name = "aspect" )
	private String aspect;

	/**
	 * 栽培技术
	 */
   	@Column(name = "cultivation" )
	private String cultivation;

	/**
	 * 生命阶段
	 */
   	@Column(name = "stage" )
	private String stage;

	/**
	 * 注意事项
	 */
   	@Column(name = "attention" )
	private String attention;

	/**
	 * 图片
	 */
   	@Column(name = "image" )
	private String image;

	/**
	 * 农作物状态，0代表删除，1代表存在
	 */
   	@Column(name = "status" )
	private Integer status;

}
