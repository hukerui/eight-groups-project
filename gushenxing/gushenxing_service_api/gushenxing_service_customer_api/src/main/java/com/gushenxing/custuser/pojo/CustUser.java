package com.gushenxing.custuser.pojo;

import javax.persistence.*;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * @Description  
 * @Author  meitonglin
 * @Date 2020-10-11 
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
@Table ( name ="t_cust_user" )
public class CustUser implements Serializable {

	private static final long serialVersionUID =  8415446783588455732L;

	@Id
	@GeneratedValue(generator = "JDBC")
	@Column(name = "id" )
	private Integer id;

	/**
	 * 所属农业组织的ID。
	 */
   	@Column(name = "customer_id" )
	private Integer customerId;

	/**
	 * 用户名
	 */
   	@Column(name = "name" )
	private String name;

	/**
	 * 登录名
	 */
   	@Column(name = "username" )
	private String username;

	/**
	 * 密码
	 */
   	@Column(name = "password" )
	private String password;

	/**
	 * 0:停用，1:启用。
	 */
   	@Column(name = "status" )
	private String status;

	/**
	 * 0:不是，1：是
	 */
   	@Column(name = "isdelete" )
	private String isdelete;

	/**
	 * 0:不是，1：是
	 */
	@Column(name = "isload" )
	private String isload;

}
