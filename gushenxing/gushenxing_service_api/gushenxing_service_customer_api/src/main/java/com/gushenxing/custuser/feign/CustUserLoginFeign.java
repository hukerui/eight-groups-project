package com.gushenxing.custuser.feign;

import com.gushenxing.commom.pojo.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author Swift
 * @date 2020/10/17
 */
@FeignClient(name = "custuserlogin")
public interface CustUserLoginFeign {
    @GetMapping("/custuser/login")
    public Result login(@RequestParam("username")String username, @RequestParam("password")String password);
}
