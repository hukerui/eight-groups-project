package com.gushenxing.custuser.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * @Description  
 * @Author  meitonglin
 * @Date 2020-10-12 
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
@Table ( name ="t_role" )
public class Role implements Serializable {

	private static final long serialVersionUID =  4951591298392103830L;

   	@Column(name = "id" )
	private Integer id;

	/**
	 * 角色名
	 */
   	@Column(name = "role" )
	private String role;

	/**
	 * 说明
	 */
   	@Column(name = "info" )
	private String info;

	/**
	 * 0:后台角色，1:平台角色
	 */
   	@Column(name = "category" )
	private String category;

}
