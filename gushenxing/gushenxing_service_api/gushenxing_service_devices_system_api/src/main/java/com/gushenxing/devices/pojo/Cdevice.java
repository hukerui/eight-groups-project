package com.gushenxing.devices.pojo;

import lombok.Data;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@ToString
@Entity
@Table(name = "t_cust_device")
public class Cdevice {
    private Integer id;
    private Integer cid;
    private Integer did;
    private String code;
    private String status;
    private String install;
    private String position;
}
