package com.gushenxing.gateway.filter;

public class UrlFilter {
    //需要拦截的请求地址
    public static String filterPath = "/api/wcustomer/**,/api/custuserInfo/**,/api/customer/**,/api/system/**,/api/field/**,/api/farmland/**,/api/farmlandcrops/**,/api/area/**,/api/crops/**,/api/index/**,/api/disease/**";
    public static boolean hasAuthorize(String url){
        String[] split = filterPath.replace("/**", "").split(",");
        for (String value : split) {
            if (url.startsWith(value)){
                return true;
            }
        }
        return false;
    }
}
