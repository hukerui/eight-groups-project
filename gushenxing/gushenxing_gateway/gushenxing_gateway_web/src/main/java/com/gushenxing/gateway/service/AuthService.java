package com.gushenxing.gateway.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.HttpCookie;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;


@Service
public class AuthService {

    @Autowired
    private StringRedisTemplate redisTemplate;

    public String getJtiFromCookie(ServerHttpRequest request){
        //查看cookie中jti是否存在
        //获取cookie对象
        MultiValueMap<String, HttpCookie> cookies = request.getCookies();
        HttpCookie cookie = cookies.getFirst("uid");
        if(cookie!=null){
            String jti = cookie.getValue();
            return jti;
        }
        return null;
    }

    //查看redis中的token是否过期或者存在
    public String getTokenFromRedis(String jti){
        String token = redisTemplate.boundValueOps(jti).get();
        return token;
    }
}
