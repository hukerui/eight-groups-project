package com.gushenxing.commom.pojo;

import java.io.Serializable;
import java.util.List;

/**
 * 分页对象3
 * @param <T>
 */
public class Page <T> implements Serializable{

	//当前默认为第一页
	public static final Integer pageNum = 1;
	//默认每页显示条件
	public static final Integer pageSize = 10;


	//判断当前页是否为空或是小于1
	public static Integer cpn(Integer pageNum){
		if(null == pageNum || pageNum < 1){
			pageNum = 1;
		}
		return pageNum;
	}


	// 页数（第几页）
	private Integer currentpage;

	// 查询数据库里面对应的数据有多少条
	private Integer total;// 从数据库查处的总记录数

	// 每页查5条
	private Integer size;

	// 下页
	private Integer next;
	
	private List<T> list;

	// 最后一页
	private Integer last;
	
	private Integer lpage;
	
	private Integer rpage;
	
	//从哪条开始查
	private Integer start;
	
	//全局偏移量
	public Integer offsize = 2;
	
	public Page() {
		super();
	}

	/****
	 *
	 * @param currentpage
	 * @param total
	 * @param pagesize
	 */
	public void setCurrentpage(Integer currentpage,Integer total,Integer pagesize) {
		//可以整除的情况下
		Integer pagecount =  total/pagesize;

		//如果整除表示正好分N页，如果不能整除在N页的基础上+1页
		Integer totalPages = (int) (total%pagesize==0? total/pagesize : (total/pagesize)+1);

		//总页数
		this.last = totalPages;

		//判断当前页是否越界,如果越界，我们就查最后一页
		if(currentpage>totalPages){
			this.currentpage = totalPages;
		}else{
			this.currentpage=currentpage;
		}

		//计算start
		this.start = (this.currentpage-1)*pagesize;
	}

	//上一页
	public Integer getUpper() {
		return currentpage>1? currentpage-1: currentpage;
	}

	//总共有多少页，即末页
	public void setLast(Integer last) {
		this.last = (int) (total%size==0? total/size : (total/size)+1);
	}

	/****
	 * 带有偏移量设置的分页
	 * @param total
	 * @param currentpage
	 * @param pagesize
	 * @param offsize
	 */
	public Page(Integer total,Integer currentpage,Integer pagesize,Integer offsize) {
		this.offsize = offsize;
		initPage(total, currentpage, pagesize);
	}

	/****
	 *
	 * @param total   总记录数
	 * @param currentpage	当前页
	 * @param pagesize	每页显示多少条
	 */
	public Page(Integer total,Integer currentpage,Integer pagesize) {
		initPage(total,currentpage,pagesize);
	}

	/****
	 * 初始化分页
	 * @param total
	 * @param currentpage
	 * @param pagesize
	 */
	public void initPage(Integer total,Integer currentpage,Integer pagesize){
		//总记录数
		this.total = total;
		//每页显示多少条
		this.size=pagesize;

		//计算当前页和数据库查询起始值以及总页数
		setCurrentpage(currentpage, total, pagesize);

		//分页计算
		Integer leftcount =this.offsize,	//需要向上一页执行多少次
				rightcount =this.offsize;

		//起点页
		this.lpage =currentpage;
		//结束页
		this.rpage =currentpage;

		//2点判断
		this.lpage = currentpage-leftcount;			//正常情况下的起点
		this.rpage = currentpage+rightcount;		//正常情况下的终点

		//页差=总页数和结束页的差
		Integer topdiv = this.last-rpage;				//判断是否大于最大页数

		/***
		 * 起点页
		 * 1、页差<0  起点页=起点页+页差值
		 * 2、页差>=0 起点和终点判断
		 */
		this.lpage=topdiv<0? this.lpage+topdiv:this.lpage;

		/***
		 * 结束页
		 * 1、起点页<=0   结束页=|起点页|+1
		 * 2、起点页>0    结束页
		 */
		this.rpage=this.lpage<=0? this.rpage+(this.lpage*-1)+1: this.rpage;

		/***
		 * 当起点页<=0  让起点页为第一页
		 * 否则不管
		 */
		this.lpage=this.lpage<=0? 1:this.lpage;

		/***
		 * 如果结束页>总页数   结束页=总页数
		 * 否则不管
		 */
		this.rpage=this.rpage>last? this.last:this.rpage;
	}

	public Integer getNext() {
		return  currentpage<last? currentpage+1: last;
	}

	public void setNext(Integer next) {
		this.next = next;
	}

	public Integer getCurrentpage() {
		return currentpage;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public Integer getLast() {
		return last;
	}

	public Integer getLpage() {
		return lpage;
	}

	public void setLpage(Integer lpage) {
		this.lpage = lpage;
	}

	public Integer getRpage() {
		return rpage;
	}

	public void setRpage(Integer rpage) {
		this.rpage = rpage;
	}

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public void setCurrentpage(Integer currentpage) {
		this.currentpage = currentpage;
	}

	/**
	 * @return the list
	 */
	public List<T> getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(List<T> list) {
		this.list = list;
	}

	public static void main(String[] args) {
			//总记录数
			//当前页
			//每页显示多少条
			Integer cpage =17;
			Page page = new Page(1001,cpage,50,7);
			System.out.println("开始页:"+page.getLpage()+"__当前页："+page.getCurrentpage()+"__结束页"+page.getRpage()+"____总页数："+page.getLast());
	}
}
