package com.gushenxing.commom.utils;


import org.apache.commons.io.output.ByteArrayOutputStream;

import java.io.IOException;
import java.io.InputStream;

/**
 * 转换工具类
 */
public class ConvertUtils {
    /**
     * 输入流转换为xml字符串
     *
     * @param inputStream
     * @return
     */

    public static String convertToString(InputStream inputStream) throws IOException {

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        byte[] bytes = new byte[1024];
        int len =0;
        while ((len=inputStream.read(bytes))!=-1){
            outputStream.write(bytes,0,len);
        }
        outputStream.close();
        inputStream.close();
        String result = new String(outputStream.toByteArray(), "utf-8");
        return result;
    }

}
