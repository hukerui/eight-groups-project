package com.gushenxing.oauth;

import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.security.jwt.crypto.sign.RsaSigner;
import org.springframework.security.jwt.crypto.sign.Signer;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;

import java.security.KeyPair;
import java.security.interfaces.RSAPrivateCrtKey;
import java.security.interfaces.RSAPublicKey;
import java.util.HashMap;
import java.util.Map;

public class CreateJwtTest {

    //生成jwt令牌
    @Test
    public void createToken() {
        /*
            1.创建秘钥工厂,实例化KeyStoreKeyFactory
            其中有两个参数，参数1表示私钥路径地址，参数2表示秘钥库密码
         */
        //1.2

        String key_location = "changgou.jks";//证书
        //获取根目录下的证书
        ClassPathResource resource = new ClassPathResource(key_location);

        //秘钥库密码
        String  keystorePassword="changgou";
        //1.1 生成秘钥工厂
        KeyStoreKeyFactory keyStoreKeyFactory = new KeyStoreKeyFactory(resource,keystorePassword.toCharArray());

        //2.基于工厂获取秘钥对(公钥、私钥)
        String alias="changgou";//秘钥别名
        String keypass="changgou";//秘钥密码

        KeyPair keyPair =keyStoreKeyFactory.getKeyPair(alias,keypass.toCharArray());

        //2.1 获取私钥
        RSAPrivateCrtKey rsaPrivateCrtKey = (RSAPrivateCrtKey) keyPair.getPrivate();
        System.out.println("私钥："+rsaPrivateCrtKey);

        RSAPublicKey rsaPublicKey = (RSAPublicKey) keyPair.getPublic();
        System.out.println("公钥："+rsaPublicKey);

        //定义Payload
        Map<String, Object> tokenMap = new HashMap<>();
        tokenMap.put("id", "1");
        tokenMap.put("name", "itheima");
        tokenMap.put("roles", "ROLE_VIP,ROLE_USER");

        //生成jwt
        Signer signer = new RsaSigner(rsaPrivateCrtKey);
        Jwt jwt = JwtHelper.encode(JSON.toJSONString(tokenMap), signer);

        //获取jwt令牌
        String jwtToken = jwt.getEncoded();
        System.out.println("jwt令牌："+jwtToken);

    }

    @Test
    public void bcrypt(){
        //生成加密密码
        String hashpw = BCrypt.hashpw("123", BCrypt.gensalt());

        //校验密码一致性
        boolean checkpw = BCrypt.checkpw("123", hashpw);

        System.out.println(checkpw);
    }

}
