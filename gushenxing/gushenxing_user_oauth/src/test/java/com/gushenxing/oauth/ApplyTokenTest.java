package com.gushenxing.oauth;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Base64Utils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.URI;
import java.util.Map;

/*
认证申请令牌测试类
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OAuthApplication.class)
public class ApplyTokenTest {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private LoadBalancerClient loadBalancerClient;

    @Test
    public void testCreateToken(){
        //1.1 远程根据服务名称获取服务实例对象
        ServiceInstance instance = loadBalancerClient.choose("user-auth");
        //获取ip地址   http://localhost:9200
        URI uri = instance.getUri();
        String url = uri+"/oauth/token";

        //1.2封装数据，body和header

        //1.2.1封装body数据
       MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
       body.add("grant_type","password");
       body.add("username","itheima");
       body.add("password","itheima");

       //1.2.2封装header数据
        MultiValueMap<String, String> header = new LinkedMultiValueMap<>();
        String clientId="changgou";
        String clientSecret="changgou";
        header.add("Authorization",this.getHttpBasic(clientId,clientSecret));

        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(body,header);

        //1.3友好的体验
        //指定 restTemplate当遇到400或401响应时候也不要抛出异常，也要正常返回值
        restTemplate.setErrorHandler(new DefaultResponseErrorHandler(){
            @Override
            public void handleError(ClientHttpResponse response) throws IOException {
               if (response.getRawStatusCode()!=400 && response.getRawStatusCode()!=401){
                   super.handleError(response);
               }
            }
        });
        /*
            1.发送请求
               参数1表示请求的路径地址 http://localhost:9200/oauth/token
               参数2表示请求方式
               参数3表示封装的参数
               参数4表示封装成什么类型
         */
        ResponseEntity<Map> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity, Map.class);
        //2.获取响应结果
        Map map = responseEntity.getBody();
        System.out.println(map);

    }

    //1.2.2.1进行http basic认证操作，把客户端id和客户端密码进行拼接，然后用base64进行编码
    private String getHttpBasic(String clientId, String clientSecret) {
        String value = clientId + ":" + clientSecret;
        byte[] encode = Base64Utils.encode(value.getBytes());

        return "Basic "+new String(encode);
    }
}
