package com.gushenxing.oauth;

import org.junit.Test;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.security.jwt.crypto.sign.RsaVerifier;

public class ParseJwtTest {

    @Test
    public void parseJwtToken(){
        //jwt令牌
        String token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlcyI6IlJPTEVfVklQLFJPTEVfVVNFUiIsIm5hbWUiOiJpdGhlaW1hIiwiaWQiOiIxIn0.O7q3x3jPzURWgMuXNdzpU3s8wnDICPRpiz7JMRc675pwqqIBVO7w4LMuJXzLcG1H7X4HXZ3syZJYKtL3rpCq0rWJLbQVZWQ48SJ3IbyQvyyWEvK0ZDEVb_jRJW36IOnXJCZFSExx5p81ulI65VZHfCawQ4niuTTklISV9mBsyHrXHPJekSOXlYB2GbxUAA9E3qFISDWyZQms3sAQzl0w1ROD2GYDEaJqv0SyigV-k0phqb9x6de1oEfyF6RkCUOkylJykGEs-69sLAgYO7jJGec1MlwQG_B5-wW2BklRUZw6b7Ica5YrR9kLkGeNSQg_JTz1n3kEsz0rqW9TKrLeyQ";

        //公钥
        String publicKey = "-----BEGIN PUBLIC KEY-----MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAutCFbcuYzwSK9Ik6eJsWFSOzKEi62gMtAz8Hz3jEFbVi2MVmCeTGDsz1HXYGovOlIVNU/rbVWe/WFtUaFIWev2+qpBnIZSM/xX4IetqAj0v9YtisQMd/S+nbeLUS9eZGQ+EXUdJSHxy31a+5+A6/BcoSAjUrJ2LJLANJM4lfz/ARXtmHAjxUuRdfp07gP8XssO+R5FHFVs+DrIp0I1wyY6ZAWRYZBUqLpPBG1W5GbUz6VMWFOPuU+IXzERAZ15Fb40Qb3mJOcsSRA3Lrr4Mwc5L1uLQntLEFl7olZP/B1dB0Cd/II1LVAnVuOXJ0Sm5T2wJp4jlVcKavt37NgXhy0wIDAQAB-----END PUBLIC KEY-----";

        //检验jwt
        RsaVerifier verifer = new RsaVerifier(publicKey);
        Jwt jwt = JwtHelper.decodeAndVerify(token, verifer);

        //获取Jwt原始内容
        String claims = jwt.getClaims();
        System.out.println(claims);
        //jwt令牌
        String jwtEncoded = jwt.getEncoded();
        System.out.println(jwtEncoded);

    }
}
