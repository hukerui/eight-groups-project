package com.gushenxing.oauth.service;


import com.gushenxing.oauth.util.AuthToken;

public interface AuthService {

    AuthToken login(String username, String password, String clientId, String clientSecret);
}
